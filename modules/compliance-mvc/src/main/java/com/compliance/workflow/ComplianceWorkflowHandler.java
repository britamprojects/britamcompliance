package com.compliance.workflow;

import com.compliance.service.model.Compliance;
import com.compliance.service.service.ComplianceLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.workflow.BaseWorkflowHandler;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.kernel.workflow.WorkflowHandler;

import java.io.Serializable;
import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(property = { "model.class.name=com.compliance.service.model.Compliance" }, service = WorkflowHandler.class)
public class ComplianceWorkflowHandler extends BaseWorkflowHandler<Compliance> {
	private ComplianceLocalService complianceService;

	@Reference(unbind = "-")
	protected void setComplianceService(ComplianceLocalService complianceService) {
		this.complianceService = complianceService;
	}

	@Override
	public String getClassName() {
		return Compliance.class.getName();
	}

	@Override
	public String getType(Locale locale) {
		return "compliance";
	}

	@Override
	public Compliance updateStatus(int status, Map<String, Serializable> workflowContext) throws PortalException {

		long userId = GetterUtil.getLong((String) workflowContext.get(WorkflowConstants.CONTEXT_USER_ID));
		long resourcePrimKey = GetterUtil
				.getLong((String) workflowContext.get(WorkflowConstants.CONTEXT_ENTRY_CLASS_PK));

		ServiceContext serviceContext = (ServiceContext) workflowContext.get("serviceContext");

		return complianceService.updateStatus(userId, resourcePrimKey, status, serviceContext);
	}
}
