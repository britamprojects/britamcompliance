package com.compliance.holders;

public class UserPositions {
	long userPositionId;
	String positionName;
	long organizationId;
	long positionId;
	String orgName;
	
	
	public long getUserPositionId() {
		return userPositionId;
	}
	public String getPositionName() {
		return positionName;
	}
	
	public long getPositionId() {
		return positionId;
	}
	public void setUserPositionId(long userPositionId) {
		this.userPositionId = userPositionId;
	}
	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}
	
	public long getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(long organizationId) {
		this.organizationId = organizationId;
	}
	public void setPositionId(long positionId) {
		this.positionId = positionId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

}
