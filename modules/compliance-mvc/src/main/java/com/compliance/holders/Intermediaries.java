package com.compliance.holders;

public class Intermediaries {

	long entityId;
	long orgId;
	long positionId;
	long contractId;
	long capacity;
	String firstName;
	String middleName;
	String lastName;
	String contractNumber;
	String contractStatus;
	String contractESDate;
	String contractEEDate;
	String historyESDate;
	String historyEEDate;
	String orgName;
	String positionName;
	String assignmentStatus;

	String parentOrgName;
	String grandParentOrgName;

	String oldOrgName;
	String oldPositionName;

	public long getEntityId() {
		return entityId;
	}

	public void setEntityId(long entityId) {
		this.entityId = entityId;
	}

	public long getOrgId() {
		return orgId;
	}

	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}

	public long getPositionId() {
		return positionId;
	}

	public void setPositionId(long positionId) {
		this.positionId = positionId;
	}

	public long getContractId() {
		return contractId;
	}

	public void setContractId(long contractId) {
		this.contractId = contractId;
	}

	public long getCapacity() {
		return capacity;
	}

	public void setCapacity(long capacity) {
		this.capacity = capacity;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getContractStatus() {
		return contractStatus;
	}

	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}

	public String getContractESDate() {
		return contractESDate;
	}

	public void setContractESDate(String contractESDate) {
		this.contractESDate = contractESDate;
	}

	public String getContractEEDate() {
		return contractEEDate;
	}

	public void setContractEEDate(String contractEEDate) {
		this.contractEEDate = contractEEDate;
	}

	public String getHistoryESDate() {
		return historyESDate;
	}

	public void setHistoryESDate(String historyESDate) {
		this.historyESDate = historyESDate;
	}

	public String getHistoryEEDate() {
		return historyEEDate;
	}

	public void setHistoryEEDate(String historyEEDate) {
		this.historyEEDate = historyEEDate;
	}

	public String getOrgName() {
		return orgName;
	}

	public String getParentOrgName() {
		return parentOrgName;
	}


	public String getOldOrgName() {
		return oldOrgName;
	}

	public String getOldPositionName() {
		return oldPositionName;
	}

	public void setParentOrgName(String parentOrgName) {
		this.parentOrgName = parentOrgName;
	}

	
	public void setOldOrgName(String oldOrgName) {
		this.oldOrgName = oldOrgName;
	}

	public void setOldPositionName(String oldPositionName) {
		this.oldPositionName = oldPositionName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public String getAssignmentStatus() {
		return assignmentStatus;
	}

	public void setAssignmentStatus(String assignmentStatus) {
		this.assignmentStatus = assignmentStatus;
	}

	public String getGrandParentOrgName() {
		return grandParentOrgName;
	}

	public void setGrandParentOrgName(String grandParentOrgName) {
		this.grandParentOrgName = grandParentOrgName;
	}

}
