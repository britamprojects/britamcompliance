package com.compliance.scheduler;

import com.compliance.service.model.Compliance;
import com.compliance.service.service.ComplianceLocalService;
import com.compliance.service.service.ComplianceLocalServiceUtil;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.DestinationNames;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.model.Portlet;
import com.liferay.portal.kernel.scheduler.SchedulerEngineHelperUtil;
import com.liferay.portal.kernel.scheduler.SchedulerEntryImpl;
import com.liferay.portal.kernel.scheduler.SchedulerException;
import com.liferay.portal.kernel.scheduler.Trigger;
import com.liferay.portal.kernel.scheduler.TriggerFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;

// every minute 0 * * ? * *
// every second * * * ? * *
// everry two minutes 0 */2 * ? * *
@Component(immediate = true, property = { "cron.expression=  0 * * ? * *" }, service = ComplianceScheduler.class)
public class ComplianceScheduler extends BaseMessageListener {
	@Reference
	ComplianceLocalService complianceLocalService;
	private static Log log = LogFactoryUtil.getLog(Portlet.class);

	@Override
	protected void doReceive(Message message) throws Exception {

		// call method to invoke REST to POST to FAMS
		transferCompleteRecsToFAMS();
	}

	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) throws SchedulerException {

		try {
			String cronExpression = GetterUtil.getString(properties.get("cron.expression"), "cronExpression");
			log.info(" cronExpression: " + cronExpression);

			String listenerClass = getClass().getName();
			Trigger jobTrigger = TriggerFactoryUtil.createTrigger(listenerClass, listenerClass, new Date(), null,
					cronExpression);
			SchedulerEntryImpl schedulerEntryImpl0 = new SchedulerEntryImpl();

			SchedulerEntryImpl schedulerEntryImpl = new SchedulerEntryImpl();
			schedulerEntryImpl.setEventListenerClass(listenerClass);
			schedulerEntryImpl.setTrigger(jobTrigger);

			SchedulerEngineHelperUtil.register(this, schedulerEntryImpl, DestinationNames.SCHEDULER_DISPATCH);

		} catch (Exception e) {
			log.error(e);
		}
	}

	@Deactivate
	protected void deactive() {
		SchedulerEngineHelperUtil.unregister(this);
	}

	/* method to read complete proceces */
	public void transferCompleteRecsToFAMS() throws Exception {

		System.out.println("------invokes POST...");

		List<Compliance> completedRequests = new ArrayList<Compliance>();
		completedRequests = ComplianceLocalServiceUtil.findByUnprocessedFinalStatus(true, null);

		System.out.println("------size of list is : " + completedRequests.size());

		try {

			String uristring = "http://10.10.4.214:9001/ords/apex_ebs_extension/fams/compliance/";

			for (Compliance compl : completedRequests) {
				
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				
				// Create the request
				JSONObject comp = JSONFactoryUtil.createJSONObject();
				comp.put("contract_number", compl.getContractNumber());
				comp.put("approver_contract_number", compl.getApproverContractNumber());
				comp.put("approver_action", compl.getApproverAction());
				comp.put("approver_verdict", compl.getApproverVerdict());
				comp.put("approver_action_date", formatter.format(compl.getApprovalDate()));
				comp.put("approver_comments", compl.getComments());
				comp.put("entity_id", String.valueOf(compl.getEntityId()));
				comp.put("contract_id", String.valueOf(compl.getContractId()));
				comp.put("assignment_status", compl.getAssignmentStatus());
				comp.put("capacity", compl.getCapacity());
				
				comp.put("new_org_id_1", compl.getNewOrgId1());
				comp.put("new_org_position_id_1", compl.getNewPositionId1());
				
				comp.put("new_org_id_2", compl.getNewOrgId2());
				comp.put("new_org_position_id_2", compl.getNewPositionId2());
							
				comp.put("contract_start_date", compl.getContractStartDate());
				comp.put("contract_end_date", compl.getContractEndDate());
				comp.put("history_start_date", compl.getHistoryStartDate());
				comp.put("history_end_date", compl.getHistoryEndDate());
				comp.put("old_org_id", compl.getOldOrgId());
				comp.put("old_position_id", compl.getOldPositionId());

				System.out.println("------" + comp.toString());

				// Invoke the REST method using a POST
				HttpPost methodPost = new HttpPost(uristring.trim());
				methodPost.setEntity(new StringEntity(comp.toString()));
				methodPost.setHeader("Content-Type", "application/json");

				// Execute the POST request
				HttpClient httpclient = HttpClientBuilder.create().build();
				HttpResponse responsePut = httpclient.execute(methodPost);

				System.out.println(responsePut.getStatusLine());

				// update processed_flag
				if (responsePut.getStatusLine().getStatusCode() == 200) {
					compl.setProcessedFlag("P");
					complianceLocalService.updateCompliance(compl);
				}
			}
		} catch (

		Exception e) {
			System.out.println("The POST request failed : "+e.toString());
		}
	}

}
