<%@ include file="/init.jsp"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>

<%
	List<String> process = (ArrayList) request.getAttribute("process");
	List<String> intermediariesList = (ArrayList) request.getAttribute("intermediariesList");
%>
<liferay-util:include page="/navigation_bar.jsp"
	servletContext="<%=application%>">
	<liferay-util:param name="searchEnabled" value="false" />
</liferay-util:include>

<portlet:actionURL name="drillDownHierachy"
	var="drillDownHierachyActionURL" />
<h2>Post New Request</h2>
<table class="table table-striped">
	<tr>
		<th style="width: 150px">Contract Number</th>
		<th>Name</th>
		<th>Organization</th>
		<th>Position</th>
		<th style="width: 100px">Action</th>

	</tr>
	<c:forEach items="${intermediariesList}" var="intermediary">

		<portlet:actionURL var="recordComplianceRenderURL"
			name="getIntermediaryPositionId">
			<portlet:param name="mvcPath" value="/add-record.jsp" />
			<portlet:param name="contractNumber"
				value="${intermediary.contractNumber}" />
			<portlet:param name="firstName" value="${intermediary.firstName}" />
			<portlet:param name="middleName" value="${intermediary.middleName}" />
			<portlet:param name="lastName" value="${intermediary.lastName}" />

			<portlet:param name="newOrgName1" value="${intermediary.orgName}" />
			<portlet:param name="newPositionName1"
				value="${intermediary.positionName}" />

			<portlet:param name="entityId" value="${intermediary.entityId}" />
			<portlet:param name="contractId" value="${intermediary.contractId}" />

			<portlet:param name="oldOrgId" value="${intermediary.orgId}" />
			<portlet:param name="oldPositionId"
				value="${intermediary.positionId}" />

			<portlet:param name="parentOrgName"
				value="${intermediary.parentOrgName}" />
			<portlet:param name="grandParentOrgName"
				value="${intermediary.grandParentOrgName}" />
		</portlet:actionURL>

		<tr>
			<td class="text-center" style="width: 50px" type="submit"
				value="Submit"><aui:form id="formid">
					<a href="<%=recordComplianceRenderURL%>"
						onclick="document.getElementById('formid').submit()">${intermediary.getContractNumber()}</a>
					<aui:input name="selectedMenu" type="hidden" readonly="true"
						value="postRequests" />
				</aui:form></td>
			<td>${intermediary.getFirstName()}
				${intermediary.getMiddleName()} ${intermediary.getLastName()}</td>
			<td>${intermediary.getOrgName()}</td>
			<td>${intermediary.getPositionName()}</td>


			<td class="text-center" style="width: 50px" type="submit"
				value="Submit"><aui:form
					action="<%=drillDownHierachyActionURL%>" name="complianceForm"
					method="POST">
					<aui:input name="orgId" type="hidden" readonly="true"
						value="${intermediary.orgId}" />
					<aui:input name="positionName" type="hidden" readonly="true"
						value="${intermediary.positionName}" />
					<aui:input name="selectedMenu" type="hidden" readonly="true"
						value="postRequests" />

					<aui:button class="btn  btn-primary btn-default btn-sm px-2 py-1"
						type="submit" name="" value="Drill Down" />


				</aui:form></td>


			</td>
		</tr>
	</c:forEach>
</table>



