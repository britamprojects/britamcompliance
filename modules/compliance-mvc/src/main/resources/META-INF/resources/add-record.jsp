<%@ include file="init.jsp"%>
<%@ page import="java.util.List,java.util.ArrayList"%>

<%
	ArrayList<String> process = (ArrayList) request.getAttribute("process");
ArrayList<String> intermediariesList = (ArrayList) request.getAttribute("intermediariesList");
ArrayList<String> organizationsList = (ArrayList) request.getAttribute("organizationsList");
ArrayList<String> positionsList = (ArrayList) request.getAttribute("positionsList");
ArrayList<String> assignmentStatusList = (ArrayList) request.getAttribute("assignmentStatusList");
ArrayList<String> akiReasonsList = (ArrayList) request.getAttribute("akiReasonsList");
ArrayList<String> regionOrganizationsList = (ArrayList) request.getAttribute("regionOrganizationsList");

ArrayList<String> contractPositionsList = (ArrayList) request.getAttribute("contractPositionsList");

/* ArrayList<String> unitOrganizationsList = (ArrayList) request.getAttribute("unitOrganizationsList");
ArrayList<String> branchOrganizationsList = (ArrayList) request.getAttribute("branchOrganizationsList");
ArrayList<String> rmBranchesList = (ArrayList) request.getAttribute("rmBranchesList"); */
%>

<%
	String oldOrgId = renderRequest.getParameter("oldOrgId");
String oldPositionId = renderRequest.getParameter("oldPositionId");
String entityId = renderRequest.getParameter("entityId");
String contractId = renderRequest.getParameter("contractId");
String contractNumber = renderRequest.getParameter("contractNumber");
String firstName = renderRequest.getParameter("firstName");
String middleName = renderRequest.getParameter("middleName");
String lastName = renderRequest.getParameter("lastName");
String newPositionName1 = renderRequest.getParameter("newPositionName1");
String newOrgName1 = renderRequest.getParameter("newOrgName1");
String spacer = " ";

String catPromotion = "Promotion";
String catTransfer = "Transfer";
String catReinstatement = "Reinstatement";
String catSuspension = "Suspension";
String catTermination = "Termination";
%>
<portlet:resourceURL var="getContentURL" id="serveResource" />
<portlet:actionURL name="addRecord" var="addRecordActionURL" />
<portlet:actionURL name="detNav" var="detNavActionURL" />
<portlet:actionURL name="getChildOrgs" var="getChildOrgsURL" />
<portlet:actionURL name="getRegionOrgId" var="getRegionOrgURL" />


<!-- Dynamic Select -->
<!--Transfers-->
<portlet:resourceURL var="getRmTransferURL" id="rmTransferResource" />
<!-- IDS for getRmTransferURL =  RmTransferId -->
<portlet:resourceURL var="getBmTransferURL" id="bmTransferResource" />
<!-- IDS for getBmTransferURL =  bmTransferId -->
<portlet:resourceURL var="getUmTransferURL" id="umTransferResource" />
<!-- IDS for getUmTransferURL =  umTransferId -->
<portlet:resourceURL var="getFaTransferURL" id="faTransferResource" />
<!-- IDS for getFaTransferURL =  faTransferId -->

<!-- Promotion -->
<!--Promofers-->
<portlet:resourceURL var="getRmPromoferURL" id="rmPromoferResource" />
<!-- IDS for getRmPromoferURL =  RmPromoferId -->
<portlet:resourceURL var="getBmPromoferURL" id="bmPromoferResource" />
<!-- IDS for getBmPromoferURL =  bmPromoferId -->
<portlet:resourceURL var="getUmPromoferURL" id="umPromoferResource" />
<!-- IDS for getUmPromoferURL =  umPromoferId -->
<portlet:resourceURL var="getFaPromoferURL" id="faPromoferResource" />
<!-- IDS for getFaPromoferURL =  faPromoferId -->

<!-- DYnamic Select -->

<liferay-util:include page="/navigation_bar.jsp"
	servletContext="<%=application%>">
	<liferay-util:param name="searchEnabled" value="false" />
</liferay-util:include>

<h2>Post New Request</h2>

<table>
	<tr>
		<th>Contract Number</th>
		<td><%=contractNumber%></td>
	</tr>
	<tr>
		<th>Full Name</th>

		<%
			if (firstName == null) {
		%>
		<td><%=middleName%><%=spacer%><%=lastName%></td>
		<%
			} else if (middleName == null) {
		%>
		<td><%=firstName%><%=spacer%><%=lastName%></td>
		<%
			} else if (lastName == null) {
		%>
		<td><%=firstName%><%=spacer%><%=middleName%></td>
		<%
			} else {
		%>
		<td><%=firstName%><%=spacer%><%=middleName%><%=spacer%><%=lastName%></td>
		<%
			}
		%>
	</tr>
	<tr>
		<th>Current Organization</th>
		<td><%=newOrgName1%></td>
		<%-- old_organization=<%=newOrgName1%> --%>
	</tr>
	<tr>
		<th>Current Position</th>
		<td><%=newPositionName1%></td>
		<%--  old_position=<%=newPositionName1%> --%>
	</tr>


	<c:forEach items="${contractPositionsList}" var="newCurrentPos">
		<tr>
			<th>${newCurrentPos.positionName}</th>
			<td>${newCurrentPos.orgName}</td>
			<%--  old_position=<%=newPositionName1%> --%>
		</tr>
	</c:forEach>




</table>

</br>
</br>

<aui:select label="Application Category" name="applicationCategory"
	required="true">
	<aui:option value="" label="Select Application Category"></aui:option>
	<c:forEach items="${process}" var="process">
		<aui:option value="${process.processCategory}">${process.processFlowName}</aui:option>
	</c:forEach>
</aui:select>

<!-- RM Operations -->
<aui:form action="<%=addRecordActionURL%>" name="rmtransferForm"
	method="POST">
	<div id="rmtransfer" hidden="true">


		<liferay-ui:tabs names="Region Manager Transfer" refresh="false">

			<aui:select id="rmTransOrgId1" name="newOrgId1" label="Select Region"
				onChange="getRmTransValues(this.value,0);" required="true"
				showRequiredLabel="false">
				<aui:option value=""> Select Region</aui:option>
				<c:forEach items="${regionOrganizationsList}" var="org">
					<aui:option value="${org.orgId}">${org.orgName}</aui:option>
				</c:forEach>
			</aui:select>

			<aui:select id="rmTransOrgId2" name="newOrgId2" label="Select Branch"
				onChange="getRmTransValuess(this.value,0);" required="true"
				showRequiredLabel="false">
				<aui:option value="">Select Branch</aui:option>
			</aui:select>


			<aui:select id="capacity" name="capacity" required="true"
				label="Select Capacity">
				<aui:option value="1">Confirmed</aui:option>
				<aui:option value="2">Acting</aui:option>
			</aui:select>
			<aui:input name="historyStartDate" type="date"
				label="History Start Date" />
			<aui:input id="comments" name="comments" label="Comment"
				placeholder="Transfer RM, From xxxx, To xxxx, Position: Acting/Confirmed"
				type="textarea" size="30" />
		</liferay-ui:tabs>


		</br> </br> </br>

		<aui:button type="submit" name="submitRequest" value="Submit Request"></aui:button>


		<!-- hidden values must come from local variables and not direct from previous page -->

		<aui:input name="applicationCategory" type="hidden"
			value="<%=catTransfer%>" />
		<aui:input name="approverAction" type="hidden" value="RMTransfer" />
		<aui:input name="contractId" type="hidden" value="<%=contractId%>" />
		<aui:input name="entityId" type="hidden" value="<%=entityId%>" />

		<aui:input name="oldOrgId" type="hidden" value="<%=oldOrgId%>" />
		<aui:input name="oldPositionId" type="hidden"
			value="<%=oldPositionId%>" />

		<aui:input name="oldOrgName" type="hidden" value="<%=newOrgName1%>" />
		<aui:input name="oldPositionName" type="hidden"
			value="<%=newPositionName1%>" />
		<%-- <aui:input name="oldOrgId" type="hidden" value="<%=newOrgName1%>" /> --%>
		<%-- <aui:input name="oldPositionId" type="hidden"
value="<%=newPositionName1%>" /> --%>

		<aui:input name="contractNumber" type="hidden"
			value="<%=contractNumber%>" />

		<aui:input name="firstName" type="hidden" value="<%=firstName%>" />
		<aui:input name="middleName" type="hidden" value="<%=middleName%>" />
		<aui:input name="lastName" type="hidden" value="<%=lastName%>" />
	</div>
</aui:form>

<!-- BM Operations -->
<aui:form action="<%=addRecordActionURL%>" name="bmPromotionForm"
	method="POST">
	<div id="bmpromotion" hidden="true">

		<liferay-ui:tabs names="Branch Manager Promotion" refresh="false">

			<aui:select id="bmPromoOrgId1" name="newOrgId1" label="Select Region"
				onChange="getBmPromoValues(this.value,0);" required="true"
				showRequiredLabel="false">
				<aui:option value=""> Select Region</aui:option>
				<c:forEach items="${regionOrganizationsList}" var="org">
					<aui:option value="${org.orgId}">${org.orgName}</aui:option>
				</c:forEach>
			</aui:select>

			<aui:select id="bmPromoOrgId2" name="newOrgId2" label="Select Branch"
				onChange="getBmPromoValuess(this.value,0);" required="true"
				showRequiredLabel="false">
				<aui:option value="">Select Branch</aui:option>
			</aui:select>

			<aui:select id="capacity" name="capacity" required="true"
				label="Select Capacity">
				<aui:option value="1">Confirmed</aui:option>
				<aui:option value="2">Acting</aui:option>
			</aui:select>
			<aui:input name="historyStartDate" type="date"
				label="History Start Date" />
			<aui:input id="comments" name="comments" label="Comment"
				placeholder="Promote BM, From xxxx, To xxxx, Position: Acting/Confirmed"
				type="textarea" size="30" />
		</liferay-ui:tabs>

		</br> </br> </br>

		<aui:button type="submit" name="submitRequest" value="Submit Request"></aui:button>

		<!-- hidden values must come fron local variables and not direct from previous page -->

		<aui:input name="applicationCategory" type="hidden"
			value="<%=catPromotion%>" />
		<aui:input name="approverAction" type="hidden" value="BMPromotion" />
		<aui:input name="contractId" type="hidden" value="<%=contractId%>" />
		<aui:input name="entityId" type="hidden" value="<%=entityId%>" />
		<aui:input name="oldOrgId" type="hidden" value="<%=newOrgName1%>" />
		<aui:input name="oldPositionId" type="hidden"
			value="<%=newPositionName1%>" />

		<aui:input name="contractNumber" type="hidden"
			value="<%=contractNumber%>" />

		<aui:input name="firstName" type="hidden" value="<%=firstName%>" />
		<aui:input name="middleName" type="hidden" value="<%=middleName%>" />
		<aui:input name="lastName" type="hidden" value="<%=lastName%>" />
	</div>
</aui:form>

<aui:form action="<%=addRecordActionURL%>" name="bmtransferForm"
	method="POST">
	<div id="bmtransfer" hidden="true">
		<liferay-ui:tabs names="Branch Manager Transfer" refresh="false">

			<aui:fieldset>

				<aui:select id="bmTransOrgId1" name="" label="Select Region"
					onChange="getBmTransValues(this.value,0);" required="true"
					showRequiredLabel="false">
					<aui:option value=""> Select Region</aui:option>
					<c:forEach items="${regionOrganizationsList}" var="org">
						<aui:option value="${org.orgId}">${org.orgName}</aui:option>
					</c:forEach>
				</aui:select>

				<aui:select id="bmTransOrgId2" name="newOrgId1"
					label="Select Branch" onChange="getBmTransValuess(this.value,0);"
					required="true" showRequiredLabel="false">
					<aui:option value="">Select Branch</aui:option>
				</aui:select>

				<aui:select name="newOrgId2" label="Select Unit" id="bmTransOrgId3"
					required="true" showRequiredLabel="false">
					<aui:option value="">Select Unit</aui:option>
				</aui:select>

				<aui:select id="capacity" name="capacity" required="true"
					label="Select Capacity">
					<aui:option value="1">Confirmed</aui:option>
					<aui:option value="2">Acting</aui:option>
				</aui:select>
				<aui:input name="historyStartDate" type="date"
					label="History Start Date" />
				<aui:input id="comments" name="comments" label="Comment"
					placeholder="Transfer BM, From xxxx, To xxxx, Position: Acting/Confirmed"
					type="textarea" size="30" />

			</aui:fieldset>
		</liferay-ui:tabs>


		</br> </br> </br>

		<aui:button type="submit" name="submitRequest" value="Submit Request"></aui:button>


		<!-- hidden values must come from local variables and not direct from previous page -->

		<aui:input name="applicationCategory" type="hidden"
			value="<%=catTransfer%>" />
		<aui:input name="approverAction" type="hidden" value="BMTransfer" />
		<aui:input name="contractId" type="hidden" value="<%=contractId%>" />
		<aui:input name="entityId" type="hidden" value="<%=entityId%>" />
		<aui:input name="oldOrgId" type="hidden" value="<%=newOrgName1%>" />
		<aui:input name="oldPositionId" type="hidden"
			value="<%=newPositionName1%>" />

		<aui:input name="contractNumber" type="hidden"
			value="<%=contractNumber%>" />

		<aui:input name="firstName" type="hidden" value="<%=firstName%>" />
		<aui:input name="middleName" type="hidden" value="<%=middleName%>" />
		<aui:input name="lastName" type="hidden" value="<%=lastName%>" />
	</div>
</aui:form>

<!-- UM Operations -->
<aui:form action="<%=addRecordActionURL%>" name="umtransferForm"
	method="POST">
	<div id="umtransfer" hidden="true">

		<liferay-ui:tabs names="Unit Manager Transfer" refresh="false">

			<aui:select id="umTransOrgId1" name="" label="Select Region"
				onChange="getUmTransValues(this.value,0);" required="true"
				showRequiredLabel="false">
				<aui:option value=""> Select Region</aui:option>
				<c:forEach items="${regionOrganizationsList}" var="org">
					<aui:option value="${org.orgId}">${org.orgName}</aui:option>
				</c:forEach>
			</aui:select>

			<aui:select id="umTransOrgId2" name="newOrgId0" label="Select Branch"
				onChange="getUmTransValuess(this.value,0);" required="true"
				showRequiredLabel="false">
				<aui:option value="">Select Branch</aui:option>
			</aui:select>

			<aui:select name="newOrgId1" label="Select Unit" id="umTransOrgId3"
				required="true" showRequiredLabel="false">
				<aui:option value="">Select Unit</aui:option>
			</aui:select>

			<aui:input name="historyStartDate"
				value="${intermediary.historyESDate} " type="date"
				label="History Start Date" />
			<aui:select id="capacity" name="capacity" required="true"
				label="Select Capacity">
				<aui:option value="1">Confirmed</aui:option>
				<aui:option value="2">Acting</aui:option>
			</aui:select>
			<aui:input id="comments" name="comments" type="textarea"
				value="${compliance.comments}" />
		</liferay-ui:tabs>
		</br> </br> </br>
		<aui:button type="submit" name="submitRequest" value="Submit Request"></aui:button>
		<!-- hidden values must come fron local variables and not direct from previous page -->
		<aui:input name="applicationCategory" type="hidden"
			value="<%=catTransfer%>" />
		<aui:input name="approverAction" type="hidden" value="UMTransfer" />
		<aui:input name="contractId" type="hidden" value="<%=contractId%>" />
		<aui:input name="entityId" type="hidden" value="<%=entityId%>" />
		<aui:input name="oldOrgId" type="hidden" value="<%=newOrgName1%>" />
		<aui:input name="oldPositionId" type="hidden"
			value="<%=newPositionName1%>" />
		<aui:input name="contractNumber" type="hidden"
			value="<%=contractNumber%>" />
		<aui:input name="firstName" type="hidden" value="<%=firstName%>" />
		<aui:input name="middleName" type="hidden" value="<%=middleName%>" />
		<aui:input name="lastName" type="hidden" value="<%=lastName%>" />

	</div>
</aui:form>

<aui:form action="<%=addRecordActionURL%>" name="umpromotionForm"
	method="POST">
	<div id="umpromotion" hidden="true">
		<liferay-ui:tabs names="Unit Manager Promotion" refresh="false">
			<aui:select id="umPromoOrgId1" name="" label="Select Region"
				onChange="getUmPromoValues(this.value,0);" required="true"
				showRequiredLabel="false">
				<aui:option value=""> Select Region</aui:option>
				<c:forEach items="${regionOrganizationsList}" var="org">
					<aui:option value="${org.orgId}">${org.orgName}</aui:option>
				</c:forEach>
			</aui:select>

			<aui:select name="newOrgId1" id="umPromoOrgId2" label="Select Branch"
				onChange="getUmPromoValuess(this.value,0);" required="true"
				showRequiredLabel="false">
				<aui:option value="">Select Branch</aui:option>
			</aui:select>

			<aui:select name="newOrgId2" label="Select Unit" id="umPromoOrgId3"
				required="true" showRequiredLabel="false">
				<aui:option value="">Select Unit</aui:option>
			</aui:select>

			<aui:select id="capacity" name="capacity" required="true"
				label="Select Capacity">
				<aui:option value="1">Confirmed</aui:option>
				<aui:option value="2">Acting</aui:option>
			</aui:select>
			<aui:input name="historyStartDate" type="date"
				label="History Start Date" />
			<aui:input id="comments" name="comments" label="Comment"
				placeholder="Promote BM, From xxxx, To xxxx, Position: Acting/Confirmed"
				type="textarea" size="30" />
		</liferay-ui:tabs>
		</br> </br> </br>
		<aui:button type="submit" name="submitRequest" value="Submit Request"></aui:button>
		<!-- hidden values must come fron local variables and not direct from previous page -->

		<aui:input name="applicationCategory" type="hidden"
			value="<%=catPromotion%>" />
		<aui:input name="approverAction" type="hidden" value="UMPromotion" />
		<aui:input name="contractId" type="hidden" value="<%=contractId%>" />
		<aui:input name="entityId" type="hidden" value="<%=entityId%>" />
		<aui:input name="oldOrgId" type="hidden" value="<%=newOrgName1%>" />
		<aui:input name="oldPositionId" type="hidden"
			value="<%=newPositionName1%>" />

		<aui:input name="contractNumber" type="hidden"
			value="<%=contractNumber%>" />

		<aui:input name="firstName" type="hidden" value="<%=firstName%>" />
		<aui:input name="middleName" type="hidden" value="<%=middleName%>" />
		<aui:input name="lastName" type="hidden" value="<%=lastName%>" />
	</div>
</aui:form>
<aui:form action="<%=addRecordActionURL%>" name="UMreinstatementForm"
	method="POST">
	<div id="umreinstatement" hidden="true">
		<liferay-ui:tabs names="Unit Manager Reinstatement" refresh="false">
			<aui:input name="faLetter" label="FA letter Present ?"
				type="checkbox" />
			<aui:input name="fbmLetter"
				label="Former Branch Manager Approval Letter Present ?"
				type="checkbox" />
			<aui:input name="cbmLetter"
				label="Current Branch Manager Letter Present ?" type="checkbox" />
			<aui:input name="iraLicence" label="IRA License Present ?"
				type="checkbox" />
			<aui:input id="comments" name="comments" label="Comment"
				placeholder="Reinstate UM" type="textarea" />
		</liferay-ui:tabs>


		</br> </br> </br>

		<aui:button type="submit" name="submitRequest" value="Submit Request"></aui:button>


		<!-- hidden values must come fron local variables and not direct from previous page -->

		<aui:input name="applicationCategory" type="hidden"
			value="<%=catReinstatement%>" />
		<aui:input name="approverAction" type="hidden" value="UMReinstatement" />
		<aui:input name="contractId" type="hidden" value="<%=contractId%>" />
		<aui:input name="entityId" type="hidden" value="<%=entityId%>" />
		<aui:input name="oldOrgId" type="hidden" value="<%=newOrgName1%>" />
		<aui:input name="oldPositionId" type="hidden"
			value="<%=newPositionName1%>" />

		<aui:input name="contractNumber" type="hidden"
			value="<%=contractNumber%>" />

		<aui:input name="firstName" type="hidden" value="<%=firstName%>" />
		<aui:input name="middleName" type="hidden" value="<%=middleName%>" />
		<aui:input name="lastName" type="hidden" value="<%=lastName%>" />

	</div>
</aui:form>

<!-- New changes 05/10/2020 -->
<aui:form action="<%=addRecordActionURL%>" name="suspensionForm"
	method="POST">
	<div id="umsuspension" hidden="true">
		<liferay-ui:tabs names="Unit Manager Suspension" refresh="false">
			<aui:input id="comments" name="comments" type="textarea" />
		</liferay-ui:tabs>
		</br> </br> </br>
		<aui:button type="submit" name="submitRequest" value="Submit Request"></aui:button>
		<!-- hidden values must come fron local variables and not direct from previous page -->
		<aui:input name="applicationCategory" type="hidden"
			value="<%=catSuspension%>" />
		<aui:input name="approverAction" type="hidden" value="UMSuspension" />
		<aui:input name="contractId" type="hidden" value="<%=contractId%>" />
		<aui:input name="entityId" type="hidden" value="<%=entityId%>" />
		<aui:input name="oldOrgId" type="hidden" value="<%=newOrgName1%>" />
		<aui:input name="oldPositionId" type="hidden"
			value="<%=newPositionName1%>" />

		<aui:input name="contractNumber" type="hidden"
			value="<%=contractNumber%>" />

		<aui:input name="firstName" type="hidden" value="<%=firstName%>" />
		<aui:input name="middleName" type="hidden" value="<%=middleName%>" />
		<aui:input name="lastName" type="hidden" value="<%=lastName%>" />
	</div>
</aui:form>

<aui:form action="<%=addRecordActionURL%>" name="terminationForm"
	method="POST">
	<div id="umtermination" hidden="true">
		<liferay-ui:tabs names="Unit Manager Termination" refresh="false">
			<aui:select name="akiReasonCode" label="AKI Reason">
				<aui:option value="" label="Select AKI Reason"></aui:option>
				<c:forEach items="${akiReasonsList}" var="akiR">
					<aui:option value="${akiR.reasonCode}">${akiR.reasonName}</aui:option>
				</c:forEach>
			</aui:select>
			<aui:input name="contractEndDate" type="date"
				label="Contract End Date" />
			<aui:input id="comments" name="comments" label="Comment"
				placeholder="Terminate UM: Reason" type="textarea" />
		</liferay-ui:tabs>
		</br> </br> </br>
		<aui:button type="submit" name="submitRequest" value="Submit Request"></aui:button>
		<!-- hidden values must come fron local variables and not direct from previous page -->
		<aui:input name="applicationCategory" type="hidden"
			value="<%=catTermination%>" />
		<aui:input name="approverAction" type="hidden" value="UMTermination" />
		<aui:input name="contractId" type="hidden" value="<%=contractId%>" />
		<aui:input name="entityId" type="hidden" value="<%=entityId%>" />
		<aui:input name="oldOrgId" type="hidden" value="<%=newOrgName1%>" />
		<aui:input name="oldPositionId" type="hidden"
			value="<%=newPositionName1%>" />
		<aui:input name="contractNumber" type="hidden"
			value="<%=contractNumber%>" />
		<aui:input name="firstName" type="hidden" value="<%=firstName%>" />
		<aui:input name="middleName" type="hidden" value="<%=middleName%>" />
		<aui:input name="lastName" type="hidden" value="<%=lastName%>" />
	</div>
</aui:form>
<!-- New changes 05/10/2020 -->

<!-- FA Operations -->

<div id="promotion" hidden="true">
	<aui:form action="<%=addRecordActionURL%>" name="promotionForm"
		method="POST">

		<liferay-ui:tabs names="Promotion" refresh="false">
			<aui:select id="position1" name="newPositionId1"
				label="Select New Position 1">
				<c:forEach items="${positionsList}" var="pos">
					<aui:option value="${pos.positionId}"> ${pos.positionName}</aui:option>
				</c:forEach>
			</aui:select>
			<aui:select id="position2" name="newPositionId2"
				label="Select New Position 2">
				<c:forEach items="${positionsList}" var="pos">
					<aui:option value="${pos.positionId}"> ${pos.positionName}</aui:option>
				</c:forEach>
			</aui:select>


			<aui:select id="newOrg1" name="newOrgId1"
				label="Select New Organization 1">
				<c:forEach items="${organizationsList}" var="org">
					<aui:option value="${org.orgId}"> ${org.orgName}</aui:option>
				</c:forEach>
			</aui:select>

			<aui:select id="newOrg2" name="newOrgId2"
				label="Select New Organization 2">
				<c:forEach items="${organizationsList}" var="org">
					<aui:option value="${org.orgId}"> ${org.orgName}</aui:option>
				</c:forEach>
			</aui:select>

			<aui:select id="capacity" name="capacity" required="true"
				label="Select Capacity">
				<aui:option value="1">Confirmed</aui:option>
				<aui:option value="2">Acting</aui:option>
			</aui:select>
			<aui:input name="historyStartDate" type="date"
				label="History Start Date" size="30" />
			<aui:input id="comments" name="comments" label="Comment"
				type="textarea" />
		</liferay-ui:tabs>


		</br>
		</br>
		</br>

		<aui:button type="submit" name="submitRequest" value="Submit Request"></aui:button>


		<!-- hidden values must come fron local variables and not direct from previous page -->

		<aui:input name="applicationCategory" type="hidden"
			value="<%=catPromotion%>" />
		<aui:input name="approverAction" type="hidden" value="RMTransfer" />
		<aui:input name="contractId" type="hidden" value="<%=contractId%>" />
		<aui:input name="entityId" type="hidden" value="<%=entityId%>" />
		<aui:input name="oldOrgId" type="hidden" value="<%=newOrgName1%>" />
		<aui:input name="oldPositionId" type="hidden"
			value="<%=newPositionName1%>" />

		<aui:input name="contractNumber" type="hidden"
			value="<%=contractNumber%>" />

		<aui:input name="firstName" type="hidden" value="<%=firstName%>" />
		<aui:input name="middleName" type="hidden" value="<%=middleName%>" />
		<aui:input name="lastName" type="hidden" value="<%=lastName%>" />

	</aui:form>
</div>


<!-- FA Operations -->

<aui:form action="<%=addRecordActionURL%>" name="faumPromotionForm"
	method="POST">
	<div id="fapromotion" hidden="true">

		<liferay-ui:tabs names="Financial Advisor Promotion" refresh="false">

			<aui:select id="faPromoOrgId1" name="" label="Select Region"
				onChange="getFaPromoValues(this.value,0);" required="true"
				showRequiredLabel="false">
				<aui:option value=""> Select Region</aui:option>
				<c:forEach items="${regionOrganizationsList}" var="org">
					<aui:option value="${org.orgId}">${org.orgName}</aui:option>
				</c:forEach>
			</aui:select>

			<aui:select name="newOrgId1" id="faPromoOrgId2" label="Select Branch"
				onChange="getFaPromoValuess(this.value,0);" required="true"
				showRequiredLabel="false">
				<aui:option value="">Select Branch</aui:option>
			</aui:select>

			<aui:select name="newOrgId2" label="Select Unit" id="faPromoOrgId3"
				required="true" showRequiredLabel="false">
				<aui:option value="">Select Unit</aui:option>
			</aui:select>


			<aui:select id="capacity" name="capacity" required="true"
				label="Select Capacity">
				<aui:option value="1">Confirmed</aui:option>
				<aui:option value="2">Acting</aui:option>
			</aui:select>
			<aui:input name="historyStartDate" type="date"
				label="History Start Date" />
			<aui:input id="comments" name="comments" label="Comment"
				placeholder="Promote FA, From xxxx, To xxxx, Position: Acting/Confirmed"
				type="textarea" />
		</liferay-ui:tabs>

		</br> </br> </br>

		<aui:button type="submit" name="submitRequest" value="Submit Request"></aui:button>


		<!-- hidden values must come fron local variables and not direct from previous page -->

		<aui:input name="applicationCategory" type="hidden"
			value="<%=catPromotion%>" />
		<aui:input name="approverAction" type="hidden" value="FAPromotion" />
		<aui:input name="contractId" type="hidden" value="<%=contractId%>" />
		<aui:input name="entityId" type="hidden" value="<%=entityId%>" />
		<aui:input name="oldOrgId" type="hidden" value="<%=newOrgName1%>" />
		<aui:input name="oldPositionId" type="hidden"
			value="<%=newPositionName1%>" />

		<aui:input name="contractNumber" type="hidden"
			value="<%=contractNumber%>" />

		<aui:input name="firstName" type="hidden" value="<%=firstName%>" />
		<aui:input name="middleName" type="hidden" value="<%=middleName%>" />
		<aui:input name="lastName" type="hidden" value="<%=lastName%>" />

	</div>
</aui:form>

<aui:form action="<%=addRecordActionURL%>" name="terminationForm"
	method="POST">
	<div id="termination" hidden="true">
		<liferay-ui:tabs names="Financial Advisor Termination" refresh="false">
			<aui:select name="akiReasonCode" label="AKI Reason">
				<aui:option value="" label="Select AKI Reason"></aui:option>
				<c:forEach items="${akiReasonsList}" var="akiR">
					<aui:option value="${akiR.reasonCode}">${akiR.reasonName}</aui:option>
				</c:forEach>
			</aui:select>
			<aui:input name="contractEndDate" type="date"
				label="Contract End Date" />
			<aui:input id="comments" name="comments" label="Comment"
				placeholder="Terminate FA" type="textarea" />
		</liferay-ui:tabs>
		</br> </br> </br>
		<aui:button type="submit" name="submitRequest" value="Submit Request"></aui:button>
		<!-- hidden values must come fron local variables and not direct from previous page -->
		<aui:input name="applicationCategory" type="hidden"
			value="<%=catTermination%>" />
		<aui:input name="approverAction" type="hidden" value="FATermination" />
		<aui:input name="contractId" type="hidden" value="<%=contractId%>" />
		<aui:input name="entityId" type="hidden" value="<%=entityId%>" />
		<aui:input name="oldOrgId" type="hidden" value="<%=newOrgName1%>" />
		<aui:input name="oldPositionId" type="hidden"
			value="<%=newPositionName1%>" />
		<aui:input name="contractNumber" type="hidden"
			value="<%=contractNumber%>" />
		<aui:input name="firstName" type="hidden" value="<%=firstName%>" />
		<aui:input name="middleName" type="hidden" value="<%=middleName%>" />
		<aui:input name="lastName" type="hidden" value="<%=lastName%>" />
	</div>
</aui:form>

<aui:form action="<%=addRecordActionURL%>" name="reinstatementForm"
	method="POST">
	<div id="fareinstatement" hidden="true">
		<liferay-ui:tabs names="FA Reinstatement" refresh="false">
			<aui:input name="faLetter" label="FA letter Present ?"
				type="checkbox" />
			<aui:input name="fbmLetter"
				label="Former Branch Manager Approval Letter Present ?"
				type="checkbox" />
			<aui:input name="cbmLetter"
				label="Current Branch Manager Letter Present ?" type="checkbox" />
			<aui:input name="iraLicence" label="IRA License Present ?"
				type="checkbox" />
			<aui:input id="comments" name="comments" label="Comment"
				placeholder="FA Reinstatement" type="textarea" />
		</liferay-ui:tabs>
		</br> </br> </br>
		<aui:button type="submit" name="submitRequest" value="Submit Request"></aui:button>
		<!-- hidden values must come fron local variables and not direct from previous page -->
		<aui:input name="applicationCategory" type="hidden"
			value="<%=catReinstatement%>" />
		<aui:input name="approverAction" type="hidden" value="FAReinstatement" />
		<aui:input name="contractId" type="hidden" value="<%=contractId%>" />
		<aui:input name="entityId" type="hidden" value="<%=entityId%>" />
		<aui:input name="oldOrgId" type="hidden" value="<%=newOrgName1%>" />
		<aui:input name="oldPositionId" type="hidden"
			value="<%=newPositionName1%>" />
		<aui:input name="contractNumber" type="hidden"
			value="<%=contractNumber%>" />
		<aui:input name="firstName" type="hidden" value="<%=firstName%>" />
		<aui:input name="middleName" type="hidden" value="<%=middleName%>" />
		<aui:input name="lastName" type="hidden" value="<%=lastName%>" />
	</div>
</aui:form>

<aui:form action="<%=addRecordActionURL%>" name="transferForm"
	method="POST">
	<div id="transfer" hidden="true">

		<liferay-ui:tabs names="Financial Advisor Transfer" refresh="false">
			<aui:select id="faTransOrgId1" name="" label="Select Region"
				onChange="getFaTransValues(this.value,0);" required="true"
				showRequiredLabel="false">
				<aui:option value=""> Select Region</aui:option>
				<c:forEach items="${regionOrganizationsList}" var="org">
					<aui:option value="${org.orgId}">${org.orgName}</aui:option>
				</c:forEach>
			</aui:select>

			<aui:select name="newOrgId1" id="faTransOrgId2" label="Select Branch"
				onChange="getFaTransValuess(this.value,0);" required="true"
				showRequiredLabel="false">
				<aui:option value="">Select Branch</aui:option>
			</aui:select>

			<aui:select name="newOrgId2" label="Select Unit" id="faTransOrgId3"
				required="true" showRequiredLabel="false">
				<aui:option value="">Select Unit</aui:option>
			</aui:select>

			<aui:input name="historyStartDate"
				value="${intermediary.historyESDate} " type="date"
				label="History Start Date" />

			<aui:select id="capacity" name="capacity" required="true"
				label="Select Capacity">
				<aui:option value="1">Confirmed</aui:option>
				<aui:option value="2">Acting</aui:option>
			</aui:select>

			<aui:input id="comments" name="comments" type="textarea"
				value="${compliance.comments}" />
		</liferay-ui:tabs>


		</br> </br> </br>

		<aui:button type="submit" name="submitRequest" value="Submit Request"></aui:button>


		<!-- hidden values must come fron local variables and not direct from previous page -->

		<aui:input name="applicationCategory" type="hidden"
			value="<%=catTransfer%>" />
		<aui:input name="approverAction" type="hidden" value="FATransfer" />
		<aui:input name="contractId" type="hidden" value="<%=contractId%>" />
		<aui:input name="entityId" type="hidden" value="<%=entityId%>" />
		<aui:input name="oldOrgId" type="hidden" value="<%=newOrgName1%>" />
		<aui:input name="oldPositionId" type="hidden"
			value="<%=newPositionName1%>" />

		<aui:input name="contractNumber" type="hidden"
			value="<%=contractNumber%>" />

		<aui:input name="firstName" type="hidden" value="<%=firstName%>" />
		<aui:input name="middleName" type="hidden" value="<%=middleName%>" />
		<aui:input name="lastName" type="hidden" value="<%=lastName%>" />

	</div>
</aui:form>


<aui:form action="<%=addRecordActionURL%>" name="suspensionForm"
	method="POST">
	<div id="suspension" hidden="true">
		<liferay-ui:tabs names="Financial Advisor Suspension" refresh="false">
			<aui:input id="comments" name="comments" type="textarea" />
		</liferay-ui:tabs>
		</br> </br> </br>
		<aui:button type="submit" name="submitRequest" value="Submit Request"></aui:button>
		<!-- hidden values must come fron local variables and not direct from previous page -->
		<aui:input name="applicationCategory" type="hidden"
			value="<%=catSuspension%>" />
		<aui:input name="approverAction" type="hidden" value="FASuspension" />
		<aui:input name="contractId" type="hidden" value="<%=contractId%>" />
		<aui:input name="entityId" type="hidden" value="<%=entityId%>" />
		<aui:input name="oldOrgId" type="hidden" value="<%=newOrgName1%>" />
		<aui:input name="oldPositionId" type="hidden"
			value="<%=newPositionName1%>" />

		<aui:input name="contractNumber" type="hidden"
			value="<%=contractNumber%>" />

		<aui:input name="firstName" type="hidden" value="<%=firstName%>" />
		<aui:input name="middleName" type="hidden" value="<%=middleName%>" />
		<aui:input name="lastName" type="hidden" value="<%=lastName%>" />
	</div>
</aui:form>

<!-- This is the dynamic drop down list javascript -->
<!-- This is region Manager Transer Scripts -->
<aui:script>
   
    function getRmTransValues(rmTransOrgId1, rmTransOrgId2) {
   // alert("Value: "+orgId);
        AUI()
                .use(
                        'aui-base',
                        'aui-io-request-deprecated',
                        function(A) {
                            A.io
                                    .request(
                                            '<%=getRmTransferURL%>',
                                            {
                                                dataType : 'json',
                                                method : 'GET',
                                                data : {
                                                    <portlet:namespace />CMD : 'rmTrans',
                                                    <portlet:namespace />rmTransOrgId1 : rmTransOrgId1
                                                },
                                                on : {
                                                    success : function(event,
                                                            id, obj) {
                                                        var instance = this;
                                                        var test = instance
                                                                .get('responseData');
                                                        if (test) {
                                                            var data = JSON
                                                                    .parse(test.rmTransList);
                                                            var selectTest = document
                                                                    .getElementById("<portlet:namespace />rmTransOrgId2");
                                                            selectTest.options.length = 0;
                                                            var option = document
                                                                    .createElement("option");
                                                            option.value = '';
                                                            option.text = 'Select';
                                                            selectTest.appendChild(option);
                                                            for (var i = 0; i < data.length; i++) {
                                                                option = document
                                                                        .createElement("option");
                                                                var datavalue = data[i]
                                                                        .split(":");
                                                                option.value = datavalue[0];
                                                                option.text = datavalue[1];
                                                                selectTest
                                                                        .appendChild(option);
                                                            }
                                                            selectTest.value=rmTransOrgId2;
                                                        }
                                                    }
                                                }
                                            });
                        })
    }
   
   
</aui:script>


<aui:script>
   
    function getRmTransValuess(rmTransOrgId2, rmTransOrgId3) {
    //alert("Value: "+rmTransOrgId2);
        AUI()
                .use(
                        'aui-base',
                        'aui-io-request-deprecated',
                        function(A) {
                            A.io
                                    .request(
                                            '<%=getRmTransferURL%>',
                                            {
                                                dataType : 'json',
                                                method : 'GET',
                                                data : {
                                                    <portlet:namespace />CMD : 'rmTranss',
                                                    <portlet:namespace />rmTransOrgId2 : rmTransOrgId2
                                                },
                                                on : {
                                                    success : function(event,
                                                            id, obj) {
                                                        var instance = this;
                                                        var test = instance
                                                                .get('responseData');
                                                        if (test) {
                                                            var data = JSON
                                                                    .parse(test.rmTransListt);
                                                            var selectTest = document
                                                                    .getElementById("<portlet:namespace />rmTransOrgId3");
                                                            selectTest.options.length = 0;
                                                            var option = document
                                                                    .createElement("option");
                                                            option.value = '';
                                                            option.text = 'Select';
                                                            selectTest.appendChild(option);
                                                            for (var i = 0; i < data.length; i++) {
                                                                option = document
                                                                        .createElement("option");
                                                                var datavalue = data[i]
                                                                        .split(":");
                                                                option.value = datavalue[0];
                                                                option.text = datavalue[1];
                                                                selectTest
                                                                        .appendChild(option);
                                                            }
                                                            selectTest.value=rmTransOrgId3;
                                                        }
                                                    }
                                                }
                                            });
                        })
    }
   
   
</aui:script>
<!-- End of Rm transfer Scripts -->


<!-- This is BM transfer Scripts -->
<aui:script>
function getBmTransValues(bmTransOrgId1, bmTransOrgId2) {

        AUI()
                .use(
                        'aui-base',
                        'aui-io-request-deprecated',
                        function(A) {
                       
                            A.io
                                    .request(
                                            '<%=getBmTransferURL%>',
                                            {
                                                dataType : 'json',
                                                method : 'GET',
                                                data : {
                                                    <portlet:namespace />CMD : 'bmTrans',
                                                    <portlet:namespace />bmTransOrgId1 : bmTransOrgId1
                                                },
                                                on : {
                                                    success : function(event,
                                                            id, obj) {
                                                        var instance = this;
                                                        var test = instance
                                                                .get('responseData');
                                                        if (test) {
                                                            var data = JSON
                                                                    .parse(test.bmTransList);
                                                            var selectTest = document
                                                                    .getElementById("<portlet:namespace />bmTransOrgId2");
                                                            selectTest.options.length = 0;
                                                            var option = document
                                                                    .createElement("option");
                                                            option.value = '';
                                                            option.text = 'Select';
                                                            selectTest.appendChild(option);
                                                            for (var i = 0; i < data.length; i++) {
                                                                option = document
                                                                        .createElement("option");
                                                                var datavalue = data[i]
                                                                        .split(":");
                                                                option.value = datavalue[0];
                                                                option.text = datavalue[1];
                                                                 //alert("Value after function: "+branchId+datavalue[0]);
                                                                selectTest
                                                                        .appendChild(option);
                                                            }
                                                            selectTest.value=bmTransOrgId2;
                                                        }
                                                    }
                                                }
                                            });
                        })
    }
</aui:script>
<aui:script>
function getBmTransValuess(bmTransOrgId2, bmTransOrgId3) {
//alert("This "+bmTransOrgId2);
        AUI()
                .use(
                        'aui-base',
                        'aui-io-request-deprecated',
                        function(A) {
                       
                            A.io
                                    .request(
                                            '<%=getBmTransferURL%>',
                                            {
                                                dataType : 'json',
                                                method : 'GET',
                                                data : {
                                                    <portlet:namespace />CMD : 'bmTranss',
                                                    <portlet:namespace />bmTransOrgId2 : bmTransOrgId2
                                                },
                                                on : {
                                                    success : function(event,
                                                            id, obj) {
                                                        var instance = this;
                                                        var test = instance
                                                                .get('responseData');
                                                        if (test) {
                                                            var data = JSON
                                                                    .parse(test.bmTransListt);
                                                            var selectTest = document
                                                                    .getElementById("<portlet:namespace />bmTransOrgId3");
                                                            selectTest.options.length = 0;
                                                            var option = document
                                                                    .createElement("option");
                                                            option.value = '';
                                                            option.text = 'Select';
                                                            selectTest.appendChild(option);
                                                            for (var i = 0; i < data.length; i++) {
                                                                option = document
                                                                        .createElement("option");
                                                                var datavalue = data[i]
                                                                        .split(":");
                                                                option.value = datavalue[0];
                                                                option.text = datavalue[1];
                                                                 //alert("Value after function: "+branchId+datavalue[0]);
                                                                selectTest
                                                                        .appendChild(option);
                                                            }
                                                            selectTest.value=bmTransOrgId3;
                                                        }
                                                    }
                                                }
                                            });
                        })
    }
</aui:script>
<!-- End of Bm transfer scripts -->

<!-- Um Transfer Scripts -->
<aui:script>
function getUmTransValues(umTransOrgId1, umTransOrgId2) {

        AUI()
                .use(
                        'aui-base',
                        'aui-io-request-deprecated',
                        function(A) {
                       
                            A.io
                                    .request(
                                            '<%=getUmTransferURL%>',
                                            {
                                                dataType : 'json',
                                                method : 'GET',
                                                data : {
                                                    <portlet:namespace />CMD : 'umTrans',
                                                    <portlet:namespace />umTransOrgId1 : umTransOrgId1
                                                },
                                                on : {
                                                    success : function(event,
                                                            id, obj) {
                                                        var instance = this;
                                                        var test = instance
                                                                .get('responseData');
                                                        if (test) {
                                                            var data = JSON
                                                                    .parse(test.umTransList);
                                                            var selectTest = document
                                                                    .getElementById("<portlet:namespace />umTransOrgId2");
                                                            selectTest.options.length = 0;
                                                            var option = document
                                                                    .createElement("option");
                                                            option.value = '';
                                                            option.text = 'Select';
                                                            selectTest.appendChild(option);
                                                            for (var i = 0; i < data.length; i++) {
                                                                option = document
                                                                        .createElement("option");
                                                                var datavalue = data[i]
                                                                        .split(":");
                                                                option.value = datavalue[0];
                                                                option.text = datavalue[1];
                                                                 //alert("Value after function: "+branchId+datavalue[0]);
                                                                selectTest
                                                                        .appendChild(option);
                                                            }
                                                            selectTest.value=umTransOrgId2;
                                                        }
                                                    }
                                                }
                                            });
                        })
    }
</aui:script>

<aui:script>
function getUmTransValuess(umTransOrgId2, umTransOrgId3) {

        AUI()
                .use(
                        'aui-base',
                        'aui-io-request-deprecated',
                        function(A) {
                       
                            A.io
                                    .request(
                                            '<%=getUmTransferURL%>',
                                            {
                                                dataType : 'json',
                                                method : 'GET',
                                                data : {
                                                    <portlet:namespace />CMD : 'umTranss',
                                                    <portlet:namespace />umTransOrgId2 : umTransOrgId2
                                                },
                                                on : {
                                                    success : function(event,
                                                            id, obj) {
                                                        var instance = this;
                                                        var test = instance
                                                                .get('responseData');
                                                        if (test) {
                                                            var data = JSON
                                                                    .parse(test.umTransListt);
                                                            var selectTest = document
                                                                    .getElementById("<portlet:namespace />umTransOrgId3");
                                                            selectTest.options.length = 0;
                                                            var option = document
                                                                    .createElement("option");
                                                            option.value = '';
                                                            option.text = 'Select';
                                                            selectTest.appendChild(option);
                                                            for (var i = 0; i < data.length; i++) {
                                                                option = document
                                                                        .createElement("option");
                                                                var datavalue = data[i]
                                                                        .split(":");
                                                                option.value = datavalue[0];
                                                                option.text = datavalue[1];
                                                                 //alert("Value after function: "+branchId+datavalue[0]);
                                                                selectTest
                                                                        .appendChild(option);
                                                            }
                                                            selectTest.value=umTransOrgId3;
                                                        }
                                                    }
                                                }
                                            });
                        })
    }
</aui:script>
<!-- End of Um transfer script -->

<!-- This is Fa transfer Script -->
<aui:script>
function getFaTransValues(faTransOrgId1, faTransOrgId2) {

        AUI()
                .use(
                        'aui-base',
                        'aui-io-request-deprecated',
                        function(A) {
                       
                            A.io
                                    .request(
                                            '<%=getFaTransferURL%>',
                                            {
                                                dataType : 'json',
                                                method : 'GET',
                                                data : {
                                                    <portlet:namespace />CMD : 'faTrans',
                                                    <portlet:namespace />faTransOrgId1 : faTransOrgId1
                                                },
                                                on : {
                                                    success : function(event,
                                                            id, obj) {
                                                        var instance = this;
                                                        var test = instance
                                                                .get('responseData');
                                                        if (test) {
                                                            var data = JSON
                                                                    .parse(test.faTransList);
                                                            var selectTest = document
                                                                    .getElementById("<portlet:namespace />faTransOrgId2");
                                                            selectTest.options.length = 0;
                                                            var option = document
                                                                    .createElement("option");
                                                            option.value = '';
                                                            option.text = 'Select';
                                                            selectTest.appendChild(option);
                                                            for (var i = 0; i < data.length; i++) {
                                                                option = document
                                                                        .createElement("option");
                                                                var datavalue = data[i]
                                                                        .split(":");
                                                                option.value = datavalue[0];
                                                                option.text = datavalue[1];
                                                                 //alert("Value after function: "+branchId+datavalue[0]);
                                                                selectTest
                                                                        .appendChild(option);
                                                            }
                                                            selectTest.value=faTransOrgId2;
                                                        }
                                                    }
                                                }
                                            });
                        })
    }
</aui:script>
<aui:script>
function getFaTransValuess(faTransOrgId2, faTransOrgId3) {
//alert("This is Fa"+faTransOrgId2);
        AUI()
                .use(
                        'aui-base',
                        'aui-io-request-deprecated',
                        function(A) {
                       
                            A.io
                                    .request(
                                            '<%=getFaTransferURL%>',
                                            {
                                                dataType : 'json',
                                                method : 'GET',
                                                data : {
                                                    <portlet:namespace />CMD : 'faTranss',
                                                    <portlet:namespace />faTransOrgId2 : faTransOrgId2
                                                },
                                                on : {
                                                    success : function(event,
                                                            id, obj) {
                                                        var instance = this;
                                                        var test = instance
                                                                .get('responseData');
                                                        if (test) {
                                                            var data = JSON
                                                                    .parse(test.faTransListt);
                                                            //alert(data);
                                                            var selectTest = document
                                                                    .getElementById("<portlet:namespace />faTransOrgId3");
                                                            selectTest.options.length = 0;
                                                            var option = document
                                                                    .createElement("option");
                                                            option.value = '';
                                                            option.text = 'Select';
                                                            selectTest.appendChild(option);
                                                            for (var i = 0; i < data.length; i++) {
                                                                option = document
                                                                        .createElement("option");
                                                                var datavalue = data[i]
                                                                        .split(":");
                                                                option.value = datavalue[0];
                                                                option.text = datavalue[1];
                                                                 //alert("Value after function: "+branchId+datavalue[0]);
                                                                selectTest
                                                                        .appendChild(option);
                                                            }
                                                            selectTest.value=faTransOrgId3;
                                                        }
                                                    }
                                                }
                                            });
                        })
    }
</aui:script>
<!-- End of Fa transfer List -->



<!-- Promotion Section -->
<!-- This is region Manager Promoer Scripts -->
<aui:script>
   
    function getRmPromoValues(rmPromoOrgId1, rmPromoOrgId2) {
   // alert("Value: "+orgId);
        AUI()
                .use(
                        'aui-base',
                        'aui-io-request-deprecated',
                        function(A) {
                            A.io
                                    .request(
                                            '<%=getRmPromoferURL%>',
                                            {
                                                dataType : 'json',
                                                method : 'GET',
                                                data : {
                                                    <portlet:namespace />CMD : 'rmPromo',
                                                    <portlet:namespace />rmPromoOrgId1 : rmPromoOrgId1
                                                },
                                                on : {
                                                    success : function(event,
                                                            id, obj) {
                                                        var instance = this;
                                                        var test = instance
                                                                .get('responseData');
                                                        if (test) {
                                                            var data = JSON
                                                                    .parse(test.rmPromoList);
                                                            var selectTest = document
                                                                    .getElementById("<portlet:namespace />rmPromoOrgId2");
                                                            selectTest.options.length = 0;
                                                            var option = document
                                                                    .createElement("option");
                                                            option.value = '';
                                                            option.text = 'Select';
                                                            selectTest.appendChild(option);
                                                            for (var i = 0; i < data.length; i++) {
                                                                option = document
                                                                        .createElement("option");
                                                                var datavalue = data[i]
                                                                        .split(":");
                                                                option.value = datavalue[0];
                                                                option.text = datavalue[1];
                                                                selectTest
                                                                        .appendChild(option);
                                                            }
                                                            selectTest.value=rmPromoOrgId2;
                                                        }
                                                    }
                                                }
                                            });
                        })
    }
   
   
</aui:script>


<aui:script>
   
    function getRmPromoValuess(rmPromoOrgId2, rmPromoOrgId3) {
    //alert("Value: "+rmPromoOrgId2);
        AUI()
                .use(
                        'aui-base',
                        'aui-io-request-deprecated',
                        function(A) {
                            A.io
                                    .request(
                                            '<%=getRmPromoferURL%>',
                                            {
                                                dataType : 'json',
                                                method : 'GET',
                                                data : {
                                                    <portlet:namespace />CMD : 'rmPromos',
                                                    <portlet:namespace />rmPromoOrgId2 : rmPromoOrgId2
                                                },
                                                on : {
                                                    success : function(event,
                                                            id, obj) {
                                                        var instance = this;
                                                        var test = instance
                                                                .get('responseData');
                                                        if (test) {
                                                            var data = JSON
                                                                    .parse(test.rmPromoListt);
                                                            var selectTest = document
                                                                    .getElementById("<portlet:namespace />rmPromoOrgId3");
                                                            selectTest.options.length = 0;
                                                            var option = document
                                                                    .createElement("option");
                                                            option.value = '';
                                                            option.text = 'Select';
                                                            selectTest.appendChild(option);
                                                            for (var i = 0; i < data.length; i++) {
                                                                option = document
                                                                        .createElement("option");
                                                                var datavalue = data[i]
                                                                        .split(":");
                                                                option.value = datavalue[0];
                                                                option.text = datavalue[1];
                                                                selectTest
                                                                        .appendChild(option);
                                                            }
                                                            selectTest.value=rmPromoOrgId3;
                                                        }
                                                    }
                                                }
                                            });
                        })
    }
   
   
</aui:script>
<!-- End of Rm Promofer Scripts -->


<!-- This is BM Promofer Scripts -->
<aui:script>
function getBmPromoValues(bmPromoOrgId1, bmPromoOrgId2) {

        AUI()
                .use(
                        'aui-base',
                        'aui-io-request-deprecated',
                        function(A) {
                       
                            A.io
                                    .request(
                                            '<%=getBmPromoferURL%>',
                                            {
                                                dataType : 'json',
                                                method : 'GET',
                                                data : {
                                                    <portlet:namespace />CMD : 'bmPromo',
                                                    <portlet:namespace />bmPromoOrgId1 : bmPromoOrgId1
                                                },
                                                on : {
                                                    success : function(event,
                                                            id, obj) {
                                                        var instance = this;
                                                        var test = instance
                                                                .get('responseData');
                                                        if (test) {
                                                            var data = JSON
                                                                    .parse(test.bmPromoList);
                                                            var selectTest = document
                                                                    .getElementById("<portlet:namespace />bmPromoOrgId2");
                                                            selectTest.options.length = 0;
                                                            var option = document
                                                                    .createElement("option");
                                                            option.value = '';
                                                            option.text = 'Select';
                                                            selectTest.appendChild(option);
                                                            for (var i = 0; i < data.length; i++) {
                                                                option = document
                                                                        .createElement("option");
                                                                var datavalue = data[i]
                                                                        .split(":");
                                                                option.value = datavalue[0];
                                                                option.text = datavalue[1];
                                                                 //alert("Value after function: "+branchId+datavalue[0]);
                                                                selectTest
                                                                        .appendChild(option);
                                                            }
                                                            selectTest.value=bmPromoOrgId2;
                                                        }
                                                    }
                                                }
                                            });
                        })
    }
</aui:script>
<aui:script>
function getBmPromoValuess(bmPromoOrgId2, bmPromoOrgId3) {
//alert("This "+bmPromoOrgId2);
        AUI()
                .use(
                        'aui-base',
                        'aui-io-request-deprecated',
                        function(A) {
                       
                            A.io
                                    .request(
                                            '<%=getBmPromoferURL%>',
                                            {
                                                dataType : 'json',
                                                method : 'GET',
                                                data : {
                                                    <portlet:namespace />CMD : 'bmPromos',
                                                    <portlet:namespace />bmPromoOrgId2 : bmPromoOrgId2
                                                },
                                                on : {
                                                    success : function(event,
                                                            id, obj) {
                                                        var instance = this;
                                                        var test = instance
                                                                .get('responseData');
                                                        if (test) {
                                                            var data = JSON
                                                                    .parse(test.bmPromoListt);
                                                            var selectTest = document
                                                                    .getElementById("<portlet:namespace />bmPromoOrgId3");
                                                            selectTest.options.length = 0;
                                                            var option = document
                                                                    .createElement("option");
                                                            option.value = '';
                                                            option.text = 'Select';
                                                            selectTest.appendChild(option);
                                                            for (var i = 0; i < data.length; i++) {
                                                                option = document
                                                                        .createElement("option");
                                                                var datavalue = data[i]
                                                                        .split(":");
                                                                option.value = datavalue[0];
                                                                option.text = datavalue[1];
                                                                 //alert("Value after function: "+branchId+datavalue[0]);
                                                                selectTest
                                                                        .appendChild(option);
                                                            }
                                                            selectTest.value=bmPromoOrgId3;
                                                        }
                                                    }
                                                }
                                            });
                        })
    }
</aui:script>
<!-- End of Bm Promofer scripts -->

<!-- Um Promofer Scripts -->
<aui:script>
function getUmPromoValues(umPromoOrgId1, umPromoOrgId2) {

        AUI()
                .use(
                        'aui-base',
                        'aui-io-request-deprecated',
                        function(A) {
                       
                            A.io
                                    .request(
                                            '<%=getUmPromoferURL%>',
                                            {
                                                dataType : 'json',
                                                method : 'GET',
                                                data : {
                                                    <portlet:namespace />CMD : 'umPromo',
                                                    <portlet:namespace />umPromoOrgId1 : umPromoOrgId1
                                                },
                                                on : {
                                                    success : function(event,
                                                            id, obj) {
                                                        var instance = this;
                                                        var test = instance
                                                                .get('responseData');
                                                        if (test) {
                                                            var data = JSON
                                                                    .parse(test.umPromoList);
                                                            var selectTest = document
                                                                    .getElementById("<portlet:namespace />umPromoOrgId2");
                                                            selectTest.options.length = 0;
                                                            var option = document
                                                                    .createElement("option");
                                                            option.value = '';
                                                            option.text = 'Select';
                                                            selectTest.appendChild(option);
                                                            for (var i = 0; i < data.length; i++) {
                                                                option = document
                                                                        .createElement("option");
                                                                var datavalue = data[i]
                                                                        .split(":");
                                                                option.value = datavalue[0];
                                                                option.text = datavalue[1];
                                                                 //alert("Value after function: "+branchId+datavalue[0]);
                                                                selectTest
                                                                        .appendChild(option);
                                                            }
                                                            selectTest.value=umPromoOrgId2;
                                                        }
                                                    }
                                                }
                                            });
                        })
    }
</aui:script>

<aui:script>
function getUmPromoValuess(umPromoOrgId2, umPromoOrgId3) {

        AUI()
                .use(
                        'aui-base',
                        'aui-io-request-deprecated',
                        function(A) {
                       
                            A.io
                                    .request(
                                            '<%=getUmPromoferURL%>',
                                            {
                                                dataType : 'json',
                                                method : 'GET',
                                                data : {
                                                    <portlet:namespace />CMD : 'umPromos',
                                                    <portlet:namespace />umPromoOrgId2 : umPromoOrgId2
                                                },
                                                on : {
                                                    success : function(event,
                                                            id, obj) {
                                                        var instance = this;
                                                        var test = instance
                                                                .get('responseData');
                                                        if (test) {
                                                            var data = JSON
                                                                    .parse(test.umPromoListt);
                                                            var selectTest = document
                                                                    .getElementById("<portlet:namespace />umPromoOrgId3");
                                                            selectTest.options.length = 0;
                                                            var option = document
                                                                    .createElement("option");
                                                            option.value = '';
                                                            option.text = 'Select';
                                                            selectTest.appendChild(option);
                                                            for (var i = 0; i < data.length; i++) {
                                                                option = document
                                                                        .createElement("option");
                                                                var datavalue = data[i]
                                                                        .split(":");
                                                                option.value = datavalue[0];
                                                                option.text = datavalue[1];
                                                                 //alert("Value after function: "+branchId+datavalue[0]);
                                                                selectTest
                                                                        .appendChild(option);
                                                            }
                                                            selectTest.value=umPromoOrgId3;
                                                        }
                                                    }
                                                }
                                            });
                        })
    }
</aui:script>
<!-- End of Um Promofer script -->

<!-- This is Fa Promofer Script -->
<aui:script>
function getFaPromoValues(faPromoOrgId1, faPromoOrgId2) {

        AUI()
                .use(
                        'aui-base',
                        'aui-io-request-deprecated',
                        function(A) {
                       
                            A.io
                                    .request(
                                            '<%=getFaPromoferURL%>',
                                            {
                                                dataType : 'json',
                                                method : 'GET',
                                                data : {
                                                    <portlet:namespace />CMD : 'faPromo',
                                                    <portlet:namespace />faPromoOrgId1 : faPromoOrgId1
                                                },
                                                on : {
                                                    success : function(event,
                                                            id, obj) {
                                                        var instance = this;
                                                        var test = instance
                                                                .get('responseData');
                                                        if (test) {
                                                            var data = JSON
                                                                    .parse(test.faPromoList);
                                                            var selectTest = document
                                                                    .getElementById("<portlet:namespace />faPromoOrgId2");
                                                            selectTest.options.length = 0;
                                                            var option = document
                                                                    .createElement("option");
                                                            option.value = '';
                                                            option.text = 'Select';
                                                            selectTest.appendChild(option);
                                                            for (var i = 0; i < data.length; i++) {
                                                                option = document
                                                                        .createElement("option");
                                                                var datavalue = data[i]
                                                                        .split(":");
                                                                option.value = datavalue[0];
                                                                option.text = datavalue[1];
                                                                 //alert("Value after function: "+branchId+datavalue[0]);
                                                                selectTest
                                                                        .appendChild(option);
                                                            }
                                                            selectTest.value=faPromoOrgId2;
                                                        }
                                                    }
                                                }
                                            });
                        })
    }
</aui:script>
<aui:script>
function getFaPromoValuess(faPromoOrgId2, faPromoOrgId3) {
//alert("This is Fa"+faPromoOrgId2);
        AUI()
                .use(
                        'aui-base',
                        'aui-io-request-deprecated',
                        function(A) {
                       
                            A.io
                                    .request(
                                            '<%=getFaPromoferURL%>',
                                            {
                                                dataType : 'json',
                                                method : 'GET',
                                                data : {
                                                    <portlet:namespace />CMD : 'faPromos',
                                                    <portlet:namespace />faPromoOrgId2 : faPromoOrgId2
                                                },
                                                on : {
                                                    success : function(event,
                                                            id, obj) {
                                                        var instance = this;
                                                        var test = instance
                                                                .get('responseData');
                                                        if (test) {
                                                            var data = JSON
                                                                    .parse(test.faPromoListt);
                                                            //alert(data);
                                                            var selectTest = document
                                                                    .getElementById("<portlet:namespace />faPromoOrgId3");
                                                            selectTest.options.length = 0;
                                                            var option = document
                                                                    .createElement("option");
                                                            option.value = '';
                                                            option.text = 'Select';
                                                            selectTest.appendChild(option);
                                                            for (var i = 0; i < data.length; i++) {
                                                                option = document
                                                                        .createElement("option");
                                                                var datavalue = data[i]
                                                                        .split(":");
                                                                option.value = datavalue[0];
                                                                option.text = datavalue[1];
                                                                 //alert("Value after function: "+branchId+datavalue[0]);
                                                                selectTest
                                                                        .appendChild(option);
                                                            }
                                                            selectTest.value=faPromoOrgId3;
                                                        }
                                                    }
                                                }
                                            });
                        })
    }
</aui:script>
<!-- End of Fa Promofer List -->
<!-- This is the end of the dynamic drop down list javascript -->



<aui:script use="aui-char-counter">
AUI().use(
  function(A) {
    new A.CharCounter(
      {
        counter: '#counter',
        input: '#<portlet:namespace />comments',
        maxLength: 1000
      }
    );
  }
);
</aui:script>

<aui:script use="aui-base">
    var applicationCategory= A.one("#<portlet:namespace />applicationCategory");
    applicationCategory.on(
        "change",
        function(event) {
            applicationCategory.all("option:selected").each(
            function(node) {
                    var selected = node.val();
<!--This is where we initilize the switching div options  -->
var promotion = A.one('#promotion');
var termination = A.one('#termination');
var transfer = A.one('#transfer');
var rmtransfer = A.one('#rmtransfer');
var bmtransfer = A.one('#bmtransfer');
var suspension = A.one('#suspension');
var reinstatement = A.one('#reinstatement');
var bmpromotion = A.one('#bmpromotion');
var fapromotion = A.one('#fapromotion');
var umpromotion = A.one('#umpromotion');
var umreinstatement = A.one('#umreinstatement');
var umtransfer = A.one('#umtransfer');
var umsuspension = A.one('#umsuspension');
var umtermination = A.one('#umtermination');
var fareinstatement=A.one('#fareinstatement');
<!-- New Items 05/10/2020 -->
	<!-- var fatransfer = A.one('#fatransfer'); -->
                    if (selected == "FATermination") {
                        termination.attr('hidden', false);
                        promotion.attr('hidden', true);
                        transfer.attr('hidden', true);
                        suspension.attr('hidden', true);
                        reinstatement.attr('hidden', true);
                        bmpromotion.attr('hidden', true);
                        fapromotion.attr('hidden', true);
                        rmtransfer.attr('hidden', true);
                        bmtransfer.attr('hidden', true);
                        umpromotion.attr('hidden', true);
                        umtransfer.attr('hidden', true);
                        umreinstatement.attr('hidden', true);  
                        umtermination.attr('hidden', true);  
umsuspension.attr('hidden', true);  fareinstatement.attr('hidden', true);
                    } else if (selected == "Promotion"){
promotion.attr('hidden', false);
termination.attr('hidden', true);
transfer.attr('hidden', true);
                        suspension.attr('hidden', true);
                        reinstatement.attr('hidden', true);
                        bmpromotion.attr('hidden', true);
                        fapromotion.attr('hidden', true);
                        rmtransfer.attr('hidden', true);
                        bmtransfer.attr('hidden', true);
                        umpromotion.attr('hidden', true);
                        umtransfer.attr('hidden', true);
                        umreinstatement.attr('hidden', true);  
                        umtermination.attr('hidden', true);  
umsuspension.attr('hidden', true);  fareinstatement.attr('hidden', true);
                     } else if (selected == "BMPromotion"){
                        bmpromotion.attr('hidden', false);
promotion.attr('hidden', true);
termination.attr('hidden', true);
transfer.attr('hidden', true);
                        suspension.attr('hidden', true);
                        reinstatement.attr('hidden', true);
                        fapromotion.attr('hidden', true);
                        rmtransfer.attr('hidden', true);
                        bmtransfer.attr('hidden', true);
                        umpromotion.attr('hidden', true);
                        umtransfer.attr('hidden', true);
                        umreinstatement.attr('hidden', true);  
                  umtermination.attr('hidden', true);  fareinstatement.attr('hidden', true);
umsuspension.attr('hidden', true);  
                    }else if (selected == "UMPromotion"){
                        bmpromotion.attr('hidden', true);
promotion.attr('hidden', true);
termination.attr('hidden', true);
transfer.attr('hidden', true);
                        suspension.attr('hidden', true);
                        reinstatement.attr('hidden', true);
                        fapromotion.attr('hidden', true);
                        rmtransfer.attr('hidden', true);
                        bmtransfer.attr('hidden', true);
                        umpromotion.attr('hidden', false);
                        umtransfer.attr('hidden', true);
                        umreinstatement.attr('hidden', true);  
                    umtermination.attr('hidden', true);  fareinstatement.attr('hidden', true);
umsuspension.attr('hidden', true);  
                    } else if (selected == "FAPromotion"){
                        fapromotion.attr('hidden', false);
                        bmpromotion.attr('hidden', true);
promotion.attr('hidden', true);
termination.attr('hidden', true);
transfer.attr('hidden', true);
                        suspension.attr('hidden', true);
                        reinstatement.attr('hidden', true);
                        rmtransfer.attr('hidden', true);
                        bmtransfer.attr('hidden', true);
                        umpromotion.attr('hidden', true);
                        umtransfer.attr('hidden', true);
                        umreinstatement.attr('hidden', true);
                        umtermination.attr('hidden', true); fareinstatement.attr('hidden', true);
umsuspension.attr('hidden', true);    
                       
}else if (selected == "FATransfer"){
promotion.attr('hidden', true);
termination.attr('hidden', true);
transfer.attr('hidden', false);
                        suspension.attr('hidden', true);
                        reinstatement.attr('hidden', true);
                        bmpromotion.attr('hidden', true);
                        fapromotion.attr('hidden', true);
                        rmtransfer.attr('hidden', true);
                        bmtransfer.attr('hidden', true);
                        umpromotion.attr('hidden', true);
                        umreinstatement.attr('hidden', true);  
                        umtransfer.attr('hidden', true);
                        umtermination.attr('hidden', true); fareinstatement.attr('hidden', true);
umsuspension.attr('hidden', true);  
                         
}else if (selected == "RMTransfer"){
promotion.attr('hidden', true);
termination.attr('hidden', true);
rmtransfer.attr('hidden', false);
                        suspension.attr('hidden', true);
                        reinstatement.attr('hidden', true);
                        bmpromotion.attr('hidden', true);
                        fapromotion.attr('hidden', true);
                        transfer.attr('hidden', true);
                        bmtransfer.attr('hidden', true);
                       umpromotion.attr('hidden', true);
                       umtransfer.attr('hidden', true);
                       umreinstatement.attr('hidden', true);fareinstatement.attr('hidden', true);  
                       umtermination.attr('hidden', true);  
umsuspension.attr('hidden', true);  
}else if (selected == "FAReinstatement"){
promotion.attr('hidden', true);
termination.attr('hidden', true);
rmtransfer.attr('hidden', true);
fareinstatement.attr('hidden', false);
                        suspension.attr('hidden', true);
                        reinstatement.attr('hidden', true);
                        bmpromotion.attr('hidden', true);
                        fapromotion.attr('hidden', true);
                        transfer.attr('hidden', true);
                        bmtransfer.attr('hidden', true);
                       umpromotion.attr('hidden', true);
                       umtransfer.attr('hidden', true);
                       umreinstatement.attr('hidden', true);  
                       umtermination.attr('hidden', true);  
umsuspension.attr('hidden', true);
                         
}else if (selected == "BMTransfer"){
promotion.attr('hidden', true);
termination.attr('hidden', true);
rmtransfer.attr('hidden', true);
                        suspension.attr('hidden', true);
                        reinstatement.attr('hidden', true);
                        bmpromotion.attr('hidden', true);
                        fapromotion.attr('hidden', true);
                        transfer.attr('hidden', true);
                        bmtransfer.attr('hidden', false);
                        umpromotion.attr('hidden', true);  
                        umtransfer.attr('hidden', true);    
                        umreinstatement.attr('hidden', true); fareinstatement.attr('hidden', true);
                        umtermination.attr('hidden', true);  
umsuspension.attr('hidden', true);                  
                         
}else if (selected == "UMTransfer"){
promotion.attr('hidden', true);
termination.attr('hidden', true);
rmtransfer.attr('hidden', true);
                        suspension.attr('hidden', true);
                        reinstatement.attr('hidden', true);
                        bmpromotion.attr('hidden', true);
                        fapromotion.attr('hidden', true);
                        transfer.attr('hidden', true);
                        bmtransfer.attr('hidden', true);
                        umpromotion.attr('hidden', true);  
                        umtransfer.attr('hidden', false);  
                        umreinstatement.attr('hidden', true);  fareinstatement.attr('hidden', true);
                        umtermination.attr('hidden', true);  
umsuspension.attr('hidden', true);                  
                         
}else if (selected == "UMReinstatement"){
umreinstatement.attr('hidden', false);  
promotion.attr('hidden', true);
termination.attr('hidden', true);
rmtransfer.attr('hidden', true);
                        suspension.attr('hidden', true);
                        reinstatement.attr('hidden', true);
                        bmpromotion.attr('hidden', true);
                        fapromotion.attr('hidden', true); fareinstatement.attr('hidden', true);
                        transfer.attr('hidden', true);
                        bmtransfer.attr('hidden', true);
                        umpromotion.attr('hidden', true);  
                        umtransfer.attr('hidden', true);  
                        umtermination.attr('hidden', true);  
umsuspension.attr('hidden', true);  
                                         
                         
}else if (selected == "FASuspension"){
promotion.attr('hidden', true);
termination.attr('hidden', true);
transfer.attr('hidden', true);
                        suspension.attr('hidden', false);
                        reinstatement.attr('hidden', true);
                        bmpromotion.attr('hidden', true);
                        fapromotion.attr('hidden', true); fareinstatement.attr('hidden', true);
                        rmtransfer.attr('hidden', true);
                        bmtransfer.attr('hidden', true);
                        umpromotion.attr('hidden', true);
                        umtransfer.attr('hidden', true);
                        umreinstatement.attr('hidden', true);
                        umtermination.attr('hidden', true);  
umsuspension.attr('hidden', true);    
                         
}
else if (selected == "Reinstatement"){
promotion.attr('hidden', true);
termination.attr('hidden', true);
transfer.attr('hidden', true);
                        suspension.attr('hidden', true);
                        reinstatement.attr('hidden', false);
                        bmpromotion.attr('hidden', true);
                        fapromotion.attr('hidden', true);
                        rmtransfer.attr('hidden', true);
                        bmtransfer.attr('hidden', true);
                        umpromotion.attr('hidden', true);
                        umtransfer.attr('hidden', true);
                        umreinstatement.attr('hidden', true);fareinstatement.attr('hidden', true);
                        umtermination.attr('hidden', true);  
umsuspension.attr('hidden', true);    
}else if (selected == "UMTermination"){
                        bmpromotion.attr('hidden', true);
promotion.attr('hidden', true);
termination.attr('hidden', true);
transfer.attr('hidden', true);
                        suspension.attr('hidden', true);
                        reinstatement.attr('hidden', true);
                        fapromotion.attr('hidden', true);
                        rmtransfer.attr('hidden', true);
                        bmtransfer.attr('hidden', true);
                        umpromotion.attr('hidden', true); fareinstatement.attr('hidden', true);
                        umtransfer.attr('hidden', true);
                        umreinstatement.attr('hidden', true);  
                        umtermination.attr('hidden', false);  
                        umsuspension.attr('hidden', true);  
                   
                    }else if (selected == "UMSuspension"){
                        bmpromotion.attr('hidden', true);
promotion.attr('hidden', true);
termination.attr('hidden', true);
transfer.attr('hidden', true);
                        suspension.attr('hidden', true);
                        reinstatement.attr('hidden', true);
                        fapromotion.attr('hidden', true);
                        rmtransfer.attr('hidden', true);
                        bmtransfer.attr('hidden', true); fareinstatement.attr('hidden', true);
                        umpromotion.attr('hidden', true);
                        umtransfer.attr('hidden', true);
                        umreinstatement.attr('hidden', true);  
                        umtermination.attr('hidden', true);  
                        umsuspension.attr('hidden', false);  
                   
                    }
                    else {
                        promotion.attr('hidden', true);
                        termination.attr('hidden', true);
                        transfer.attr('hidden', true);
                        suspension.attr('hidden', true);
                        reinstatement.attr('hidden', true);
                        bmpromotion.attr('hidden', true);
                        fapromotion.attr('hidden', true);fareinstatement.attr('hidden', true);
                        rmtransfer.attr('hidden', true);
                        bmtransfer.attr('hidden', true);
                        umpromotion.attr('hidden', true);
                        umtransfer.attr('hidden', true);
                        umreinstatement.attr('hidden', true);  
                    }
                }
            );
        }
    );
</aui:script>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script
	src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>