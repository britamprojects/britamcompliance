<%@ include file="init.jsp"%>

<liferay-portlet:renderURL var="postRequestUrl">
	<portlet:param name="mvcPath" value="/view.jsp" />
	<portlet:param name="selectedMenu" value="postRequests" />
</liferay-portlet:renderURL>

<liferay-portlet:renderURL var="requestApprovalUrl">
	<portlet:param name="mvcPath" value="/approvals.jsp" />
	<portlet:param name="selectedMenu" value="requestsApproval" />
</liferay-portlet:renderURL>

<liferay-portlet:renderURL var="requestsOnHoldUrl">
	<portlet:param name="mvcPath" value="/more-info.jsp" />
	<portlet:param name="selectedMenu" value="requestsOnHold" />
</liferay-portlet:renderURL>

<liferay-portlet:renderURL var="approvedRequestsUrl">
	<portlet:param name="mvcPath" value="/approved-requests.jsp" />
	<portlet:param name="selectedMenu" value="approvedRequests" />
</liferay-portlet:renderURL>

<liferay-portlet:renderURL var="rejectedRequestsUrl">
	<portlet:param name="mvcPath" value="/rejected-requests.jsp" />
	<portlet:param name="selectedMenu" value="rejectedRequests" />
</liferay-portlet:renderURL>

<div class="container-fuid-1280">
	<liferay-ui:icon-list>
		<liferay-ui:icon image="post" message="Post Request"
			url="${postRequestUrl}" />

		<liferay-ui:icon image="check" message="Requests Approval"
			url="${requestApprovalUrl}" />

		<liferay-ui:icon image="lock" message="Requests on Hold"
			url="${requestsOnHoldUrl}" />

		<liferay-ui:icon image="checked" message="Approved Requests"
			url="${approvedRequestsUrl}" />

		<liferay-ui:icon image="close" message="Rejected Requests"
			url="${rejectedRequestsUrl}" />
	</liferay-ui:icon-list>
</div>