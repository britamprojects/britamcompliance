<%@ include file="init.jsp"%>
<%@page import="com.compliance.service.model.Compliance"%>
<%@page import="java.util.List"%>
<portlet:defineObjects />

<%
	List<Compliance> complianceList = (List<Compliance>) request.getAttribute("complianceList");
	String spacer = " ";
%>
<portlet:renderURL var="addComplianceRenderURL">
	<portlet:param name="mvcPath" value="/add-record.jsp" />
</portlet:renderURL>


<liferay-util:include page="/navigation_bar.jsp"
	servletContext="<%=application%>">
	<liferay-util:param name="searchEnabled" value="false" />
</liferay-util:include>

<h2>Requests Approval</h2>

<table class="table table-striped">
	<tr>
		<th>Contract Number</th>
		<th>Name</th>
		<th>Request Date</th>
		<th>Category</th>
		<th colspan="2" style="width: 100px">Action</th>
	</tr>
	<c:forEach items="${complianceList}" var="compliance">

		<portlet:renderURL var="updateComplianceRenderURL">
			<portlet:param name="mvcPath" value="/update-record.jsp" />
			<portlet:param name="contractNumber"
				value="${compliance.contractNumber}" />
			<portlet:param name="firstName" value="${compliance.firstName}" />
			<portlet:param name="middleName" value="${compliance.middleName}" />
			<portlet:param name="lastName" value="${compliance.lastName}" />

			<portlet:param name="lastName" value="${compliance.approvalLevel}" />
			<portlet:param name="createDate" value="${compliance.createDate}" />
			<portlet:param name="approverContractNumber"
				value="${compliance.approverContractNumber}" />
			<portlet:param name="complianceId" value="${compliance.complianceId}" />
			<portlet:param name="approverCategory"
				value="${compliance.approverCategory}" />

			<portlet:param name="newOrgName1" value="${compliance.newOrgName1}" />
			<portlet:param name="newPositionName1"
				value="${compliance.newPositionName1}" />

			<portlet:param name="newOrgName2" value="${compliance.newOrgName2}" />
			<portlet:param name="newPositionName2"
				value="${compliance.newPositionName2}" />

			<portlet:param name="oldOrgName" value="${compliance.oldOrgName}" />
			<portlet:param name="oldPositionName"
				value="${compliance.oldPositionName}" />

			<portlet:param name="statusByUserName"
				value="${compliance.statusByUserName}" />

			<portlet:param name="comments" value="${compliance.comments}" />

			<portlet:param name="oldPositionId"
				value="${compliance.oldPositionId}" />

			<portlet:param name="parentOrgName"
				value="${compliance.parentOrgName}" />
			<portlet:param name="grandParentOrgName"
				value="${compliance.grandParentOrgName}" />

		</portlet:renderURL>

		<portlet:actionURL name="deleteCompliance"
			var="deleteComplianceActionURL">
			<portlet:param name="complianceId"
				value="${compliance.getComplianceId()}" />

		</portlet:actionURL>



		<tr>
			<td>${compliance.getContractNumber()}</td>
			<%
				if ("${compliance.getFirstName()}" == null) {
			%>
			<td>${compliance.getMiddleName()}${compliance.getLastName()}</td>
			<%
				} else if ("${compliance.getMiddleName()}" == null) {
			%>
			<td>${compliance.getFirstName()}${compliance.getLastName()}</td>
			<%
				} else if ("${compliance.getLastName()}" == null) {
			%>
			<td>${compliance.getFirstName()}${compliance.getMiddleName()}</td>
			<%
				} else {
			%>
			<td>${compliance.getFirstName()}${compliance.getMiddleName()}
				${compliance.getLastName()}</td>
			<%
				}
			%>

			<td><fmt:formatDate type="both" dateStyle="short"
					timeStyle="short" value="${compliance.getCreateDate()}" /></td>
			<td>${compliance.getApproverCategory()}</td>
			<td class="text-center" style="width: 50px"><a
				href="<%=updateComplianceRenderURL%>"
				class="btn  btn-primary btn-default btn-sm px-2 py-1"> <i
					class="glyphicon glyphicon-edit"></i> Respond
			</a></td>
		</tr>
	</c:forEach>
</table>