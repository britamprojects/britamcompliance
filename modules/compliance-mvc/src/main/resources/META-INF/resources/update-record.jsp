<%@ include file="init.jsp"%>
<%@page import="com.compliance.service.model.UserComment"%>
<%@page
	import="com.compliance.service.service.UserCommentLocalServiceUtil"%>

<%@page import="java.util.List"%>

<portlet:defineObjects />

<portlet:actionURL name="updateCompliance"
	var="updateComplianceActionURL" />

<aui:form action="<%=updateComplianceActionURL%>" name="complianceForm"
	method="POST" />

<%
	String complianceId = renderRequest.getParameter("complianceId");
	String contractNumber = renderRequest.getParameter("contractNumber");
	String entityId = renderRequest.getParameter("entityId");
	String comments = renderRequest.getParameter("comments");
	String approverContractNumber = renderRequest.getParameter("approverContractNumber");
	String approvalLevel = renderRequest.getParameter("approvalLevel");
	String approverComments = renderRequest.getParameter("approverComments");
	String contractId = renderRequest.getParameter("contractId");
	String positionId = renderRequest.getParameter("positionId");
	String orgId = renderRequest.getParameter("orgId");
	String firstName = renderRequest.getParameter("firstName");
	String middleName = renderRequest.getParameter("middleName");
	String lastName = renderRequest.getParameter("lastName");
	String newPositionName1 = renderRequest.getParameter("newPositionName1");
	String newOrgName1 = renderRequest.getParameter("newOrgName1");

	String newPositionName2 = renderRequest.getParameter("newPositionName2");
	String newOrgName2 = renderRequest.getParameter("newOrgName2");

	String oldOrgName = renderRequest.getParameter("oldOrgName");
	String oldPositionName = renderRequest.getParameter("oldPositionName");
	String statusByUserName = renderRequest.getParameter("statusByUserName");

	String oldPositionId = renderRequest.getParameter("oldPositionId");

	String parentOrgName = renderRequest.getParameter("parentOrgName");
	String gParentOrgName = renderRequest.getParameter("gParentOrgName");

	String spacer = " ";
%>

<%
	long newComplianceId = Long.parseLong(complianceId);
	List<UserComment> userCommentList = UserCommentLocalServiceUtil.findByComplianceId(newComplianceId);
%>


<liferay-util:include page="/navigation_bar.jsp"
	servletContext="<%=application%>">
	<liferay-util:param name="searchEnabled" value="false" />
</liferay-util:include>
<h2>Requests Approval</h2>
<aui:form action="<%=updateComplianceActionURL%>" method="post">

	<table>
	
	
	
		<tr>
		
			<th>Contract Number</th>
			<td><%=contractNumber%></td>
		</tr>
		<tr>
			<th>Full Name</th>
			<%
				if (firstName == null) {
			%>
			<td><%=middleName%><%=spacer%><%=lastName%></td>
			<%
				} else if (middleName == null) {
			%>
			<td><%=firstName%><%=spacer%><%=lastName%></td>
			<%
				} else if (lastName == null) {
			%>
			<td><%=firstName%><%=spacer%><%=middleName%></td>
			<%
				} else {
			%>
			<td><%=firstName%><%=spacer%><%=middleName%><%=spacer%><%=lastName%></td>
			<%
				}
			%>
		</tr>

		<tr>
			<th><%=oldPositionName%></th>
			<td><%=oldOrgName%></td>
		</tr>

		<tr>
			<%
				if (oldPositionId.equals("3")) {
			%>
			<th>Unit Manager</th>
			<td><%=parentOrgName%></td>
			
			<th>New Organization 1</th>
			<td><%=newOrgName1%></td>
			
			<th>New Organization 2</th>
			<td><%=newOrgName2%></td>
			
			

			<%
				} else if (oldPositionId.equals("4")) {
			%>
			<th>Branch Manager</th>
			<td><%=parentOrgName%></td>
				<th>New Organization 1</th>
			<td><%=newOrgName1%></td>
			
			<th>New Organization 2</th>
			<td><%=newOrgName2%></td>
			<%
				}
			%>
		</tr>

		<tr>
			<th>Requested by</th>
			<td><%=statusByUserName%></td>
			<%--  old_position=<%=newPositionName1%> --%>
		</tr>

	</table>

	</br>
	</br>
	</br>

	<aui:input id="comments" name="comments" type="textarea"
		value="<%=comments%>" readonly="true" label="Requester Comment" />

	<aui:select name="approverVerdict" required="true"
		label="Select Option:">
		<aui:option value="">Approval Action</aui:option>
		<aui:option value="Accepted">Accepted</aui:option>
		<aui:option value="Rejected">Rejected</aui:option>
		<aui:option value="MoreInfo">Request More Information</aui:option>
	</aui:select>

	<aui:input id="comments2" name="approverComment" type="textarea"
		label="Approver Comment" required="true" />

	<aui:button type="submit" name="" value="Submit Response"></aui:button>

	<aui:input name="complianceId" type="hidden" value="<%=complianceId%>" />

	<br />
	<br />
	<br />
	
<table class="table table-striped">
	<tr>



		<th>Comment</th>
		<th>Comment By</th>
		<th>Comment Date</th>

	</tr>
	<c:forEach items="${userCommentList}" var="compliance">
		<portlet:renderURL var="updateComplianceRenderURL">
			<portlet:param name="createDate" value="${compliance.createDate}" />
			<portlet:param name="approverComments" value="${compliance.comment}" />
		</portlet:renderURL>
		<tr>
			<td>${compliance.getComment()}</td>
			<td>${compliance.getApproverName()}</td>
			<td><fmt:formatDate type="both" dateStyle="short"
					timeStyle="short" value="${compliance.getCreateDate()}" /></td>





		</tr>
	</c:forEach>
</table>

</aui:form>





