
<%@ include file="init.jsp"%>
<%@page import="com.compliance.service.model.Compliance"%>
<%@page import="com.compliance.service.model.UserComment"%>
<%@page import="java.util.List"%>
<portlet:defineObjects />
<portlet:actionURL name="updateComplianceUserComment"
	var="updateComplianceActionURL" />
<aui:form action="<%=updateComplianceActionURL%>" name="complianceForm"
	method="POST" />
<%
	List<UserComment> userCommentList = (List<UserComment>) request.getAttribute("userCommentList");
%>
<%
	String complianceId = renderRequest.getParameter("complianceId");
	String contractNumber = renderRequest.getParameter("contractNumber");
	String entityId = renderRequest.getParameter("entityId");
	String comments = renderRequest.getParameter("comments");
	String approverContractNumber = renderRequest.getParameter("approverContractNumber");
	String approvalLevel = renderRequest.getParameter("approvalLevel");
	String approverComments = renderRequest.getParameter("approverComments");
	String comment_ = renderRequest.getParameter("comment_");

	String statusByUserName = renderRequest.getParameter("statusByUserName");
%>

<liferay-util:include page="/navigation_bar.jsp"
	servletContext="<%=application%>">
	<liferay-util:param name="searchEnabled" value="false" />
</liferay-util:include>

<h2>Requests on Hold</h2>

<liferay-frontend:horizontal-card text="Documents"
	url="/docs/7-1/tutorials/-/knowledge_base/t/clay-icons">
	<liferay-frontend:horizontal-card-col>
		<liferay-frontend:horizontal-card-icon icon="folder" />
	</liferay-frontend:horizontal-card-col>
</liferay-frontend:horizontal-card>

<aui:form action="<%=updateComplianceActionURL%>" method="post">
	<label>${compliance.comment_}</label>
	
	<aui:input id="comments2" name="approverComment"
		label="Approver Comment" type="textarea" required="true" />

	<aui:button type="submit" name="" value="Submit Response"></aui:button>
	<aui:input name="complianceId" type="hidden" value="<%=complianceId%>" />
</aui:form>
<br />

<table class="table table-striped">
	<tr>



		<th>Comment</th>
		<th>Comment By</th>
		<th>Comment Date</th>

	</tr>
	<c:forEach items="${userCommentList}" var="compliance">
		<portlet:renderURL var="updateComplianceRenderURL">
			<portlet:param name="createDate" value="${compliance.createDate}" />
			<portlet:param name="approverComments" value="${compliance.comment}" />
		</portlet:renderURL>
		<tr>
			<td>${compliance.getComment()}</td>
			<td>${compliance.getApproverName()}</td>
			<td><fmt:formatDate type="both" dateStyle="short"
					timeStyle="short" value="${compliance.getCreateDate()}" /></td>





		</tr>
	</c:forEach>
</table>
