create index IX_209FCD0B on bic_Compliance (approvalLevel[$COLUMN_LENGTH:75$], finalStatus);
create index IX_AA524DAA on bic_Compliance (approverVerdict[$COLUMN_LENGTH:75$], processedFlag[$COLUMN_LENGTH:75$]);
create index IX_9B8A1276 on bic_Compliance (entityId);
create index IX_DDE48576 on bic_Compliance (finalStatus, processedFlag[$COLUMN_LENGTH:75$]);
create index IX_28DF526C on bic_Compliance (groupId, status);
create index IX_3259AE8A on bic_Compliance (status);
create index IX_90D9B498 on bic_Compliance (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_6DF6811A on bic_Compliance (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_CF4A6381 on bic_UserComment (complianceId);
create index IX_EFE259B9 on bic_UserComment (groupId, status);
create index IX_B84BCC9D on bic_UserComment (status);
create index IX_AA3796EB on bic_UserComment (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_E7FEDC2D on bic_UserComment (uuid_[$COLUMN_LENGTH:75$], groupId);