/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.compliance.service.service.impl;

import com.compliance.service.model.UserComment;
import com.compliance.service.service.base.UserCommentLocalServiceBaseImpl;

import java.util.List;

/**
 * The implementation of the user comment local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>com.compliance.service.service.UserCommentLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserCommentLocalServiceBaseImpl
 */
public class UserCommentLocalServiceImpl
	extends UserCommentLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>com.compliance.service.service.UserCommentLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>com.compliance.service.service.UserCommentLocalServiceUtil</code>.
	 */
	
	/**
	 * Returns all the user comments where complianceId = &#63;.
	 *
	 * @param complianceId the compliance ID
	 * @return the matching user comments
	 */
	public List<UserComment> findByComplianceId(long complianceId) {
		return getUserCommentPersistence().findByComplianceId(complianceId);
	}
}