/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.compliance.service.service.persistence.impl;

import com.compliance.service.exception.NoSuchComplianceException;
import com.compliance.service.model.Compliance;
import com.compliance.service.model.impl.ComplianceImpl;
import com.compliance.service.model.impl.ComplianceModelImpl;
import com.compliance.service.service.persistence.CompliancePersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the compliance service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class CompliancePersistenceImpl
	extends BasePersistenceImpl<Compliance> implements CompliancePersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>ComplianceUtil</code> to access the compliance persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		ComplianceImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the compliances where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching compliances
	 */
	@Override
	public List<Compliance> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the compliances where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @return the range of matching compliances
	 */
	@Override
	public List<Compliance> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the compliances where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching compliances
	 */
	@Override
	public List<Compliance> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Compliance> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the compliances where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching compliances
	 */
	@Override
	public List<Compliance> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Compliance> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<Compliance> list = null;

		if (useFinderCache) {
			list = (List<Compliance>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Compliance compliance : list) {
					if (!uuid.equals(compliance.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_COMPLIANCE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(ComplianceModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<Compliance>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				if (useFinderCache) {
					finderCache.removeResult(finderPath, finderArgs);
				}

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first compliance in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	@Override
	public Compliance findByUuid_First(
			String uuid, OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		Compliance compliance = fetchByUuid_First(uuid, orderByComparator);

		if (compliance != null) {
			return compliance;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchComplianceException(sb.toString());
	}

	/**
	 * Returns the first compliance in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	@Override
	public Compliance fetchByUuid_First(
		String uuid, OrderByComparator<Compliance> orderByComparator) {

		List<Compliance> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last compliance in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	@Override
	public Compliance findByUuid_Last(
			String uuid, OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		Compliance compliance = fetchByUuid_Last(uuid, orderByComparator);

		if (compliance != null) {
			return compliance;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchComplianceException(sb.toString());
	}

	/**
	 * Returns the last compliance in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	@Override
	public Compliance fetchByUuid_Last(
		String uuid, OrderByComparator<Compliance> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<Compliance> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the compliances before and after the current compliance in the ordered set where uuid = &#63;.
	 *
	 * @param complianceId the primary key of the current compliance
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next compliance
	 * @throws NoSuchComplianceException if a compliance with the primary key could not be found
	 */
	@Override
	public Compliance[] findByUuid_PrevAndNext(
			long complianceId, String uuid,
			OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		uuid = Objects.toString(uuid, "");

		Compliance compliance = findByPrimaryKey(complianceId);

		Session session = null;

		try {
			session = openSession();

			Compliance[] array = new ComplianceImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, compliance, uuid, orderByComparator, true);

			array[1] = compliance;

			array[2] = getByUuid_PrevAndNext(
				session, compliance, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Compliance getByUuid_PrevAndNext(
		Session session, Compliance compliance, String uuid,
		OrderByComparator<Compliance> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_COMPLIANCE_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(ComplianceModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(compliance)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Compliance> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the compliances where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (Compliance compliance :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(compliance);
		}
	}

	/**
	 * Returns the number of compliances where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching compliances
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_COMPLIANCE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"compliance.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(compliance.uuid IS NULL OR compliance.uuid = '')";

	private FinderPath _finderPathFetchByUUID_G;
	private FinderPath _finderPathCountByUUID_G;

	/**
	 * Returns the compliance where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchComplianceException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	@Override
	public Compliance findByUUID_G(String uuid, long groupId)
		throws NoSuchComplianceException {

		Compliance compliance = fetchByUUID_G(uuid, groupId);

		if (compliance == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("uuid=");
			sb.append(uuid);

			sb.append(", groupId=");
			sb.append(groupId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchComplianceException(sb.toString());
		}

		return compliance;
	}

	/**
	 * Returns the compliance where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	@Override
	public Compliance fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the compliance where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	@Override
	public Compliance fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {uuid, groupId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByUUID_G, finderArgs, this);
		}

		if (result instanceof Compliance) {
			Compliance compliance = (Compliance)result;

			if (!Objects.equals(uuid, compliance.getUuid()) ||
				(groupId != compliance.getGroupId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_COMPLIANCE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				List<Compliance> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByUUID_G, finderArgs, list);
					}
				}
				else {
					Compliance compliance = list.get(0);

					result = compliance;

					cacheResult(compliance);
				}
			}
			catch (Exception exception) {
				if (useFinderCache) {
					finderCache.removeResult(
						_finderPathFetchByUUID_G, finderArgs);
				}

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Compliance)result;
		}
	}

	/**
	 * Removes the compliance where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the compliance that was removed
	 */
	@Override
	public Compliance removeByUUID_G(String uuid, long groupId)
		throws NoSuchComplianceException {

		Compliance compliance = findByUUID_G(uuid, groupId);

		return remove(compliance);
	}

	/**
	 * Returns the number of compliances where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching compliances
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUUID_G;

		Object[] finderArgs = new Object[] {uuid, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_COMPLIANCE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_2 =
		"compliance.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_G_UUID_3 =
		"(compliance.uuid IS NULL OR compliance.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 =
		"compliance.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the compliances where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching compliances
	 */
	@Override
	public List<Compliance> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the compliances where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @return the range of matching compliances
	 */
	@Override
	public List<Compliance> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the compliances where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching compliances
	 */
	@Override
	public List<Compliance> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Compliance> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the compliances where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching compliances
	 */
	@Override
	public List<Compliance> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Compliance> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid_C;
				finderArgs = new Object[] {uuid, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<Compliance> list = null;

		if (useFinderCache) {
			list = (List<Compliance>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Compliance compliance : list) {
					if (!uuid.equals(compliance.getUuid()) ||
						(companyId != compliance.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_COMPLIANCE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(ComplianceModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				list = (List<Compliance>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				if (useFinderCache) {
					finderCache.removeResult(finderPath, finderArgs);
				}

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first compliance in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	@Override
	public Compliance findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		Compliance compliance = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (compliance != null) {
			return compliance;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchComplianceException(sb.toString());
	}

	/**
	 * Returns the first compliance in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	@Override
	public Compliance fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Compliance> orderByComparator) {

		List<Compliance> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last compliance in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	@Override
	public Compliance findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		Compliance compliance = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (compliance != null) {
			return compliance;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchComplianceException(sb.toString());
	}

	/**
	 * Returns the last compliance in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	@Override
	public Compliance fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Compliance> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<Compliance> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the compliances before and after the current compliance in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param complianceId the primary key of the current compliance
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next compliance
	 * @throws NoSuchComplianceException if a compliance with the primary key could not be found
	 */
	@Override
	public Compliance[] findByUuid_C_PrevAndNext(
			long complianceId, String uuid, long companyId,
			OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		uuid = Objects.toString(uuid, "");

		Compliance compliance = findByPrimaryKey(complianceId);

		Session session = null;

		try {
			session = openSession();

			Compliance[] array = new ComplianceImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, compliance, uuid, companyId, orderByComparator, true);

			array[1] = compliance;

			array[2] = getByUuid_C_PrevAndNext(
				session, compliance, uuid, companyId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Compliance getByUuid_C_PrevAndNext(
		Session session, Compliance compliance, String uuid, long companyId,
		OrderByComparator<Compliance> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_COMPLIANCE_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(ComplianceModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(compliance)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Compliance> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the compliances where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (Compliance compliance :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(compliance);
		}
	}

	/**
	 * Returns the number of compliances where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching compliances
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_COMPLIANCE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"compliance.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(compliance.uuid IS NULL OR compliance.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"compliance.companyId = ?";

	private FinderPath _finderPathWithPaginationFindByEntityId;
	private FinderPath _finderPathWithoutPaginationFindByEntityId;
	private FinderPath _finderPathCountByEntityId;

	/**
	 * Returns all the compliances where entityId = &#63;.
	 *
	 * @param entityId the entity ID
	 * @return the matching compliances
	 */
	@Override
	public List<Compliance> findByEntityId(long entityId) {
		return findByEntityId(
			entityId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the compliances where entityId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param entityId the entity ID
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @return the range of matching compliances
	 */
	@Override
	public List<Compliance> findByEntityId(long entityId, int start, int end) {
		return findByEntityId(entityId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the compliances where entityId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param entityId the entity ID
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching compliances
	 */
	@Override
	public List<Compliance> findByEntityId(
		long entityId, int start, int end,
		OrderByComparator<Compliance> orderByComparator) {

		return findByEntityId(entityId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the compliances where entityId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param entityId the entity ID
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching compliances
	 */
	@Override
	public List<Compliance> findByEntityId(
		long entityId, int start, int end,
		OrderByComparator<Compliance> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByEntityId;
				finderArgs = new Object[] {entityId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByEntityId;
			finderArgs = new Object[] {entityId, start, end, orderByComparator};
		}

		List<Compliance> list = null;

		if (useFinderCache) {
			list = (List<Compliance>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Compliance compliance : list) {
					if (entityId != compliance.getEntityId()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_COMPLIANCE_WHERE);

			sb.append(_FINDER_COLUMN_ENTITYID_ENTITYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(ComplianceModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(entityId);

				list = (List<Compliance>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				if (useFinderCache) {
					finderCache.removeResult(finderPath, finderArgs);
				}

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first compliance in the ordered set where entityId = &#63;.
	 *
	 * @param entityId the entity ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	@Override
	public Compliance findByEntityId_First(
			long entityId, OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		Compliance compliance = fetchByEntityId_First(
			entityId, orderByComparator);

		if (compliance != null) {
			return compliance;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("entityId=");
		sb.append(entityId);

		sb.append("}");

		throw new NoSuchComplianceException(sb.toString());
	}

	/**
	 * Returns the first compliance in the ordered set where entityId = &#63;.
	 *
	 * @param entityId the entity ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	@Override
	public Compliance fetchByEntityId_First(
		long entityId, OrderByComparator<Compliance> orderByComparator) {

		List<Compliance> list = findByEntityId(
			entityId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last compliance in the ordered set where entityId = &#63;.
	 *
	 * @param entityId the entity ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	@Override
	public Compliance findByEntityId_Last(
			long entityId, OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		Compliance compliance = fetchByEntityId_Last(
			entityId, orderByComparator);

		if (compliance != null) {
			return compliance;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("entityId=");
		sb.append(entityId);

		sb.append("}");

		throw new NoSuchComplianceException(sb.toString());
	}

	/**
	 * Returns the last compliance in the ordered set where entityId = &#63;.
	 *
	 * @param entityId the entity ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	@Override
	public Compliance fetchByEntityId_Last(
		long entityId, OrderByComparator<Compliance> orderByComparator) {

		int count = countByEntityId(entityId);

		if (count == 0) {
			return null;
		}

		List<Compliance> list = findByEntityId(
			entityId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the compliances before and after the current compliance in the ordered set where entityId = &#63;.
	 *
	 * @param complianceId the primary key of the current compliance
	 * @param entityId the entity ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next compliance
	 * @throws NoSuchComplianceException if a compliance with the primary key could not be found
	 */
	@Override
	public Compliance[] findByEntityId_PrevAndNext(
			long complianceId, long entityId,
			OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		Compliance compliance = findByPrimaryKey(complianceId);

		Session session = null;

		try {
			session = openSession();

			Compliance[] array = new ComplianceImpl[3];

			array[0] = getByEntityId_PrevAndNext(
				session, compliance, entityId, orderByComparator, true);

			array[1] = compliance;

			array[2] = getByEntityId_PrevAndNext(
				session, compliance, entityId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Compliance getByEntityId_PrevAndNext(
		Session session, Compliance compliance, long entityId,
		OrderByComparator<Compliance> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_COMPLIANCE_WHERE);

		sb.append(_FINDER_COLUMN_ENTITYID_ENTITYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(ComplianceModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(entityId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(compliance)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Compliance> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the compliances where entityId = &#63; from the database.
	 *
	 * @param entityId the entity ID
	 */
	@Override
	public void removeByEntityId(long entityId) {
		for (Compliance compliance :
				findByEntityId(
					entityId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(compliance);
		}
	}

	/**
	 * Returns the number of compliances where entityId = &#63;.
	 *
	 * @param entityId the entity ID
	 * @return the number of matching compliances
	 */
	@Override
	public int countByEntityId(long entityId) {
		FinderPath finderPath = _finderPathCountByEntityId;

		Object[] finderArgs = new Object[] {entityId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_COMPLIANCE_WHERE);

			sb.append(_FINDER_COLUMN_ENTITYID_ENTITYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(entityId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ENTITYID_ENTITYID_2 =
		"compliance.entityId = ?";

	private FinderPath _finderPathWithPaginationFindByApprovalLevel;
	private FinderPath _finderPathWithoutPaginationFindByApprovalLevel;
	private FinderPath _finderPathCountByApprovalLevel;

	/**
	 * Returns all the compliances where approvalLevel = &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @return the matching compliances
	 */
	@Override
	public List<Compliance> findByApprovalLevel(String approvalLevel) {
		return findByApprovalLevel(
			approvalLevel, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the compliances where approvalLevel = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param approvalLevel the approval level
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @return the range of matching compliances
	 */
	@Override
	public List<Compliance> findByApprovalLevel(
		String approvalLevel, int start, int end) {

		return findByApprovalLevel(approvalLevel, start, end, null);
	}

	/**
	 * Returns an ordered range of all the compliances where approvalLevel = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param approvalLevel the approval level
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching compliances
	 */
	@Override
	public List<Compliance> findByApprovalLevel(
		String approvalLevel, int start, int end,
		OrderByComparator<Compliance> orderByComparator) {

		return findByApprovalLevel(
			approvalLevel, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the compliances where approvalLevel = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param approvalLevel the approval level
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching compliances
	 */
	@Override
	public List<Compliance> findByApprovalLevel(
		String approvalLevel, int start, int end,
		OrderByComparator<Compliance> orderByComparator,
		boolean useFinderCache) {

		approvalLevel = Objects.toString(approvalLevel, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByApprovalLevel;
				finderArgs = new Object[] {approvalLevel};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByApprovalLevel;
			finderArgs = new Object[] {
				approvalLevel, start, end, orderByComparator
			};
		}

		List<Compliance> list = null;

		if (useFinderCache) {
			list = (List<Compliance>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Compliance compliance : list) {
					if (!approvalLevel.equals(compliance.getApprovalLevel())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_COMPLIANCE_WHERE);

			boolean bindApprovalLevel = false;

			if (approvalLevel.isEmpty()) {
				sb.append(_FINDER_COLUMN_APPROVALLEVEL_APPROVALLEVEL_3);
			}
			else {
				bindApprovalLevel = true;

				sb.append(_FINDER_COLUMN_APPROVALLEVEL_APPROVALLEVEL_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(ComplianceModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindApprovalLevel) {
					queryPos.add(approvalLevel);
				}

				list = (List<Compliance>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				if (useFinderCache) {
					finderCache.removeResult(finderPath, finderArgs);
				}

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first compliance in the ordered set where approvalLevel = &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	@Override
	public Compliance findByApprovalLevel_First(
			String approvalLevel,
			OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		Compliance compliance = fetchByApprovalLevel_First(
			approvalLevel, orderByComparator);

		if (compliance != null) {
			return compliance;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("approvalLevel=");
		sb.append(approvalLevel);

		sb.append("}");

		throw new NoSuchComplianceException(sb.toString());
	}

	/**
	 * Returns the first compliance in the ordered set where approvalLevel = &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	@Override
	public Compliance fetchByApprovalLevel_First(
		String approvalLevel, OrderByComparator<Compliance> orderByComparator) {

		List<Compliance> list = findByApprovalLevel(
			approvalLevel, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last compliance in the ordered set where approvalLevel = &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	@Override
	public Compliance findByApprovalLevel_Last(
			String approvalLevel,
			OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		Compliance compliance = fetchByApprovalLevel_Last(
			approvalLevel, orderByComparator);

		if (compliance != null) {
			return compliance;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("approvalLevel=");
		sb.append(approvalLevel);

		sb.append("}");

		throw new NoSuchComplianceException(sb.toString());
	}

	/**
	 * Returns the last compliance in the ordered set where approvalLevel = &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	@Override
	public Compliance fetchByApprovalLevel_Last(
		String approvalLevel, OrderByComparator<Compliance> orderByComparator) {

		int count = countByApprovalLevel(approvalLevel);

		if (count == 0) {
			return null;
		}

		List<Compliance> list = findByApprovalLevel(
			approvalLevel, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the compliances before and after the current compliance in the ordered set where approvalLevel = &#63;.
	 *
	 * @param complianceId the primary key of the current compliance
	 * @param approvalLevel the approval level
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next compliance
	 * @throws NoSuchComplianceException if a compliance with the primary key could not be found
	 */
	@Override
	public Compliance[] findByApprovalLevel_PrevAndNext(
			long complianceId, String approvalLevel,
			OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		approvalLevel = Objects.toString(approvalLevel, "");

		Compliance compliance = findByPrimaryKey(complianceId);

		Session session = null;

		try {
			session = openSession();

			Compliance[] array = new ComplianceImpl[3];

			array[0] = getByApprovalLevel_PrevAndNext(
				session, compliance, approvalLevel, orderByComparator, true);

			array[1] = compliance;

			array[2] = getByApprovalLevel_PrevAndNext(
				session, compliance, approvalLevel, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Compliance getByApprovalLevel_PrevAndNext(
		Session session, Compliance compliance, String approvalLevel,
		OrderByComparator<Compliance> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_COMPLIANCE_WHERE);

		boolean bindApprovalLevel = false;

		if (approvalLevel.isEmpty()) {
			sb.append(_FINDER_COLUMN_APPROVALLEVEL_APPROVALLEVEL_3);
		}
		else {
			bindApprovalLevel = true;

			sb.append(_FINDER_COLUMN_APPROVALLEVEL_APPROVALLEVEL_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(ComplianceModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindApprovalLevel) {
			queryPos.add(approvalLevel);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(compliance)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Compliance> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the compliances where approvalLevel = &#63; from the database.
	 *
	 * @param approvalLevel the approval level
	 */
	@Override
	public void removeByApprovalLevel(String approvalLevel) {
		for (Compliance compliance :
				findByApprovalLevel(
					approvalLevel, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(compliance);
		}
	}

	/**
	 * Returns the number of compliances where approvalLevel = &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @return the number of matching compliances
	 */
	@Override
	public int countByApprovalLevel(String approvalLevel) {
		approvalLevel = Objects.toString(approvalLevel, "");

		FinderPath finderPath = _finderPathCountByApprovalLevel;

		Object[] finderArgs = new Object[] {approvalLevel};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_COMPLIANCE_WHERE);

			boolean bindApprovalLevel = false;

			if (approvalLevel.isEmpty()) {
				sb.append(_FINDER_COLUMN_APPROVALLEVEL_APPROVALLEVEL_3);
			}
			else {
				bindApprovalLevel = true;

				sb.append(_FINDER_COLUMN_APPROVALLEVEL_APPROVALLEVEL_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindApprovalLevel) {
					queryPos.add(approvalLevel);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_APPROVALLEVEL_APPROVALLEVEL_2 =
		"compliance.approvalLevel = ?";

	private static final String _FINDER_COLUMN_APPROVALLEVEL_APPROVALLEVEL_3 =
		"(compliance.approvalLevel IS NULL OR compliance.approvalLevel = '')";

	private FinderPath _finderPathWithPaginationFindByUnprocessedFinalStatus;
	private FinderPath _finderPathWithoutPaginationFindByUnprocessedFinalStatus;
	private FinderPath _finderPathCountByUnprocessedFinalStatus;

	/**
	 * Returns all the compliances where finalStatus = &#63; and processedFlag = &#63;.
	 *
	 * @param finalStatus the final status
	 * @param processedFlag the processed flag
	 * @return the matching compliances
	 */
	@Override
	public List<Compliance> findByUnprocessedFinalStatus(
		boolean finalStatus, String processedFlag) {

		return findByUnprocessedFinalStatus(
			finalStatus, processedFlag, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the compliances where finalStatus = &#63; and processedFlag = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param finalStatus the final status
	 * @param processedFlag the processed flag
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @return the range of matching compliances
	 */
	@Override
	public List<Compliance> findByUnprocessedFinalStatus(
		boolean finalStatus, String processedFlag, int start, int end) {

		return findByUnprocessedFinalStatus(
			finalStatus, processedFlag, start, end, null);
	}

	/**
	 * Returns an ordered range of all the compliances where finalStatus = &#63; and processedFlag = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param finalStatus the final status
	 * @param processedFlag the processed flag
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching compliances
	 */
	@Override
	public List<Compliance> findByUnprocessedFinalStatus(
		boolean finalStatus, String processedFlag, int start, int end,
		OrderByComparator<Compliance> orderByComparator) {

		return findByUnprocessedFinalStatus(
			finalStatus, processedFlag, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the compliances where finalStatus = &#63; and processedFlag = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param finalStatus the final status
	 * @param processedFlag the processed flag
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching compliances
	 */
	@Override
	public List<Compliance> findByUnprocessedFinalStatus(
		boolean finalStatus, String processedFlag, int start, int end,
		OrderByComparator<Compliance> orderByComparator,
		boolean useFinderCache) {

		processedFlag = Objects.toString(processedFlag, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath =
					_finderPathWithoutPaginationFindByUnprocessedFinalStatus;
				finderArgs = new Object[] {finalStatus, processedFlag};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUnprocessedFinalStatus;
			finderArgs = new Object[] {
				finalStatus, processedFlag, start, end, orderByComparator
			};
		}

		List<Compliance> list = null;

		if (useFinderCache) {
			list = (List<Compliance>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Compliance compliance : list) {
					if ((finalStatus != compliance.isFinalStatus()) ||
						!processedFlag.equals(compliance.getProcessedFlag())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_COMPLIANCE_WHERE);

			sb.append(_FINDER_COLUMN_UNPROCESSEDFINALSTATUS_FINALSTATUS_2);

			boolean bindProcessedFlag = false;

			if (processedFlag.isEmpty()) {
				sb.append(
					_FINDER_COLUMN_UNPROCESSEDFINALSTATUS_PROCESSEDFLAG_3);
			}
			else {
				bindProcessedFlag = true;

				sb.append(
					_FINDER_COLUMN_UNPROCESSEDFINALSTATUS_PROCESSEDFLAG_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(ComplianceModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(finalStatus);

				if (bindProcessedFlag) {
					queryPos.add(processedFlag);
				}

				list = (List<Compliance>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				if (useFinderCache) {
					finderCache.removeResult(finderPath, finderArgs);
				}

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first compliance in the ordered set where finalStatus = &#63; and processedFlag = &#63;.
	 *
	 * @param finalStatus the final status
	 * @param processedFlag the processed flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	@Override
	public Compliance findByUnprocessedFinalStatus_First(
			boolean finalStatus, String processedFlag,
			OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		Compliance compliance = fetchByUnprocessedFinalStatus_First(
			finalStatus, processedFlag, orderByComparator);

		if (compliance != null) {
			return compliance;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("finalStatus=");
		sb.append(finalStatus);

		sb.append(", processedFlag=");
		sb.append(processedFlag);

		sb.append("}");

		throw new NoSuchComplianceException(sb.toString());
	}

	/**
	 * Returns the first compliance in the ordered set where finalStatus = &#63; and processedFlag = &#63;.
	 *
	 * @param finalStatus the final status
	 * @param processedFlag the processed flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	@Override
	public Compliance fetchByUnprocessedFinalStatus_First(
		boolean finalStatus, String processedFlag,
		OrderByComparator<Compliance> orderByComparator) {

		List<Compliance> list = findByUnprocessedFinalStatus(
			finalStatus, processedFlag, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last compliance in the ordered set where finalStatus = &#63; and processedFlag = &#63;.
	 *
	 * @param finalStatus the final status
	 * @param processedFlag the processed flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	@Override
	public Compliance findByUnprocessedFinalStatus_Last(
			boolean finalStatus, String processedFlag,
			OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		Compliance compliance = fetchByUnprocessedFinalStatus_Last(
			finalStatus, processedFlag, orderByComparator);

		if (compliance != null) {
			return compliance;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("finalStatus=");
		sb.append(finalStatus);

		sb.append(", processedFlag=");
		sb.append(processedFlag);

		sb.append("}");

		throw new NoSuchComplianceException(sb.toString());
	}

	/**
	 * Returns the last compliance in the ordered set where finalStatus = &#63; and processedFlag = &#63;.
	 *
	 * @param finalStatus the final status
	 * @param processedFlag the processed flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	@Override
	public Compliance fetchByUnprocessedFinalStatus_Last(
		boolean finalStatus, String processedFlag,
		OrderByComparator<Compliance> orderByComparator) {

		int count = countByUnprocessedFinalStatus(finalStatus, processedFlag);

		if (count == 0) {
			return null;
		}

		List<Compliance> list = findByUnprocessedFinalStatus(
			finalStatus, processedFlag, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the compliances before and after the current compliance in the ordered set where finalStatus = &#63; and processedFlag = &#63;.
	 *
	 * @param complianceId the primary key of the current compliance
	 * @param finalStatus the final status
	 * @param processedFlag the processed flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next compliance
	 * @throws NoSuchComplianceException if a compliance with the primary key could not be found
	 */
	@Override
	public Compliance[] findByUnprocessedFinalStatus_PrevAndNext(
			long complianceId, boolean finalStatus, String processedFlag,
			OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		processedFlag = Objects.toString(processedFlag, "");

		Compliance compliance = findByPrimaryKey(complianceId);

		Session session = null;

		try {
			session = openSession();

			Compliance[] array = new ComplianceImpl[3];

			array[0] = getByUnprocessedFinalStatus_PrevAndNext(
				session, compliance, finalStatus, processedFlag,
				orderByComparator, true);

			array[1] = compliance;

			array[2] = getByUnprocessedFinalStatus_PrevAndNext(
				session, compliance, finalStatus, processedFlag,
				orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Compliance getByUnprocessedFinalStatus_PrevAndNext(
		Session session, Compliance compliance, boolean finalStatus,
		String processedFlag, OrderByComparator<Compliance> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_COMPLIANCE_WHERE);

		sb.append(_FINDER_COLUMN_UNPROCESSEDFINALSTATUS_FINALSTATUS_2);

		boolean bindProcessedFlag = false;

		if (processedFlag.isEmpty()) {
			sb.append(_FINDER_COLUMN_UNPROCESSEDFINALSTATUS_PROCESSEDFLAG_3);
		}
		else {
			bindProcessedFlag = true;

			sb.append(_FINDER_COLUMN_UNPROCESSEDFINALSTATUS_PROCESSEDFLAG_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(ComplianceModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(finalStatus);

		if (bindProcessedFlag) {
			queryPos.add(processedFlag);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(compliance)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Compliance> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the compliances where finalStatus = &#63; and processedFlag = &#63; from the database.
	 *
	 * @param finalStatus the final status
	 * @param processedFlag the processed flag
	 */
	@Override
	public void removeByUnprocessedFinalStatus(
		boolean finalStatus, String processedFlag) {

		for (Compliance compliance :
				findByUnprocessedFinalStatus(
					finalStatus, processedFlag, QueryUtil.ALL_POS,
					QueryUtil.ALL_POS, null)) {

			remove(compliance);
		}
	}

	/**
	 * Returns the number of compliances where finalStatus = &#63; and processedFlag = &#63;.
	 *
	 * @param finalStatus the final status
	 * @param processedFlag the processed flag
	 * @return the number of matching compliances
	 */
	@Override
	public int countByUnprocessedFinalStatus(
		boolean finalStatus, String processedFlag) {

		processedFlag = Objects.toString(processedFlag, "");

		FinderPath finderPath = _finderPathCountByUnprocessedFinalStatus;

		Object[] finderArgs = new Object[] {finalStatus, processedFlag};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_COMPLIANCE_WHERE);

			sb.append(_FINDER_COLUMN_UNPROCESSEDFINALSTATUS_FINALSTATUS_2);

			boolean bindProcessedFlag = false;

			if (processedFlag.isEmpty()) {
				sb.append(
					_FINDER_COLUMN_UNPROCESSEDFINALSTATUS_PROCESSEDFLAG_3);
			}
			else {
				bindProcessedFlag = true;

				sb.append(
					_FINDER_COLUMN_UNPROCESSEDFINALSTATUS_PROCESSEDFLAG_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(finalStatus);

				if (bindProcessedFlag) {
					queryPos.add(processedFlag);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String
		_FINDER_COLUMN_UNPROCESSEDFINALSTATUS_FINALSTATUS_2 =
			"compliance.finalStatus = ? AND ";

	private static final String
		_FINDER_COLUMN_UNPROCESSEDFINALSTATUS_PROCESSEDFLAG_2 =
			"compliance.processedFlag = ?";

	private static final String
		_FINDER_COLUMN_UNPROCESSEDFINALSTATUS_PROCESSEDFLAG_3 =
			"(compliance.processedFlag IS NULL OR compliance.processedFlag = '')";

	private FinderPath _finderPathWithPaginationFindByFinalApproved;
	private FinderPath _finderPathWithoutPaginationFindByFinalApproved;
	private FinderPath _finderPathCountByFinalApproved;

	/**
	 * Returns all the compliances where approvalLevel = &#63; and finalStatus = &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @param finalStatus the final status
	 * @return the matching compliances
	 */
	@Override
	public List<Compliance> findByFinalApproved(
		String approvalLevel, boolean finalStatus) {

		return findByFinalApproved(
			approvalLevel, finalStatus, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the compliances where approvalLevel = &#63; and finalStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param approvalLevel the approval level
	 * @param finalStatus the final status
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @return the range of matching compliances
	 */
	@Override
	public List<Compliance> findByFinalApproved(
		String approvalLevel, boolean finalStatus, int start, int end) {

		return findByFinalApproved(
			approvalLevel, finalStatus, start, end, null);
	}

	/**
	 * Returns an ordered range of all the compliances where approvalLevel = &#63; and finalStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param approvalLevel the approval level
	 * @param finalStatus the final status
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching compliances
	 */
	@Override
	public List<Compliance> findByFinalApproved(
		String approvalLevel, boolean finalStatus, int start, int end,
		OrderByComparator<Compliance> orderByComparator) {

		return findByFinalApproved(
			approvalLevel, finalStatus, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the compliances where approvalLevel = &#63; and finalStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param approvalLevel the approval level
	 * @param finalStatus the final status
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching compliances
	 */
	@Override
	public List<Compliance> findByFinalApproved(
		String approvalLevel, boolean finalStatus, int start, int end,
		OrderByComparator<Compliance> orderByComparator,
		boolean useFinderCache) {

		approvalLevel = Objects.toString(approvalLevel, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByFinalApproved;
				finderArgs = new Object[] {approvalLevel, finalStatus};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByFinalApproved;
			finderArgs = new Object[] {
				approvalLevel, finalStatus, start, end, orderByComparator
			};
		}

		List<Compliance> list = null;

		if (useFinderCache) {
			list = (List<Compliance>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Compliance compliance : list) {
					if (!approvalLevel.equals(compliance.getApprovalLevel()) ||
						(finalStatus != compliance.isFinalStatus())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_COMPLIANCE_WHERE);

			boolean bindApprovalLevel = false;

			if (approvalLevel.isEmpty()) {
				sb.append(_FINDER_COLUMN_FINALAPPROVED_APPROVALLEVEL_3);
			}
			else {
				bindApprovalLevel = true;

				sb.append(_FINDER_COLUMN_FINALAPPROVED_APPROVALLEVEL_2);
			}

			sb.append(_FINDER_COLUMN_FINALAPPROVED_FINALSTATUS_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(ComplianceModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindApprovalLevel) {
					queryPos.add(approvalLevel);
				}

				queryPos.add(finalStatus);

				list = (List<Compliance>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				if (useFinderCache) {
					finderCache.removeResult(finderPath, finderArgs);
				}

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first compliance in the ordered set where approvalLevel = &#63; and finalStatus = &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @param finalStatus the final status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	@Override
	public Compliance findByFinalApproved_First(
			String approvalLevel, boolean finalStatus,
			OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		Compliance compliance = fetchByFinalApproved_First(
			approvalLevel, finalStatus, orderByComparator);

		if (compliance != null) {
			return compliance;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("approvalLevel=");
		sb.append(approvalLevel);

		sb.append(", finalStatus=");
		sb.append(finalStatus);

		sb.append("}");

		throw new NoSuchComplianceException(sb.toString());
	}

	/**
	 * Returns the first compliance in the ordered set where approvalLevel = &#63; and finalStatus = &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @param finalStatus the final status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	@Override
	public Compliance fetchByFinalApproved_First(
		String approvalLevel, boolean finalStatus,
		OrderByComparator<Compliance> orderByComparator) {

		List<Compliance> list = findByFinalApproved(
			approvalLevel, finalStatus, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last compliance in the ordered set where approvalLevel = &#63; and finalStatus = &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @param finalStatus the final status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	@Override
	public Compliance findByFinalApproved_Last(
			String approvalLevel, boolean finalStatus,
			OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		Compliance compliance = fetchByFinalApproved_Last(
			approvalLevel, finalStatus, orderByComparator);

		if (compliance != null) {
			return compliance;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("approvalLevel=");
		sb.append(approvalLevel);

		sb.append(", finalStatus=");
		sb.append(finalStatus);

		sb.append("}");

		throw new NoSuchComplianceException(sb.toString());
	}

	/**
	 * Returns the last compliance in the ordered set where approvalLevel = &#63; and finalStatus = &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @param finalStatus the final status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	@Override
	public Compliance fetchByFinalApproved_Last(
		String approvalLevel, boolean finalStatus,
		OrderByComparator<Compliance> orderByComparator) {

		int count = countByFinalApproved(approvalLevel, finalStatus);

		if (count == 0) {
			return null;
		}

		List<Compliance> list = findByFinalApproved(
			approvalLevel, finalStatus, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the compliances before and after the current compliance in the ordered set where approvalLevel = &#63; and finalStatus = &#63;.
	 *
	 * @param complianceId the primary key of the current compliance
	 * @param approvalLevel the approval level
	 * @param finalStatus the final status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next compliance
	 * @throws NoSuchComplianceException if a compliance with the primary key could not be found
	 */
	@Override
	public Compliance[] findByFinalApproved_PrevAndNext(
			long complianceId, String approvalLevel, boolean finalStatus,
			OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		approvalLevel = Objects.toString(approvalLevel, "");

		Compliance compliance = findByPrimaryKey(complianceId);

		Session session = null;

		try {
			session = openSession();

			Compliance[] array = new ComplianceImpl[3];

			array[0] = getByFinalApproved_PrevAndNext(
				session, compliance, approvalLevel, finalStatus,
				orderByComparator, true);

			array[1] = compliance;

			array[2] = getByFinalApproved_PrevAndNext(
				session, compliance, approvalLevel, finalStatus,
				orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Compliance getByFinalApproved_PrevAndNext(
		Session session, Compliance compliance, String approvalLevel,
		boolean finalStatus, OrderByComparator<Compliance> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_COMPLIANCE_WHERE);

		boolean bindApprovalLevel = false;

		if (approvalLevel.isEmpty()) {
			sb.append(_FINDER_COLUMN_FINALAPPROVED_APPROVALLEVEL_3);
		}
		else {
			bindApprovalLevel = true;

			sb.append(_FINDER_COLUMN_FINALAPPROVED_APPROVALLEVEL_2);
		}

		sb.append(_FINDER_COLUMN_FINALAPPROVED_FINALSTATUS_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(ComplianceModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindApprovalLevel) {
			queryPos.add(approvalLevel);
		}

		queryPos.add(finalStatus);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(compliance)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Compliance> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the compliances where approvalLevel = &#63; and finalStatus = &#63; from the database.
	 *
	 * @param approvalLevel the approval level
	 * @param finalStatus the final status
	 */
	@Override
	public void removeByFinalApproved(
		String approvalLevel, boolean finalStatus) {

		for (Compliance compliance :
				findByFinalApproved(
					approvalLevel, finalStatus, QueryUtil.ALL_POS,
					QueryUtil.ALL_POS, null)) {

			remove(compliance);
		}
	}

	/**
	 * Returns the number of compliances where approvalLevel = &#63; and finalStatus = &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @param finalStatus the final status
	 * @return the number of matching compliances
	 */
	@Override
	public int countByFinalApproved(String approvalLevel, boolean finalStatus) {
		approvalLevel = Objects.toString(approvalLevel, "");

		FinderPath finderPath = _finderPathCountByFinalApproved;

		Object[] finderArgs = new Object[] {approvalLevel, finalStatus};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_COMPLIANCE_WHERE);

			boolean bindApprovalLevel = false;

			if (approvalLevel.isEmpty()) {
				sb.append(_FINDER_COLUMN_FINALAPPROVED_APPROVALLEVEL_3);
			}
			else {
				bindApprovalLevel = true;

				sb.append(_FINDER_COLUMN_FINALAPPROVED_APPROVALLEVEL_2);
			}

			sb.append(_FINDER_COLUMN_FINALAPPROVED_FINALSTATUS_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindApprovalLevel) {
					queryPos.add(approvalLevel);
				}

				queryPos.add(finalStatus);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_FINALAPPROVED_APPROVALLEVEL_2 =
		"compliance.approvalLevel = ? AND ";

	private static final String _FINDER_COLUMN_FINALAPPROVED_APPROVALLEVEL_3 =
		"(compliance.approvalLevel IS NULL OR compliance.approvalLevel = '') AND ";

	private static final String _FINDER_COLUMN_FINALAPPROVED_FINALSTATUS_2 =
		"compliance.finalStatus = ?";

	private FinderPath _finderPathFetchByApproverComments;
	private FinderPath _finderPathCountByApproverComments;

	/**
	 * Returns the compliance where complianceId = &#63; or throws a <code>NoSuchComplianceException</code> if it could not be found.
	 *
	 * @param complianceId the compliance ID
	 * @return the matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	@Override
	public Compliance findByApproverComments(long complianceId)
		throws NoSuchComplianceException {

		Compliance compliance = fetchByApproverComments(complianceId);

		if (compliance == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("complianceId=");
			sb.append(complianceId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchComplianceException(sb.toString());
		}

		return compliance;
	}

	/**
	 * Returns the compliance where complianceId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param complianceId the compliance ID
	 * @return the matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	@Override
	public Compliance fetchByApproverComments(long complianceId) {
		return fetchByApproverComments(complianceId, true);
	}

	/**
	 * Returns the compliance where complianceId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param complianceId the compliance ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	@Override
	public Compliance fetchByApproverComments(
		long complianceId, boolean useFinderCache) {

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {complianceId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByApproverComments, finderArgs, this);
		}

		if (result instanceof Compliance) {
			Compliance compliance = (Compliance)result;

			if (complianceId != compliance.getComplianceId()) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_SELECT_COMPLIANCE_WHERE);

			sb.append(_FINDER_COLUMN_APPROVERCOMMENTS_COMPLIANCEID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(complianceId);

				List<Compliance> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByApproverComments, finderArgs,
							list);
					}
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							if (!useFinderCache) {
								finderArgs = new Object[] {complianceId};
							}

							_log.warn(
								"CompliancePersistenceImpl.fetchByApproverComments(long, boolean) with parameters (" +
									StringUtil.merge(finderArgs) +
										") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					Compliance compliance = list.get(0);

					result = compliance;

					cacheResult(compliance);
				}
			}
			catch (Exception exception) {
				if (useFinderCache) {
					finderCache.removeResult(
						_finderPathFetchByApproverComments, finderArgs);
				}

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Compliance)result;
		}
	}

	/**
	 * Removes the compliance where complianceId = &#63; from the database.
	 *
	 * @param complianceId the compliance ID
	 * @return the compliance that was removed
	 */
	@Override
	public Compliance removeByApproverComments(long complianceId)
		throws NoSuchComplianceException {

		Compliance compliance = findByApproverComments(complianceId);

		return remove(compliance);
	}

	/**
	 * Returns the number of compliances where complianceId = &#63;.
	 *
	 * @param complianceId the compliance ID
	 * @return the number of matching compliances
	 */
	@Override
	public int countByApproverComments(long complianceId) {
		FinderPath finderPath = _finderPathCountByApproverComments;

		Object[] finderArgs = new Object[] {complianceId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_COMPLIANCE_WHERE);

			sb.append(_FINDER_COLUMN_APPROVERCOMMENTS_COMPLIANCEID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(complianceId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_APPROVERCOMMENTS_COMPLIANCEID_2 =
		"compliance.complianceId = ?";

	private FinderPath _finderPathWithPaginationFindByRequestApprovals;
	private FinderPath _finderPathWithoutPaginationFindByRequestApprovals;
	private FinderPath _finderPathCountByRequestApprovals;

	/**
	 * Returns all the compliances where approverVerdict = &#63; and processedFlag = &#63;.
	 *
	 * @param approverVerdict the approver verdict
	 * @param processedFlag the processed flag
	 * @return the matching compliances
	 */
	@Override
	public List<Compliance> findByRequestApprovals(
		String approverVerdict, String processedFlag) {

		return findByRequestApprovals(
			approverVerdict, processedFlag, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the compliances where approverVerdict = &#63; and processedFlag = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param approverVerdict the approver verdict
	 * @param processedFlag the processed flag
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @return the range of matching compliances
	 */
	@Override
	public List<Compliance> findByRequestApprovals(
		String approverVerdict, String processedFlag, int start, int end) {

		return findByRequestApprovals(
			approverVerdict, processedFlag, start, end, null);
	}

	/**
	 * Returns an ordered range of all the compliances where approverVerdict = &#63; and processedFlag = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param approverVerdict the approver verdict
	 * @param processedFlag the processed flag
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching compliances
	 */
	@Override
	public List<Compliance> findByRequestApprovals(
		String approverVerdict, String processedFlag, int start, int end,
		OrderByComparator<Compliance> orderByComparator) {

		return findByRequestApprovals(
			approverVerdict, processedFlag, start, end, orderByComparator,
			true);
	}

	/**
	 * Returns an ordered range of all the compliances where approverVerdict = &#63; and processedFlag = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param approverVerdict the approver verdict
	 * @param processedFlag the processed flag
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching compliances
	 */
	@Override
	public List<Compliance> findByRequestApprovals(
		String approverVerdict, String processedFlag, int start, int end,
		OrderByComparator<Compliance> orderByComparator,
		boolean useFinderCache) {

		approverVerdict = Objects.toString(approverVerdict, "");
		processedFlag = Objects.toString(processedFlag, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByRequestApprovals;
				finderArgs = new Object[] {approverVerdict, processedFlag};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByRequestApprovals;
			finderArgs = new Object[] {
				approverVerdict, processedFlag, start, end, orderByComparator
			};
		}

		List<Compliance> list = null;

		if (useFinderCache) {
			list = (List<Compliance>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Compliance compliance : list) {
					if (!approverVerdict.equals(
							compliance.getApproverVerdict()) ||
						!processedFlag.equals(compliance.getProcessedFlag())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_COMPLIANCE_WHERE);

			boolean bindApproverVerdict = false;

			if (approverVerdict.isEmpty()) {
				sb.append(_FINDER_COLUMN_REQUESTAPPROVALS_APPROVERVERDICT_3);
			}
			else {
				bindApproverVerdict = true;

				sb.append(_FINDER_COLUMN_REQUESTAPPROVALS_APPROVERVERDICT_2);
			}

			boolean bindProcessedFlag = false;

			if (processedFlag.isEmpty()) {
				sb.append(_FINDER_COLUMN_REQUESTAPPROVALS_PROCESSEDFLAG_3);
			}
			else {
				bindProcessedFlag = true;

				sb.append(_FINDER_COLUMN_REQUESTAPPROVALS_PROCESSEDFLAG_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(ComplianceModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindApproverVerdict) {
					queryPos.add(approverVerdict);
				}

				if (bindProcessedFlag) {
					queryPos.add(processedFlag);
				}

				list = (List<Compliance>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				if (useFinderCache) {
					finderCache.removeResult(finderPath, finderArgs);
				}

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first compliance in the ordered set where approverVerdict = &#63; and processedFlag = &#63;.
	 *
	 * @param approverVerdict the approver verdict
	 * @param processedFlag the processed flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	@Override
	public Compliance findByRequestApprovals_First(
			String approverVerdict, String processedFlag,
			OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		Compliance compliance = fetchByRequestApprovals_First(
			approverVerdict, processedFlag, orderByComparator);

		if (compliance != null) {
			return compliance;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("approverVerdict=");
		sb.append(approverVerdict);

		sb.append(", processedFlag=");
		sb.append(processedFlag);

		sb.append("}");

		throw new NoSuchComplianceException(sb.toString());
	}

	/**
	 * Returns the first compliance in the ordered set where approverVerdict = &#63; and processedFlag = &#63;.
	 *
	 * @param approverVerdict the approver verdict
	 * @param processedFlag the processed flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	@Override
	public Compliance fetchByRequestApprovals_First(
		String approverVerdict, String processedFlag,
		OrderByComparator<Compliance> orderByComparator) {

		List<Compliance> list = findByRequestApprovals(
			approverVerdict, processedFlag, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last compliance in the ordered set where approverVerdict = &#63; and processedFlag = &#63;.
	 *
	 * @param approverVerdict the approver verdict
	 * @param processedFlag the processed flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	@Override
	public Compliance findByRequestApprovals_Last(
			String approverVerdict, String processedFlag,
			OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		Compliance compliance = fetchByRequestApprovals_Last(
			approverVerdict, processedFlag, orderByComparator);

		if (compliance != null) {
			return compliance;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("approverVerdict=");
		sb.append(approverVerdict);

		sb.append(", processedFlag=");
		sb.append(processedFlag);

		sb.append("}");

		throw new NoSuchComplianceException(sb.toString());
	}

	/**
	 * Returns the last compliance in the ordered set where approverVerdict = &#63; and processedFlag = &#63;.
	 *
	 * @param approverVerdict the approver verdict
	 * @param processedFlag the processed flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	@Override
	public Compliance fetchByRequestApprovals_Last(
		String approverVerdict, String processedFlag,
		OrderByComparator<Compliance> orderByComparator) {

		int count = countByRequestApprovals(approverVerdict, processedFlag);

		if (count == 0) {
			return null;
		}

		List<Compliance> list = findByRequestApprovals(
			approverVerdict, processedFlag, count - 1, count,
			orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the compliances before and after the current compliance in the ordered set where approverVerdict = &#63; and processedFlag = &#63;.
	 *
	 * @param complianceId the primary key of the current compliance
	 * @param approverVerdict the approver verdict
	 * @param processedFlag the processed flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next compliance
	 * @throws NoSuchComplianceException if a compliance with the primary key could not be found
	 */
	@Override
	public Compliance[] findByRequestApprovals_PrevAndNext(
			long complianceId, String approverVerdict, String processedFlag,
			OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		approverVerdict = Objects.toString(approverVerdict, "");
		processedFlag = Objects.toString(processedFlag, "");

		Compliance compliance = findByPrimaryKey(complianceId);

		Session session = null;

		try {
			session = openSession();

			Compliance[] array = new ComplianceImpl[3];

			array[0] = getByRequestApprovals_PrevAndNext(
				session, compliance, approverVerdict, processedFlag,
				orderByComparator, true);

			array[1] = compliance;

			array[2] = getByRequestApprovals_PrevAndNext(
				session, compliance, approverVerdict, processedFlag,
				orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Compliance getByRequestApprovals_PrevAndNext(
		Session session, Compliance compliance, String approverVerdict,
		String processedFlag, OrderByComparator<Compliance> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_COMPLIANCE_WHERE);

		boolean bindApproverVerdict = false;

		if (approverVerdict.isEmpty()) {
			sb.append(_FINDER_COLUMN_REQUESTAPPROVALS_APPROVERVERDICT_3);
		}
		else {
			bindApproverVerdict = true;

			sb.append(_FINDER_COLUMN_REQUESTAPPROVALS_APPROVERVERDICT_2);
		}

		boolean bindProcessedFlag = false;

		if (processedFlag.isEmpty()) {
			sb.append(_FINDER_COLUMN_REQUESTAPPROVALS_PROCESSEDFLAG_3);
		}
		else {
			bindProcessedFlag = true;

			sb.append(_FINDER_COLUMN_REQUESTAPPROVALS_PROCESSEDFLAG_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(ComplianceModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindApproverVerdict) {
			queryPos.add(approverVerdict);
		}

		if (bindProcessedFlag) {
			queryPos.add(processedFlag);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(compliance)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Compliance> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the compliances where approverVerdict = &#63; and processedFlag = &#63; from the database.
	 *
	 * @param approverVerdict the approver verdict
	 * @param processedFlag the processed flag
	 */
	@Override
	public void removeByRequestApprovals(
		String approverVerdict, String processedFlag) {

		for (Compliance compliance :
				findByRequestApprovals(
					approverVerdict, processedFlag, QueryUtil.ALL_POS,
					QueryUtil.ALL_POS, null)) {

			remove(compliance);
		}
	}

	/**
	 * Returns the number of compliances where approverVerdict = &#63; and processedFlag = &#63;.
	 *
	 * @param approverVerdict the approver verdict
	 * @param processedFlag the processed flag
	 * @return the number of matching compliances
	 */
	@Override
	public int countByRequestApprovals(
		String approverVerdict, String processedFlag) {

		approverVerdict = Objects.toString(approverVerdict, "");
		processedFlag = Objects.toString(processedFlag, "");

		FinderPath finderPath = _finderPathCountByRequestApprovals;

		Object[] finderArgs = new Object[] {approverVerdict, processedFlag};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_COMPLIANCE_WHERE);

			boolean bindApproverVerdict = false;

			if (approverVerdict.isEmpty()) {
				sb.append(_FINDER_COLUMN_REQUESTAPPROVALS_APPROVERVERDICT_3);
			}
			else {
				bindApproverVerdict = true;

				sb.append(_FINDER_COLUMN_REQUESTAPPROVALS_APPROVERVERDICT_2);
			}

			boolean bindProcessedFlag = false;

			if (processedFlag.isEmpty()) {
				sb.append(_FINDER_COLUMN_REQUESTAPPROVALS_PROCESSEDFLAG_3);
			}
			else {
				bindProcessedFlag = true;

				sb.append(_FINDER_COLUMN_REQUESTAPPROVALS_PROCESSEDFLAG_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindApproverVerdict) {
					queryPos.add(approverVerdict);
				}

				if (bindProcessedFlag) {
					queryPos.add(processedFlag);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String
		_FINDER_COLUMN_REQUESTAPPROVALS_APPROVERVERDICT_2 =
			"compliance.approverVerdict = ? AND ";

	private static final String
		_FINDER_COLUMN_REQUESTAPPROVALS_APPROVERVERDICT_3 =
			"(compliance.approverVerdict IS NULL OR compliance.approverVerdict = '') AND ";

	private static final String
		_FINDER_COLUMN_REQUESTAPPROVALS_PROCESSEDFLAG_2 =
			"compliance.processedFlag = ?";

	private static final String
		_FINDER_COLUMN_REQUESTAPPROVALS_PROCESSEDFLAG_3 =
			"(compliance.processedFlag IS NULL OR compliance.processedFlag = '')";

	private FinderPath _finderPathWithPaginationFindByStatus;
	private FinderPath _finderPathWithoutPaginationFindByStatus;
	private FinderPath _finderPathCountByStatus;

	/**
	 * Returns all the compliances where status = &#63;.
	 *
	 * @param status the status
	 * @return the matching compliances
	 */
	@Override
	public List<Compliance> findByStatus(int status) {
		return findByStatus(status, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the compliances where status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param status the status
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @return the range of matching compliances
	 */
	@Override
	public List<Compliance> findByStatus(int status, int start, int end) {
		return findByStatus(status, start, end, null);
	}

	/**
	 * Returns an ordered range of all the compliances where status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param status the status
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching compliances
	 */
	@Override
	public List<Compliance> findByStatus(
		int status, int start, int end,
		OrderByComparator<Compliance> orderByComparator) {

		return findByStatus(status, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the compliances where status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param status the status
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching compliances
	 */
	@Override
	public List<Compliance> findByStatus(
		int status, int start, int end,
		OrderByComparator<Compliance> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByStatus;
				finderArgs = new Object[] {status};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByStatus;
			finderArgs = new Object[] {status, start, end, orderByComparator};
		}

		List<Compliance> list = null;

		if (useFinderCache) {
			list = (List<Compliance>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Compliance compliance : list) {
					if (status != compliance.getStatus()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_COMPLIANCE_WHERE);

			sb.append(_FINDER_COLUMN_STATUS_STATUS_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(ComplianceModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(status);

				list = (List<Compliance>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				if (useFinderCache) {
					finderCache.removeResult(finderPath, finderArgs);
				}

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first compliance in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	@Override
	public Compliance findByStatus_First(
			int status, OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		Compliance compliance = fetchByStatus_First(status, orderByComparator);

		if (compliance != null) {
			return compliance;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("status=");
		sb.append(status);

		sb.append("}");

		throw new NoSuchComplianceException(sb.toString());
	}

	/**
	 * Returns the first compliance in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	@Override
	public Compliance fetchByStatus_First(
		int status, OrderByComparator<Compliance> orderByComparator) {

		List<Compliance> list = findByStatus(status, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last compliance in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	@Override
	public Compliance findByStatus_Last(
			int status, OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		Compliance compliance = fetchByStatus_Last(status, orderByComparator);

		if (compliance != null) {
			return compliance;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("status=");
		sb.append(status);

		sb.append("}");

		throw new NoSuchComplianceException(sb.toString());
	}

	/**
	 * Returns the last compliance in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	@Override
	public Compliance fetchByStatus_Last(
		int status, OrderByComparator<Compliance> orderByComparator) {

		int count = countByStatus(status);

		if (count == 0) {
			return null;
		}

		List<Compliance> list = findByStatus(
			status, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the compliances before and after the current compliance in the ordered set where status = &#63;.
	 *
	 * @param complianceId the primary key of the current compliance
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next compliance
	 * @throws NoSuchComplianceException if a compliance with the primary key could not be found
	 */
	@Override
	public Compliance[] findByStatus_PrevAndNext(
			long complianceId, int status,
			OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		Compliance compliance = findByPrimaryKey(complianceId);

		Session session = null;

		try {
			session = openSession();

			Compliance[] array = new ComplianceImpl[3];

			array[0] = getByStatus_PrevAndNext(
				session, compliance, status, orderByComparator, true);

			array[1] = compliance;

			array[2] = getByStatus_PrevAndNext(
				session, compliance, status, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Compliance getByStatus_PrevAndNext(
		Session session, Compliance compliance, int status,
		OrderByComparator<Compliance> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_COMPLIANCE_WHERE);

		sb.append(_FINDER_COLUMN_STATUS_STATUS_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(ComplianceModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(status);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(compliance)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Compliance> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the compliances where status = &#63; from the database.
	 *
	 * @param status the status
	 */
	@Override
	public void removeByStatus(int status) {
		for (Compliance compliance :
				findByStatus(
					status, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(compliance);
		}
	}

	/**
	 * Returns the number of compliances where status = &#63;.
	 *
	 * @param status the status
	 * @return the number of matching compliances
	 */
	@Override
	public int countByStatus(int status) {
		FinderPath finderPath = _finderPathCountByStatus;

		Object[] finderArgs = new Object[] {status};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_COMPLIANCE_WHERE);

			sb.append(_FINDER_COLUMN_STATUS_STATUS_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(status);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_STATUS_STATUS_2 =
		"compliance.status = ?";

	private FinderPath _finderPathWithPaginationFindByG_S;
	private FinderPath _finderPathWithoutPaginationFindByG_S;
	private FinderPath _finderPathCountByG_S;

	/**
	 * Returns all the compliances where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @return the matching compliances
	 */
	@Override
	public List<Compliance> findByG_S(long groupId, int status) {
		return findByG_S(
			groupId, status, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the compliances where groupId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @return the range of matching compliances
	 */
	@Override
	public List<Compliance> findByG_S(
		long groupId, int status, int start, int end) {

		return findByG_S(groupId, status, start, end, null);
	}

	/**
	 * Returns an ordered range of all the compliances where groupId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching compliances
	 */
	@Override
	public List<Compliance> findByG_S(
		long groupId, int status, int start, int end,
		OrderByComparator<Compliance> orderByComparator) {

		return findByG_S(groupId, status, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the compliances where groupId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching compliances
	 */
	@Override
	public List<Compliance> findByG_S(
		long groupId, int status, int start, int end,
		OrderByComparator<Compliance> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByG_S;
				finderArgs = new Object[] {groupId, status};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByG_S;
			finderArgs = new Object[] {
				groupId, status, start, end, orderByComparator
			};
		}

		List<Compliance> list = null;

		if (useFinderCache) {
			list = (List<Compliance>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Compliance compliance : list) {
					if ((groupId != compliance.getGroupId()) ||
						(status != compliance.getStatus())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_COMPLIANCE_WHERE);

			sb.append(_FINDER_COLUMN_G_S_GROUPID_2);

			sb.append(_FINDER_COLUMN_G_S_STATUS_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(ComplianceModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				queryPos.add(status);

				list = (List<Compliance>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				if (useFinderCache) {
					finderCache.removeResult(finderPath, finderArgs);
				}

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first compliance in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	@Override
	public Compliance findByG_S_First(
			long groupId, int status,
			OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		Compliance compliance = fetchByG_S_First(
			groupId, status, orderByComparator);

		if (compliance != null) {
			return compliance;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append(", status=");
		sb.append(status);

		sb.append("}");

		throw new NoSuchComplianceException(sb.toString());
	}

	/**
	 * Returns the first compliance in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	@Override
	public Compliance fetchByG_S_First(
		long groupId, int status,
		OrderByComparator<Compliance> orderByComparator) {

		List<Compliance> list = findByG_S(
			groupId, status, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last compliance in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	@Override
	public Compliance findByG_S_Last(
			long groupId, int status,
			OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		Compliance compliance = fetchByG_S_Last(
			groupId, status, orderByComparator);

		if (compliance != null) {
			return compliance;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append(", status=");
		sb.append(status);

		sb.append("}");

		throw new NoSuchComplianceException(sb.toString());
	}

	/**
	 * Returns the last compliance in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	@Override
	public Compliance fetchByG_S_Last(
		long groupId, int status,
		OrderByComparator<Compliance> orderByComparator) {

		int count = countByG_S(groupId, status);

		if (count == 0) {
			return null;
		}

		List<Compliance> list = findByG_S(
			groupId, status, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the compliances before and after the current compliance in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param complianceId the primary key of the current compliance
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next compliance
	 * @throws NoSuchComplianceException if a compliance with the primary key could not be found
	 */
	@Override
	public Compliance[] findByG_S_PrevAndNext(
			long complianceId, long groupId, int status,
			OrderByComparator<Compliance> orderByComparator)
		throws NoSuchComplianceException {

		Compliance compliance = findByPrimaryKey(complianceId);

		Session session = null;

		try {
			session = openSession();

			Compliance[] array = new ComplianceImpl[3];

			array[0] = getByG_S_PrevAndNext(
				session, compliance, groupId, status, orderByComparator, true);

			array[1] = compliance;

			array[2] = getByG_S_PrevAndNext(
				session, compliance, groupId, status, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Compliance getByG_S_PrevAndNext(
		Session session, Compliance compliance, long groupId, int status,
		OrderByComparator<Compliance> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_COMPLIANCE_WHERE);

		sb.append(_FINDER_COLUMN_G_S_GROUPID_2);

		sb.append(_FINDER_COLUMN_G_S_STATUS_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(ComplianceModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(groupId);

		queryPos.add(status);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(compliance)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Compliance> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the compliances where groupId = &#63; and status = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 */
	@Override
	public void removeByG_S(long groupId, int status) {
		for (Compliance compliance :
				findByG_S(
					groupId, status, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(compliance);
		}
	}

	/**
	 * Returns the number of compliances where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @return the number of matching compliances
	 */
	@Override
	public int countByG_S(long groupId, int status) {
		FinderPath finderPath = _finderPathCountByG_S;

		Object[] finderArgs = new Object[] {groupId, status};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_COMPLIANCE_WHERE);

			sb.append(_FINDER_COLUMN_G_S_GROUPID_2);

			sb.append(_FINDER_COLUMN_G_S_STATUS_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				queryPos.add(status);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_G_S_GROUPID_2 =
		"compliance.groupId = ? AND ";

	private static final String _FINDER_COLUMN_G_S_STATUS_2 =
		"compliance.status = ?";

	public CompliancePersistenceImpl() {
		setModelClass(Compliance.class);

		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");

		try {
			Field field = BasePersistenceImpl.class.getDeclaredField(
				"_dbColumnNames");

			field.setAccessible(true);

			field.set(this, dbColumnNames);
		}
		catch (Exception exception) {
			if (_log.isDebugEnabled()) {
				_log.debug(exception, exception);
			}
		}
	}

	/**
	 * Caches the compliance in the entity cache if it is enabled.
	 *
	 * @param compliance the compliance
	 */
	@Override
	public void cacheResult(Compliance compliance) {
		entityCache.putResult(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED, ComplianceImpl.class,
			compliance.getPrimaryKey(), compliance);

		finderCache.putResult(
			_finderPathFetchByUUID_G,
			new Object[] {compliance.getUuid(), compliance.getGroupId()},
			compliance);

		finderCache.putResult(
			_finderPathFetchByApproverComments,
			new Object[] {compliance.getComplianceId()}, compliance);

		compliance.resetOriginalValues();
	}

	/**
	 * Caches the compliances in the entity cache if it is enabled.
	 *
	 * @param compliances the compliances
	 */
	@Override
	public void cacheResult(List<Compliance> compliances) {
		for (Compliance compliance : compliances) {
			if (entityCache.getResult(
					ComplianceModelImpl.ENTITY_CACHE_ENABLED,
					ComplianceImpl.class, compliance.getPrimaryKey()) == null) {

				cacheResult(compliance);
			}
			else {
				compliance.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all compliances.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ComplianceImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the compliance.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Compliance compliance) {
		entityCache.removeResult(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED, ComplianceImpl.class,
			compliance.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((ComplianceModelImpl)compliance, true);
	}

	@Override
	public void clearCache(List<Compliance> compliances) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Compliance compliance : compliances) {
			entityCache.removeResult(
				ComplianceModelImpl.ENTITY_CACHE_ENABLED, ComplianceImpl.class,
				compliance.getPrimaryKey());

			clearUniqueFindersCache((ComplianceModelImpl)compliance, true);
		}
	}

	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(
				ComplianceModelImpl.ENTITY_CACHE_ENABLED, ComplianceImpl.class,
				primaryKey);
		}
	}

	protected void cacheUniqueFindersCache(
		ComplianceModelImpl complianceModelImpl) {

		Object[] args = new Object[] {
			complianceModelImpl.getUuid(), complianceModelImpl.getGroupId()
		};

		finderCache.putResult(
			_finderPathCountByUUID_G, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByUUID_G, args, complianceModelImpl, false);

		args = new Object[] {complianceModelImpl.getComplianceId()};

		finderCache.putResult(
			_finderPathCountByApproverComments, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByApproverComments, args, complianceModelImpl,
			false);
	}

	protected void clearUniqueFindersCache(
		ComplianceModelImpl complianceModelImpl, boolean clearCurrent) {

		if (clearCurrent) {
			Object[] args = new Object[] {
				complianceModelImpl.getUuid(), complianceModelImpl.getGroupId()
			};

			finderCache.removeResult(_finderPathCountByUUID_G, args);
			finderCache.removeResult(_finderPathFetchByUUID_G, args);
		}

		if ((complianceModelImpl.getColumnBitmask() &
			 _finderPathFetchByUUID_G.getColumnBitmask()) != 0) {

			Object[] args = new Object[] {
				complianceModelImpl.getOriginalUuid(),
				complianceModelImpl.getOriginalGroupId()
			};

			finderCache.removeResult(_finderPathCountByUUID_G, args);
			finderCache.removeResult(_finderPathFetchByUUID_G, args);
		}

		if (clearCurrent) {
			Object[] args = new Object[] {
				complianceModelImpl.getComplianceId()
			};

			finderCache.removeResult(_finderPathCountByApproverComments, args);
			finderCache.removeResult(_finderPathFetchByApproverComments, args);
		}

		if ((complianceModelImpl.getColumnBitmask() &
			 _finderPathFetchByApproverComments.getColumnBitmask()) != 0) {

			Object[] args = new Object[] {
				complianceModelImpl.getOriginalComplianceId()
			};

			finderCache.removeResult(_finderPathCountByApproverComments, args);
			finderCache.removeResult(_finderPathFetchByApproverComments, args);
		}
	}

	/**
	 * Creates a new compliance with the primary key. Does not add the compliance to the database.
	 *
	 * @param complianceId the primary key for the new compliance
	 * @return the new compliance
	 */
	@Override
	public Compliance create(long complianceId) {
		Compliance compliance = new ComplianceImpl();

		compliance.setNew(true);
		compliance.setPrimaryKey(complianceId);

		String uuid = PortalUUIDUtil.generate();

		compliance.setUuid(uuid);

		compliance.setCompanyId(CompanyThreadLocal.getCompanyId());

		return compliance;
	}

	/**
	 * Removes the compliance with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param complianceId the primary key of the compliance
	 * @return the compliance that was removed
	 * @throws NoSuchComplianceException if a compliance with the primary key could not be found
	 */
	@Override
	public Compliance remove(long complianceId)
		throws NoSuchComplianceException {

		return remove((Serializable)complianceId);
	}

	/**
	 * Removes the compliance with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the compliance
	 * @return the compliance that was removed
	 * @throws NoSuchComplianceException if a compliance with the primary key could not be found
	 */
	@Override
	public Compliance remove(Serializable primaryKey)
		throws NoSuchComplianceException {

		Session session = null;

		try {
			session = openSession();

			Compliance compliance = (Compliance)session.get(
				ComplianceImpl.class, primaryKey);

			if (compliance == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchComplianceException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(compliance);
		}
		catch (NoSuchComplianceException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Compliance removeImpl(Compliance compliance) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(compliance)) {
				compliance = (Compliance)session.get(
					ComplianceImpl.class, compliance.getPrimaryKeyObj());
			}

			if (compliance != null) {
				session.delete(compliance);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (compliance != null) {
			clearCache(compliance);
		}

		return compliance;
	}

	@Override
	public Compliance updateImpl(Compliance compliance) {
		boolean isNew = compliance.isNew();

		if (!(compliance instanceof ComplianceModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(compliance.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(compliance);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in compliance proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom Compliance implementation " +
					compliance.getClass());
		}

		ComplianceModelImpl complianceModelImpl =
			(ComplianceModelImpl)compliance;

		if (Validator.isNull(compliance.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			compliance.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (compliance.getCreateDate() == null)) {
			if (serviceContext == null) {
				compliance.setCreateDate(now);
			}
			else {
				compliance.setCreateDate(serviceContext.getCreateDate(now));
			}
		}

		if (!complianceModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				compliance.setModifiedDate(now);
			}
			else {
				compliance.setModifiedDate(serviceContext.getModifiedDate(now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (compliance.isNew()) {
				session.save(compliance);

				compliance.setNew(false);
			}
			else {
				compliance = (Compliance)session.merge(compliance);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!ComplianceModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else if (isNew) {
			Object[] args = new Object[] {complianceModelImpl.getUuid()};

			finderCache.removeResult(_finderPathCountByUuid, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByUuid, args);

			args = new Object[] {
				complianceModelImpl.getUuid(),
				complianceModelImpl.getCompanyId()
			};

			finderCache.removeResult(_finderPathCountByUuid_C, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByUuid_C, args);

			args = new Object[] {complianceModelImpl.getEntityId()};

			finderCache.removeResult(_finderPathCountByEntityId, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByEntityId, args);

			args = new Object[] {complianceModelImpl.getApprovalLevel()};

			finderCache.removeResult(_finderPathCountByApprovalLevel, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByApprovalLevel, args);

			args = new Object[] {
				complianceModelImpl.isFinalStatus(),
				complianceModelImpl.getProcessedFlag()
			};

			finderCache.removeResult(
				_finderPathCountByUnprocessedFinalStatus, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByUnprocessedFinalStatus, args);

			args = new Object[] {
				complianceModelImpl.getApprovalLevel(),
				complianceModelImpl.isFinalStatus()
			};

			finderCache.removeResult(_finderPathCountByFinalApproved, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByFinalApproved, args);

			args = new Object[] {
				complianceModelImpl.getApproverVerdict(),
				complianceModelImpl.getProcessedFlag()
			};

			finderCache.removeResult(_finderPathCountByRequestApprovals, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByRequestApprovals, args);

			args = new Object[] {complianceModelImpl.getStatus()};

			finderCache.removeResult(_finderPathCountByStatus, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByStatus, args);

			args = new Object[] {
				complianceModelImpl.getGroupId(),
				complianceModelImpl.getStatus()
			};

			finderCache.removeResult(_finderPathCountByG_S, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByG_S, args);

			finderCache.removeResult(_finderPathCountAll, FINDER_ARGS_EMPTY);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindAll, FINDER_ARGS_EMPTY);
		}
		else {
			if ((complianceModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByUuid.getColumnBitmask()) !=
					 0) {

				Object[] args = new Object[] {
					complianceModelImpl.getOriginalUuid()
				};

				finderCache.removeResult(_finderPathCountByUuid, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid, args);

				args = new Object[] {complianceModelImpl.getUuid()};

				finderCache.removeResult(_finderPathCountByUuid, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid, args);
			}

			if ((complianceModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByUuid_C.getColumnBitmask()) !=
					 0) {

				Object[] args = new Object[] {
					complianceModelImpl.getOriginalUuid(),
					complianceModelImpl.getOriginalCompanyId()
				};

				finderCache.removeResult(_finderPathCountByUuid_C, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid_C, args);

				args = new Object[] {
					complianceModelImpl.getUuid(),
					complianceModelImpl.getCompanyId()
				};

				finderCache.removeResult(_finderPathCountByUuid_C, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid_C, args);
			}

			if ((complianceModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByEntityId.
					 getColumnBitmask()) != 0) {

				Object[] args = new Object[] {
					complianceModelImpl.getOriginalEntityId()
				};

				finderCache.removeResult(_finderPathCountByEntityId, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByEntityId, args);

				args = new Object[] {complianceModelImpl.getEntityId()};

				finderCache.removeResult(_finderPathCountByEntityId, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByEntityId, args);
			}

			if ((complianceModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByApprovalLevel.
					 getColumnBitmask()) != 0) {

				Object[] args = new Object[] {
					complianceModelImpl.getOriginalApprovalLevel()
				};

				finderCache.removeResult(_finderPathCountByApprovalLevel, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByApprovalLevel, args);

				args = new Object[] {complianceModelImpl.getApprovalLevel()};

				finderCache.removeResult(_finderPathCountByApprovalLevel, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByApprovalLevel, args);
			}

			if ((complianceModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByUnprocessedFinalStatus.
					 getColumnBitmask()) != 0) {

				Object[] args = new Object[] {
					complianceModelImpl.getOriginalFinalStatus(),
					complianceModelImpl.getOriginalProcessedFlag()
				};

				finderCache.removeResult(
					_finderPathCountByUnprocessedFinalStatus, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUnprocessedFinalStatus,
					args);

				args = new Object[] {
					complianceModelImpl.isFinalStatus(),
					complianceModelImpl.getProcessedFlag()
				};

				finderCache.removeResult(
					_finderPathCountByUnprocessedFinalStatus, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUnprocessedFinalStatus,
					args);
			}

			if ((complianceModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByFinalApproved.
					 getColumnBitmask()) != 0) {

				Object[] args = new Object[] {
					complianceModelImpl.getOriginalApprovalLevel(),
					complianceModelImpl.getOriginalFinalStatus()
				};

				finderCache.removeResult(_finderPathCountByFinalApproved, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByFinalApproved, args);

				args = new Object[] {
					complianceModelImpl.getApprovalLevel(),
					complianceModelImpl.isFinalStatus()
				};

				finderCache.removeResult(_finderPathCountByFinalApproved, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByFinalApproved, args);
			}

			if ((complianceModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByRequestApprovals.
					 getColumnBitmask()) != 0) {

				Object[] args = new Object[] {
					complianceModelImpl.getOriginalApproverVerdict(),
					complianceModelImpl.getOriginalProcessedFlag()
				};

				finderCache.removeResult(
					_finderPathCountByRequestApprovals, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByRequestApprovals, args);

				args = new Object[] {
					complianceModelImpl.getApproverVerdict(),
					complianceModelImpl.getProcessedFlag()
				};

				finderCache.removeResult(
					_finderPathCountByRequestApprovals, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByRequestApprovals, args);
			}

			if ((complianceModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByStatus.getColumnBitmask()) !=
					 0) {

				Object[] args = new Object[] {
					complianceModelImpl.getOriginalStatus()
				};

				finderCache.removeResult(_finderPathCountByStatus, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByStatus, args);

				args = new Object[] {complianceModelImpl.getStatus()};

				finderCache.removeResult(_finderPathCountByStatus, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByStatus, args);
			}

			if ((complianceModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByG_S.getColumnBitmask()) !=
					 0) {

				Object[] args = new Object[] {
					complianceModelImpl.getOriginalGroupId(),
					complianceModelImpl.getOriginalStatus()
				};

				finderCache.removeResult(_finderPathCountByG_S, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByG_S, args);

				args = new Object[] {
					complianceModelImpl.getGroupId(),
					complianceModelImpl.getStatus()
				};

				finderCache.removeResult(_finderPathCountByG_S, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByG_S, args);
			}
		}

		entityCache.putResult(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED, ComplianceImpl.class,
			compliance.getPrimaryKey(), compliance, false);

		clearUniqueFindersCache(complianceModelImpl, false);
		cacheUniqueFindersCache(complianceModelImpl);

		compliance.resetOriginalValues();

		return compliance;
	}

	/**
	 * Returns the compliance with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the compliance
	 * @return the compliance
	 * @throws NoSuchComplianceException if a compliance with the primary key could not be found
	 */
	@Override
	public Compliance findByPrimaryKey(Serializable primaryKey)
		throws NoSuchComplianceException {

		Compliance compliance = fetchByPrimaryKey(primaryKey);

		if (compliance == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchComplianceException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return compliance;
	}

	/**
	 * Returns the compliance with the primary key or throws a <code>NoSuchComplianceException</code> if it could not be found.
	 *
	 * @param complianceId the primary key of the compliance
	 * @return the compliance
	 * @throws NoSuchComplianceException if a compliance with the primary key could not be found
	 */
	@Override
	public Compliance findByPrimaryKey(long complianceId)
		throws NoSuchComplianceException {

		return findByPrimaryKey((Serializable)complianceId);
	}

	/**
	 * Returns the compliance with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the compliance
	 * @return the compliance, or <code>null</code> if a compliance with the primary key could not be found
	 */
	@Override
	public Compliance fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED, ComplianceImpl.class,
			primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Compliance compliance = (Compliance)serializable;

		if (compliance == null) {
			Session session = null;

			try {
				session = openSession();

				compliance = (Compliance)session.get(
					ComplianceImpl.class, primaryKey);

				if (compliance != null) {
					cacheResult(compliance);
				}
				else {
					entityCache.putResult(
						ComplianceModelImpl.ENTITY_CACHE_ENABLED,
						ComplianceImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception exception) {
				entityCache.removeResult(
					ComplianceModelImpl.ENTITY_CACHE_ENABLED,
					ComplianceImpl.class, primaryKey);

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return compliance;
	}

	/**
	 * Returns the compliance with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param complianceId the primary key of the compliance
	 * @return the compliance, or <code>null</code> if a compliance with the primary key could not be found
	 */
	@Override
	public Compliance fetchByPrimaryKey(long complianceId) {
		return fetchByPrimaryKey((Serializable)complianceId);
	}

	@Override
	public Map<Serializable, Compliance> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Compliance> map =
			new HashMap<Serializable, Compliance>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Compliance compliance = fetchByPrimaryKey(primaryKey);

			if (compliance != null) {
				map.put(primaryKey, compliance);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(
				ComplianceModelImpl.ENTITY_CACHE_ENABLED, ComplianceImpl.class,
				primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Compliance)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler sb = new StringBundler(
			uncachedPrimaryKeys.size() * 2 + 1);

		sb.append(_SQL_SELECT_COMPLIANCE_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			sb.append((long)primaryKey);

			sb.append(",");
		}

		sb.setIndex(sb.index() - 1);

		sb.append(")");

		String sql = sb.toString();

		Session session = null;

		try {
			session = openSession();

			Query query = session.createQuery(sql);

			for (Compliance compliance : (List<Compliance>)query.list()) {
				map.put(compliance.getPrimaryKeyObj(), compliance);

				cacheResult(compliance);

				uncachedPrimaryKeys.remove(compliance.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(
					ComplianceModelImpl.ENTITY_CACHE_ENABLED,
					ComplianceImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the compliances.
	 *
	 * @return the compliances
	 */
	@Override
	public List<Compliance> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the compliances.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @return the range of compliances
	 */
	@Override
	public List<Compliance> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the compliances.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of compliances
	 */
	@Override
	public List<Compliance> findAll(
		int start, int end, OrderByComparator<Compliance> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the compliances.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of compliances
	 */
	@Override
	public List<Compliance> findAll(
		int start, int end, OrderByComparator<Compliance> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<Compliance> list = null;

		if (useFinderCache) {
			list = (List<Compliance>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_COMPLIANCE);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_COMPLIANCE;

				sql = sql.concat(ComplianceModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<Compliance>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				if (useFinderCache) {
					finderCache.removeResult(finderPath, finderArgs);
				}

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the compliances from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Compliance compliance : findAll()) {
			remove(compliance);
		}
	}

	/**
	 * Returns the number of compliances.
	 *
	 * @return the number of compliances
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_COMPLIANCE);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				finderCache.removeResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY);

				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ComplianceModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the compliance persistence.
	 */
	public void afterPropertiesSet() {
		_finderPathWithPaginationFindAll = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, ComplianceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, ComplianceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll",
			new String[0]);

		_finderPathCountAll = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0]);

		_finderPathWithPaginationFindByUuid = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, ComplianceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByUuid = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, ComplianceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()},
			ComplianceModelImpl.UUID_COLUMN_BITMASK);

		_finderPathCountByUuid = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()});

		_finderPathFetchByUUID_G = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, ComplianceImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			ComplianceModelImpl.UUID_COLUMN_BITMASK |
			ComplianceModelImpl.GROUPID_COLUMN_BITMASK);

		_finderPathCountByUUID_G = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()});

		_finderPathWithPaginationFindByUuid_C = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, ComplianceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByUuid_C = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, ComplianceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			ComplianceModelImpl.UUID_COLUMN_BITMASK |
			ComplianceModelImpl.COMPANYID_COLUMN_BITMASK);

		_finderPathCountByUuid_C = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()});

		_finderPathWithPaginationFindByEntityId = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, ComplianceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByEntityId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByEntityId = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, ComplianceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByEntityId",
			new String[] {Long.class.getName()},
			ComplianceModelImpl.ENTITYID_COLUMN_BITMASK);

		_finderPathCountByEntityId = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByEntityId",
			new String[] {Long.class.getName()});

		_finderPathWithPaginationFindByApprovalLevel = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, ComplianceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByApprovalLevel",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByApprovalLevel = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, ComplianceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByApprovalLevel",
			new String[] {String.class.getName()},
			ComplianceModelImpl.APPROVALLEVEL_COLUMN_BITMASK);

		_finderPathCountByApprovalLevel = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByApprovalLevel",
			new String[] {String.class.getName()});

		_finderPathWithPaginationFindByUnprocessedFinalStatus = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, ComplianceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByUnprocessedFinalStatus",
			new String[] {
				Boolean.class.getName(), String.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByUnprocessedFinalStatus =
			new FinderPath(
				ComplianceModelImpl.ENTITY_CACHE_ENABLED,
				ComplianceModelImpl.FINDER_CACHE_ENABLED, ComplianceImpl.class,
				FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
				"findByUnprocessedFinalStatus",
				new String[] {Boolean.class.getName(), String.class.getName()},
				ComplianceModelImpl.FINALSTATUS_COLUMN_BITMASK |
				ComplianceModelImpl.PROCESSEDFLAG_COLUMN_BITMASK);

		_finderPathCountByUnprocessedFinalStatus = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByUnprocessedFinalStatus",
			new String[] {Boolean.class.getName(), String.class.getName()});

		_finderPathWithPaginationFindByFinalApproved = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, ComplianceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByFinalApproved",
			new String[] {
				String.class.getName(), Boolean.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByFinalApproved = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, ComplianceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByFinalApproved",
			new String[] {String.class.getName(), Boolean.class.getName()},
			ComplianceModelImpl.APPROVALLEVEL_COLUMN_BITMASK |
			ComplianceModelImpl.FINALSTATUS_COLUMN_BITMASK);

		_finderPathCountByFinalApproved = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByFinalApproved",
			new String[] {String.class.getName(), Boolean.class.getName()});

		_finderPathFetchByApproverComments = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, ComplianceImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByApproverComments",
			new String[] {Long.class.getName()},
			ComplianceModelImpl.COMPLIANCEID_COLUMN_BITMASK);

		_finderPathCountByApproverComments = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByApproverComments", new String[] {Long.class.getName()});

		_finderPathWithPaginationFindByRequestApprovals = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, ComplianceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByRequestApprovals",
			new String[] {
				String.class.getName(), String.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByRequestApprovals = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, ComplianceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByRequestApprovals",
			new String[] {String.class.getName(), String.class.getName()},
			ComplianceModelImpl.APPROVERVERDICT_COLUMN_BITMASK |
			ComplianceModelImpl.PROCESSEDFLAG_COLUMN_BITMASK);

		_finderPathCountByRequestApprovals = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByRequestApprovals",
			new String[] {String.class.getName(), String.class.getName()});

		_finderPathWithPaginationFindByStatus = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, ComplianceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByStatus",
			new String[] {
				Integer.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByStatus = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, ComplianceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByStatus",
			new String[] {Integer.class.getName()},
			ComplianceModelImpl.STATUS_COLUMN_BITMASK);

		_finderPathCountByStatus = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByStatus",
			new String[] {Integer.class.getName()});

		_finderPathWithPaginationFindByG_S = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, ComplianceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByG_S",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByG_S = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, ComplianceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByG_S",
			new String[] {Long.class.getName(), Integer.class.getName()},
			ComplianceModelImpl.GROUPID_COLUMN_BITMASK |
			ComplianceModelImpl.STATUS_COLUMN_BITMASK);

		_finderPathCountByG_S = new FinderPath(
			ComplianceModelImpl.ENTITY_CACHE_ENABLED,
			ComplianceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByG_S",
			new String[] {Long.class.getName(), Integer.class.getName()});
	}

	public void destroy() {
		entityCache.removeCache(ComplianceImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;

	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_COMPLIANCE =
		"SELECT compliance FROM Compliance compliance";

	private static final String _SQL_SELECT_COMPLIANCE_WHERE_PKS_IN =
		"SELECT compliance FROM Compliance compliance WHERE complianceId IN (";

	private static final String _SQL_SELECT_COMPLIANCE_WHERE =
		"SELECT compliance FROM Compliance compliance WHERE ";

	private static final String _SQL_COUNT_COMPLIANCE =
		"SELECT COUNT(compliance) FROM Compliance compliance";

	private static final String _SQL_COUNT_COMPLIANCE_WHERE =
		"SELECT COUNT(compliance) FROM Compliance compliance WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "compliance.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No Compliance exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No Compliance exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		CompliancePersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid"});

}