/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.compliance.service.service.persistence;

import com.compliance.service.model.Compliance;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The persistence utility for the compliance service. This utility wraps <code>com.compliance.service.service.persistence.impl.CompliancePersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CompliancePersistence
 * @generated
 */
public class ComplianceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Compliance compliance) {
		getPersistence().clearCache(compliance);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Compliance> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Compliance> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Compliance> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Compliance> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Compliance update(Compliance compliance) {
		return getPersistence().update(compliance);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Compliance update(
		Compliance compliance, ServiceContext serviceContext) {

		return getPersistence().update(compliance, serviceContext);
	}

	/**
	 * Returns all the compliances where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching compliances
	 */
	public static List<Compliance> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the compliances where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @return the range of matching compliances
	 */
	public static List<Compliance> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the compliances where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching compliances
	 */
	public static List<Compliance> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the compliances where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching compliances
	 */
	public static List<Compliance> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Compliance> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first compliance in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	public static Compliance findByUuid_First(
			String uuid, OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first compliance in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	public static Compliance fetchByUuid_First(
		String uuid, OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last compliance in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	public static Compliance findByUuid_Last(
			String uuid, OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last compliance in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	public static Compliance fetchByUuid_Last(
		String uuid, OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the compliances before and after the current compliance in the ordered set where uuid = &#63;.
	 *
	 * @param complianceId the primary key of the current compliance
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next compliance
	 * @throws NoSuchComplianceException if a compliance with the primary key could not be found
	 */
	public static Compliance[] findByUuid_PrevAndNext(
			long complianceId, String uuid,
			OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByUuid_PrevAndNext(
			complianceId, uuid, orderByComparator);
	}

	/**
	 * Removes all the compliances where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of compliances where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching compliances
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the compliance where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchComplianceException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	public static Compliance findByUUID_G(String uuid, long groupId)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the compliance where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	public static Compliance fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the compliance where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	public static Compliance fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the compliance where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the compliance that was removed
	 */
	public static Compliance removeByUUID_G(String uuid, long groupId)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of compliances where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching compliances
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the compliances where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching compliances
	 */
	public static List<Compliance> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the compliances where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @return the range of matching compliances
	 */
	public static List<Compliance> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the compliances where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching compliances
	 */
	public static List<Compliance> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the compliances where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching compliances
	 */
	public static List<Compliance> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Compliance> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first compliance in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	public static Compliance findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first compliance in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	public static Compliance fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last compliance in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	public static Compliance findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last compliance in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	public static Compliance fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the compliances before and after the current compliance in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param complianceId the primary key of the current compliance
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next compliance
	 * @throws NoSuchComplianceException if a compliance with the primary key could not be found
	 */
	public static Compliance[] findByUuid_C_PrevAndNext(
			long complianceId, String uuid, long companyId,
			OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByUuid_C_PrevAndNext(
			complianceId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the compliances where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of compliances where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching compliances
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the compliances where entityId = &#63;.
	 *
	 * @param entityId the entity ID
	 * @return the matching compliances
	 */
	public static List<Compliance> findByEntityId(long entityId) {
		return getPersistence().findByEntityId(entityId);
	}

	/**
	 * Returns a range of all the compliances where entityId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param entityId the entity ID
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @return the range of matching compliances
	 */
	public static List<Compliance> findByEntityId(
		long entityId, int start, int end) {

		return getPersistence().findByEntityId(entityId, start, end);
	}

	/**
	 * Returns an ordered range of all the compliances where entityId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param entityId the entity ID
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching compliances
	 */
	public static List<Compliance> findByEntityId(
		long entityId, int start, int end,
		OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().findByEntityId(
			entityId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the compliances where entityId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param entityId the entity ID
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching compliances
	 */
	public static List<Compliance> findByEntityId(
		long entityId, int start, int end,
		OrderByComparator<Compliance> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByEntityId(
			entityId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first compliance in the ordered set where entityId = &#63;.
	 *
	 * @param entityId the entity ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	public static Compliance findByEntityId_First(
			long entityId, OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByEntityId_First(
			entityId, orderByComparator);
	}

	/**
	 * Returns the first compliance in the ordered set where entityId = &#63;.
	 *
	 * @param entityId the entity ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	public static Compliance fetchByEntityId_First(
		long entityId, OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().fetchByEntityId_First(
			entityId, orderByComparator);
	}

	/**
	 * Returns the last compliance in the ordered set where entityId = &#63;.
	 *
	 * @param entityId the entity ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	public static Compliance findByEntityId_Last(
			long entityId, OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByEntityId_Last(
			entityId, orderByComparator);
	}

	/**
	 * Returns the last compliance in the ordered set where entityId = &#63;.
	 *
	 * @param entityId the entity ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	public static Compliance fetchByEntityId_Last(
		long entityId, OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().fetchByEntityId_Last(
			entityId, orderByComparator);
	}

	/**
	 * Returns the compliances before and after the current compliance in the ordered set where entityId = &#63;.
	 *
	 * @param complianceId the primary key of the current compliance
	 * @param entityId the entity ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next compliance
	 * @throws NoSuchComplianceException if a compliance with the primary key could not be found
	 */
	public static Compliance[] findByEntityId_PrevAndNext(
			long complianceId, long entityId,
			OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByEntityId_PrevAndNext(
			complianceId, entityId, orderByComparator);
	}

	/**
	 * Removes all the compliances where entityId = &#63; from the database.
	 *
	 * @param entityId the entity ID
	 */
	public static void removeByEntityId(long entityId) {
		getPersistence().removeByEntityId(entityId);
	}

	/**
	 * Returns the number of compliances where entityId = &#63;.
	 *
	 * @param entityId the entity ID
	 * @return the number of matching compliances
	 */
	public static int countByEntityId(long entityId) {
		return getPersistence().countByEntityId(entityId);
	}

	/**
	 * Returns all the compliances where approvalLevel = &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @return the matching compliances
	 */
	public static List<Compliance> findByApprovalLevel(String approvalLevel) {
		return getPersistence().findByApprovalLevel(approvalLevel);
	}

	/**
	 * Returns a range of all the compliances where approvalLevel = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param approvalLevel the approval level
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @return the range of matching compliances
	 */
	public static List<Compliance> findByApprovalLevel(
		String approvalLevel, int start, int end) {

		return getPersistence().findByApprovalLevel(approvalLevel, start, end);
	}

	/**
	 * Returns an ordered range of all the compliances where approvalLevel = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param approvalLevel the approval level
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching compliances
	 */
	public static List<Compliance> findByApprovalLevel(
		String approvalLevel, int start, int end,
		OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().findByApprovalLevel(
			approvalLevel, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the compliances where approvalLevel = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param approvalLevel the approval level
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching compliances
	 */
	public static List<Compliance> findByApprovalLevel(
		String approvalLevel, int start, int end,
		OrderByComparator<Compliance> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByApprovalLevel(
			approvalLevel, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first compliance in the ordered set where approvalLevel = &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	public static Compliance findByApprovalLevel_First(
			String approvalLevel,
			OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByApprovalLevel_First(
			approvalLevel, orderByComparator);
	}

	/**
	 * Returns the first compliance in the ordered set where approvalLevel = &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	public static Compliance fetchByApprovalLevel_First(
		String approvalLevel, OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().fetchByApprovalLevel_First(
			approvalLevel, orderByComparator);
	}

	/**
	 * Returns the last compliance in the ordered set where approvalLevel = &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	public static Compliance findByApprovalLevel_Last(
			String approvalLevel,
			OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByApprovalLevel_Last(
			approvalLevel, orderByComparator);
	}

	/**
	 * Returns the last compliance in the ordered set where approvalLevel = &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	public static Compliance fetchByApprovalLevel_Last(
		String approvalLevel, OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().fetchByApprovalLevel_Last(
			approvalLevel, orderByComparator);
	}

	/**
	 * Returns the compliances before and after the current compliance in the ordered set where approvalLevel = &#63;.
	 *
	 * @param complianceId the primary key of the current compliance
	 * @param approvalLevel the approval level
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next compliance
	 * @throws NoSuchComplianceException if a compliance with the primary key could not be found
	 */
	public static Compliance[] findByApprovalLevel_PrevAndNext(
			long complianceId, String approvalLevel,
			OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByApprovalLevel_PrevAndNext(
			complianceId, approvalLevel, orderByComparator);
	}

	/**
	 * Removes all the compliances where approvalLevel = &#63; from the database.
	 *
	 * @param approvalLevel the approval level
	 */
	public static void removeByApprovalLevel(String approvalLevel) {
		getPersistence().removeByApprovalLevel(approvalLevel);
	}

	/**
	 * Returns the number of compliances where approvalLevel = &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @return the number of matching compliances
	 */
	public static int countByApprovalLevel(String approvalLevel) {
		return getPersistence().countByApprovalLevel(approvalLevel);
	}

	/**
	 * Returns all the compliances where finalStatus = &#63; and processedFlag = &#63;.
	 *
	 * @param finalStatus the final status
	 * @param processedFlag the processed flag
	 * @return the matching compliances
	 */
	public static List<Compliance> findByUnprocessedFinalStatus(
		boolean finalStatus, String processedFlag) {

		return getPersistence().findByUnprocessedFinalStatus(
			finalStatus, processedFlag);
	}

	/**
	 * Returns a range of all the compliances where finalStatus = &#63; and processedFlag = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param finalStatus the final status
	 * @param processedFlag the processed flag
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @return the range of matching compliances
	 */
	public static List<Compliance> findByUnprocessedFinalStatus(
		boolean finalStatus, String processedFlag, int start, int end) {

		return getPersistence().findByUnprocessedFinalStatus(
			finalStatus, processedFlag, start, end);
	}

	/**
	 * Returns an ordered range of all the compliances where finalStatus = &#63; and processedFlag = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param finalStatus the final status
	 * @param processedFlag the processed flag
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching compliances
	 */
	public static List<Compliance> findByUnprocessedFinalStatus(
		boolean finalStatus, String processedFlag, int start, int end,
		OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().findByUnprocessedFinalStatus(
			finalStatus, processedFlag, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the compliances where finalStatus = &#63; and processedFlag = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param finalStatus the final status
	 * @param processedFlag the processed flag
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching compliances
	 */
	public static List<Compliance> findByUnprocessedFinalStatus(
		boolean finalStatus, String processedFlag, int start, int end,
		OrderByComparator<Compliance> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUnprocessedFinalStatus(
			finalStatus, processedFlag, start, end, orderByComparator,
			useFinderCache);
	}

	/**
	 * Returns the first compliance in the ordered set where finalStatus = &#63; and processedFlag = &#63;.
	 *
	 * @param finalStatus the final status
	 * @param processedFlag the processed flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	public static Compliance findByUnprocessedFinalStatus_First(
			boolean finalStatus, String processedFlag,
			OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByUnprocessedFinalStatus_First(
			finalStatus, processedFlag, orderByComparator);
	}

	/**
	 * Returns the first compliance in the ordered set where finalStatus = &#63; and processedFlag = &#63;.
	 *
	 * @param finalStatus the final status
	 * @param processedFlag the processed flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	public static Compliance fetchByUnprocessedFinalStatus_First(
		boolean finalStatus, String processedFlag,
		OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().fetchByUnprocessedFinalStatus_First(
			finalStatus, processedFlag, orderByComparator);
	}

	/**
	 * Returns the last compliance in the ordered set where finalStatus = &#63; and processedFlag = &#63;.
	 *
	 * @param finalStatus the final status
	 * @param processedFlag the processed flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	public static Compliance findByUnprocessedFinalStatus_Last(
			boolean finalStatus, String processedFlag,
			OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByUnprocessedFinalStatus_Last(
			finalStatus, processedFlag, orderByComparator);
	}

	/**
	 * Returns the last compliance in the ordered set where finalStatus = &#63; and processedFlag = &#63;.
	 *
	 * @param finalStatus the final status
	 * @param processedFlag the processed flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	public static Compliance fetchByUnprocessedFinalStatus_Last(
		boolean finalStatus, String processedFlag,
		OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().fetchByUnprocessedFinalStatus_Last(
			finalStatus, processedFlag, orderByComparator);
	}

	/**
	 * Returns the compliances before and after the current compliance in the ordered set where finalStatus = &#63; and processedFlag = &#63;.
	 *
	 * @param complianceId the primary key of the current compliance
	 * @param finalStatus the final status
	 * @param processedFlag the processed flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next compliance
	 * @throws NoSuchComplianceException if a compliance with the primary key could not be found
	 */
	public static Compliance[] findByUnprocessedFinalStatus_PrevAndNext(
			long complianceId, boolean finalStatus, String processedFlag,
			OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByUnprocessedFinalStatus_PrevAndNext(
			complianceId, finalStatus, processedFlag, orderByComparator);
	}

	/**
	 * Removes all the compliances where finalStatus = &#63; and processedFlag = &#63; from the database.
	 *
	 * @param finalStatus the final status
	 * @param processedFlag the processed flag
	 */
	public static void removeByUnprocessedFinalStatus(
		boolean finalStatus, String processedFlag) {

		getPersistence().removeByUnprocessedFinalStatus(
			finalStatus, processedFlag);
	}

	/**
	 * Returns the number of compliances where finalStatus = &#63; and processedFlag = &#63;.
	 *
	 * @param finalStatus the final status
	 * @param processedFlag the processed flag
	 * @return the number of matching compliances
	 */
	public static int countByUnprocessedFinalStatus(
		boolean finalStatus, String processedFlag) {

		return getPersistence().countByUnprocessedFinalStatus(
			finalStatus, processedFlag);
	}

	/**
	 * Returns all the compliances where approvalLevel = &#63; and finalStatus = &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @param finalStatus the final status
	 * @return the matching compliances
	 */
	public static List<Compliance> findByFinalApproved(
		String approvalLevel, boolean finalStatus) {

		return getPersistence().findByFinalApproved(approvalLevel, finalStatus);
	}

	/**
	 * Returns a range of all the compliances where approvalLevel = &#63; and finalStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param approvalLevel the approval level
	 * @param finalStatus the final status
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @return the range of matching compliances
	 */
	public static List<Compliance> findByFinalApproved(
		String approvalLevel, boolean finalStatus, int start, int end) {

		return getPersistence().findByFinalApproved(
			approvalLevel, finalStatus, start, end);
	}

	/**
	 * Returns an ordered range of all the compliances where approvalLevel = &#63; and finalStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param approvalLevel the approval level
	 * @param finalStatus the final status
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching compliances
	 */
	public static List<Compliance> findByFinalApproved(
		String approvalLevel, boolean finalStatus, int start, int end,
		OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().findByFinalApproved(
			approvalLevel, finalStatus, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the compliances where approvalLevel = &#63; and finalStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param approvalLevel the approval level
	 * @param finalStatus the final status
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching compliances
	 */
	public static List<Compliance> findByFinalApproved(
		String approvalLevel, boolean finalStatus, int start, int end,
		OrderByComparator<Compliance> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByFinalApproved(
			approvalLevel, finalStatus, start, end, orderByComparator,
			useFinderCache);
	}

	/**
	 * Returns the first compliance in the ordered set where approvalLevel = &#63; and finalStatus = &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @param finalStatus the final status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	public static Compliance findByFinalApproved_First(
			String approvalLevel, boolean finalStatus,
			OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByFinalApproved_First(
			approvalLevel, finalStatus, orderByComparator);
	}

	/**
	 * Returns the first compliance in the ordered set where approvalLevel = &#63; and finalStatus = &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @param finalStatus the final status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	public static Compliance fetchByFinalApproved_First(
		String approvalLevel, boolean finalStatus,
		OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().fetchByFinalApproved_First(
			approvalLevel, finalStatus, orderByComparator);
	}

	/**
	 * Returns the last compliance in the ordered set where approvalLevel = &#63; and finalStatus = &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @param finalStatus the final status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	public static Compliance findByFinalApproved_Last(
			String approvalLevel, boolean finalStatus,
			OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByFinalApproved_Last(
			approvalLevel, finalStatus, orderByComparator);
	}

	/**
	 * Returns the last compliance in the ordered set where approvalLevel = &#63; and finalStatus = &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @param finalStatus the final status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	public static Compliance fetchByFinalApproved_Last(
		String approvalLevel, boolean finalStatus,
		OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().fetchByFinalApproved_Last(
			approvalLevel, finalStatus, orderByComparator);
	}

	/**
	 * Returns the compliances before and after the current compliance in the ordered set where approvalLevel = &#63; and finalStatus = &#63;.
	 *
	 * @param complianceId the primary key of the current compliance
	 * @param approvalLevel the approval level
	 * @param finalStatus the final status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next compliance
	 * @throws NoSuchComplianceException if a compliance with the primary key could not be found
	 */
	public static Compliance[] findByFinalApproved_PrevAndNext(
			long complianceId, String approvalLevel, boolean finalStatus,
			OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByFinalApproved_PrevAndNext(
			complianceId, approvalLevel, finalStatus, orderByComparator);
	}

	/**
	 * Removes all the compliances where approvalLevel = &#63; and finalStatus = &#63; from the database.
	 *
	 * @param approvalLevel the approval level
	 * @param finalStatus the final status
	 */
	public static void removeByFinalApproved(
		String approvalLevel, boolean finalStatus) {

		getPersistence().removeByFinalApproved(approvalLevel, finalStatus);
	}

	/**
	 * Returns the number of compliances where approvalLevel = &#63; and finalStatus = &#63;.
	 *
	 * @param approvalLevel the approval level
	 * @param finalStatus the final status
	 * @return the number of matching compliances
	 */
	public static int countByFinalApproved(
		String approvalLevel, boolean finalStatus) {

		return getPersistence().countByFinalApproved(
			approvalLevel, finalStatus);
	}

	/**
	 * Returns the compliance where complianceId = &#63; or throws a <code>NoSuchComplianceException</code> if it could not be found.
	 *
	 * @param complianceId the compliance ID
	 * @return the matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	public static Compliance findByApproverComments(long complianceId)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByApproverComments(complianceId);
	}

	/**
	 * Returns the compliance where complianceId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param complianceId the compliance ID
	 * @return the matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	public static Compliance fetchByApproverComments(long complianceId) {
		return getPersistence().fetchByApproverComments(complianceId);
	}

	/**
	 * Returns the compliance where complianceId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param complianceId the compliance ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	public static Compliance fetchByApproverComments(
		long complianceId, boolean useFinderCache) {

		return getPersistence().fetchByApproverComments(
			complianceId, useFinderCache);
	}

	/**
	 * Removes the compliance where complianceId = &#63; from the database.
	 *
	 * @param complianceId the compliance ID
	 * @return the compliance that was removed
	 */
	public static Compliance removeByApproverComments(long complianceId)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().removeByApproverComments(complianceId);
	}

	/**
	 * Returns the number of compliances where complianceId = &#63;.
	 *
	 * @param complianceId the compliance ID
	 * @return the number of matching compliances
	 */
	public static int countByApproverComments(long complianceId) {
		return getPersistence().countByApproverComments(complianceId);
	}

	/**
	 * Returns all the compliances where approverVerdict = &#63; and processedFlag = &#63;.
	 *
	 * @param approverVerdict the approver verdict
	 * @param processedFlag the processed flag
	 * @return the matching compliances
	 */
	public static List<Compliance> findByRequestApprovals(
		String approverVerdict, String processedFlag) {

		return getPersistence().findByRequestApprovals(
			approverVerdict, processedFlag);
	}

	/**
	 * Returns a range of all the compliances where approverVerdict = &#63; and processedFlag = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param approverVerdict the approver verdict
	 * @param processedFlag the processed flag
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @return the range of matching compliances
	 */
	public static List<Compliance> findByRequestApprovals(
		String approverVerdict, String processedFlag, int start, int end) {

		return getPersistence().findByRequestApprovals(
			approverVerdict, processedFlag, start, end);
	}

	/**
	 * Returns an ordered range of all the compliances where approverVerdict = &#63; and processedFlag = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param approverVerdict the approver verdict
	 * @param processedFlag the processed flag
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching compliances
	 */
	public static List<Compliance> findByRequestApprovals(
		String approverVerdict, String processedFlag, int start, int end,
		OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().findByRequestApprovals(
			approverVerdict, processedFlag, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the compliances where approverVerdict = &#63; and processedFlag = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param approverVerdict the approver verdict
	 * @param processedFlag the processed flag
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching compliances
	 */
	public static List<Compliance> findByRequestApprovals(
		String approverVerdict, String processedFlag, int start, int end,
		OrderByComparator<Compliance> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByRequestApprovals(
			approverVerdict, processedFlag, start, end, orderByComparator,
			useFinderCache);
	}

	/**
	 * Returns the first compliance in the ordered set where approverVerdict = &#63; and processedFlag = &#63;.
	 *
	 * @param approverVerdict the approver verdict
	 * @param processedFlag the processed flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	public static Compliance findByRequestApprovals_First(
			String approverVerdict, String processedFlag,
			OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByRequestApprovals_First(
			approverVerdict, processedFlag, orderByComparator);
	}

	/**
	 * Returns the first compliance in the ordered set where approverVerdict = &#63; and processedFlag = &#63;.
	 *
	 * @param approverVerdict the approver verdict
	 * @param processedFlag the processed flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	public static Compliance fetchByRequestApprovals_First(
		String approverVerdict, String processedFlag,
		OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().fetchByRequestApprovals_First(
			approverVerdict, processedFlag, orderByComparator);
	}

	/**
	 * Returns the last compliance in the ordered set where approverVerdict = &#63; and processedFlag = &#63;.
	 *
	 * @param approverVerdict the approver verdict
	 * @param processedFlag the processed flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	public static Compliance findByRequestApprovals_Last(
			String approverVerdict, String processedFlag,
			OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByRequestApprovals_Last(
			approverVerdict, processedFlag, orderByComparator);
	}

	/**
	 * Returns the last compliance in the ordered set where approverVerdict = &#63; and processedFlag = &#63;.
	 *
	 * @param approverVerdict the approver verdict
	 * @param processedFlag the processed flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	public static Compliance fetchByRequestApprovals_Last(
		String approverVerdict, String processedFlag,
		OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().fetchByRequestApprovals_Last(
			approverVerdict, processedFlag, orderByComparator);
	}

	/**
	 * Returns the compliances before and after the current compliance in the ordered set where approverVerdict = &#63; and processedFlag = &#63;.
	 *
	 * @param complianceId the primary key of the current compliance
	 * @param approverVerdict the approver verdict
	 * @param processedFlag the processed flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next compliance
	 * @throws NoSuchComplianceException if a compliance with the primary key could not be found
	 */
	public static Compliance[] findByRequestApprovals_PrevAndNext(
			long complianceId, String approverVerdict, String processedFlag,
			OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByRequestApprovals_PrevAndNext(
			complianceId, approverVerdict, processedFlag, orderByComparator);
	}

	/**
	 * Removes all the compliances where approverVerdict = &#63; and processedFlag = &#63; from the database.
	 *
	 * @param approverVerdict the approver verdict
	 * @param processedFlag the processed flag
	 */
	public static void removeByRequestApprovals(
		String approverVerdict, String processedFlag) {

		getPersistence().removeByRequestApprovals(
			approverVerdict, processedFlag);
	}

	/**
	 * Returns the number of compliances where approverVerdict = &#63; and processedFlag = &#63;.
	 *
	 * @param approverVerdict the approver verdict
	 * @param processedFlag the processed flag
	 * @return the number of matching compliances
	 */
	public static int countByRequestApprovals(
		String approverVerdict, String processedFlag) {

		return getPersistence().countByRequestApprovals(
			approverVerdict, processedFlag);
	}

	/**
	 * Returns all the compliances where status = &#63;.
	 *
	 * @param status the status
	 * @return the matching compliances
	 */
	public static List<Compliance> findByStatus(int status) {
		return getPersistence().findByStatus(status);
	}

	/**
	 * Returns a range of all the compliances where status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param status the status
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @return the range of matching compliances
	 */
	public static List<Compliance> findByStatus(
		int status, int start, int end) {

		return getPersistence().findByStatus(status, start, end);
	}

	/**
	 * Returns an ordered range of all the compliances where status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param status the status
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching compliances
	 */
	public static List<Compliance> findByStatus(
		int status, int start, int end,
		OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().findByStatus(
			status, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the compliances where status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param status the status
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching compliances
	 */
	public static List<Compliance> findByStatus(
		int status, int start, int end,
		OrderByComparator<Compliance> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByStatus(
			status, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first compliance in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	public static Compliance findByStatus_First(
			int status, OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByStatus_First(status, orderByComparator);
	}

	/**
	 * Returns the first compliance in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	public static Compliance fetchByStatus_First(
		int status, OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().fetchByStatus_First(status, orderByComparator);
	}

	/**
	 * Returns the last compliance in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	public static Compliance findByStatus_Last(
			int status, OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByStatus_Last(status, orderByComparator);
	}

	/**
	 * Returns the last compliance in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	public static Compliance fetchByStatus_Last(
		int status, OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().fetchByStatus_Last(status, orderByComparator);
	}

	/**
	 * Returns the compliances before and after the current compliance in the ordered set where status = &#63;.
	 *
	 * @param complianceId the primary key of the current compliance
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next compliance
	 * @throws NoSuchComplianceException if a compliance with the primary key could not be found
	 */
	public static Compliance[] findByStatus_PrevAndNext(
			long complianceId, int status,
			OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByStatus_PrevAndNext(
			complianceId, status, orderByComparator);
	}

	/**
	 * Removes all the compliances where status = &#63; from the database.
	 *
	 * @param status the status
	 */
	public static void removeByStatus(int status) {
		getPersistence().removeByStatus(status);
	}

	/**
	 * Returns the number of compliances where status = &#63;.
	 *
	 * @param status the status
	 * @return the number of matching compliances
	 */
	public static int countByStatus(int status) {
		return getPersistence().countByStatus(status);
	}

	/**
	 * Returns all the compliances where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @return the matching compliances
	 */
	public static List<Compliance> findByG_S(long groupId, int status) {
		return getPersistence().findByG_S(groupId, status);
	}

	/**
	 * Returns a range of all the compliances where groupId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @return the range of matching compliances
	 */
	public static List<Compliance> findByG_S(
		long groupId, int status, int start, int end) {

		return getPersistence().findByG_S(groupId, status, start, end);
	}

	/**
	 * Returns an ordered range of all the compliances where groupId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching compliances
	 */
	public static List<Compliance> findByG_S(
		long groupId, int status, int start, int end,
		OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().findByG_S(
			groupId, status, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the compliances where groupId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching compliances
	 */
	public static List<Compliance> findByG_S(
		long groupId, int status, int start, int end,
		OrderByComparator<Compliance> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByG_S(
			groupId, status, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first compliance in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	public static Compliance findByG_S_First(
			long groupId, int status,
			OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByG_S_First(
			groupId, status, orderByComparator);
	}

	/**
	 * Returns the first compliance in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	public static Compliance fetchByG_S_First(
		long groupId, int status,
		OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().fetchByG_S_First(
			groupId, status, orderByComparator);
	}

	/**
	 * Returns the last compliance in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance
	 * @throws NoSuchComplianceException if a matching compliance could not be found
	 */
	public static Compliance findByG_S_Last(
			long groupId, int status,
			OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByG_S_Last(
			groupId, status, orderByComparator);
	}

	/**
	 * Returns the last compliance in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching compliance, or <code>null</code> if a matching compliance could not be found
	 */
	public static Compliance fetchByG_S_Last(
		long groupId, int status,
		OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().fetchByG_S_Last(
			groupId, status, orderByComparator);
	}

	/**
	 * Returns the compliances before and after the current compliance in the ordered set where groupId = &#63; and status = &#63;.
	 *
	 * @param complianceId the primary key of the current compliance
	 * @param groupId the group ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next compliance
	 * @throws NoSuchComplianceException if a compliance with the primary key could not be found
	 */
	public static Compliance[] findByG_S_PrevAndNext(
			long complianceId, long groupId, int status,
			OrderByComparator<Compliance> orderByComparator)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByG_S_PrevAndNext(
			complianceId, groupId, status, orderByComparator);
	}

	/**
	 * Removes all the compliances where groupId = &#63; and status = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 */
	public static void removeByG_S(long groupId, int status) {
		getPersistence().removeByG_S(groupId, status);
	}

	/**
	 * Returns the number of compliances where groupId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param status the status
	 * @return the number of matching compliances
	 */
	public static int countByG_S(long groupId, int status) {
		return getPersistence().countByG_S(groupId, status);
	}

	/**
	 * Caches the compliance in the entity cache if it is enabled.
	 *
	 * @param compliance the compliance
	 */
	public static void cacheResult(Compliance compliance) {
		getPersistence().cacheResult(compliance);
	}

	/**
	 * Caches the compliances in the entity cache if it is enabled.
	 *
	 * @param compliances the compliances
	 */
	public static void cacheResult(List<Compliance> compliances) {
		getPersistence().cacheResult(compliances);
	}

	/**
	 * Creates a new compliance with the primary key. Does not add the compliance to the database.
	 *
	 * @param complianceId the primary key for the new compliance
	 * @return the new compliance
	 */
	public static Compliance create(long complianceId) {
		return getPersistence().create(complianceId);
	}

	/**
	 * Removes the compliance with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param complianceId the primary key of the compliance
	 * @return the compliance that was removed
	 * @throws NoSuchComplianceException if a compliance with the primary key could not be found
	 */
	public static Compliance remove(long complianceId)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().remove(complianceId);
	}

	public static Compliance updateImpl(Compliance compliance) {
		return getPersistence().updateImpl(compliance);
	}

	/**
	 * Returns the compliance with the primary key or throws a <code>NoSuchComplianceException</code> if it could not be found.
	 *
	 * @param complianceId the primary key of the compliance
	 * @return the compliance
	 * @throws NoSuchComplianceException if a compliance with the primary key could not be found
	 */
	public static Compliance findByPrimaryKey(long complianceId)
		throws com.compliance.service.exception.NoSuchComplianceException {

		return getPersistence().findByPrimaryKey(complianceId);
	}

	/**
	 * Returns the compliance with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param complianceId the primary key of the compliance
	 * @return the compliance, or <code>null</code> if a compliance with the primary key could not be found
	 */
	public static Compliance fetchByPrimaryKey(long complianceId) {
		return getPersistence().fetchByPrimaryKey(complianceId);
	}

	/**
	 * Returns all the compliances.
	 *
	 * @return the compliances
	 */
	public static List<Compliance> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the compliances.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @return the range of compliances
	 */
	public static List<Compliance> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the compliances.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of compliances
	 */
	public static List<Compliance> findAll(
		int start, int end, OrderByComparator<Compliance> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the compliances.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ComplianceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of compliances
	 * @param end the upper bound of the range of compliances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of compliances
	 */
	public static List<Compliance> findAll(
		int start, int end, OrderByComparator<Compliance> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the compliances from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of compliances.
	 *
	 * @return the number of compliances
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static Set<String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static CompliancePersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<CompliancePersistence, CompliancePersistence>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(CompliancePersistence.class);

		ServiceTracker<CompliancePersistence, CompliancePersistence>
			serviceTracker =
				new ServiceTracker
					<CompliancePersistence, CompliancePersistence>(
						bundle.getBundleContext(), CompliancePersistence.class,
						null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}