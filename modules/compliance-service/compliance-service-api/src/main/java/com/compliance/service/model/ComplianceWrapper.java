/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.compliance.service.model;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Compliance}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Compliance
 * @generated
 */
public class ComplianceWrapper implements Compliance, ModelWrapper<Compliance> {

	public ComplianceWrapper(Compliance compliance) {
		_compliance = compliance;
	}

	@Override
	public Class<?> getModelClass() {
		return Compliance.class;
	}

	@Override
	public String getModelClassName() {
		return Compliance.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("complianceId", getComplianceId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("createdby", getCreatedby());
		attributes.put("modifiedby", getModifiedby());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("contractId", getContractId());
		attributes.put("entityId", getEntityId());
		attributes.put("approvalDate", getApprovalDate());
		attributes.put("comments", getComments());
		attributes.put("approverComments", getApproverComments());
		attributes.put("firstName", getFirstName());
		attributes.put("middleName", getMiddleName());
		attributes.put("lastName", getLastName());
		attributes.put("contractNumber", getContractNumber());
		attributes.put("approverContractNumber", getApproverContractNumber());
		attributes.put("approverVerdict", getApproverVerdict());
		attributes.put("approverAction", getApproverAction());
		attributes.put("errorReason", getErrorReason());
		attributes.put("assignmentStatus", getAssignmentStatus());
		attributes.put("capacity", getCapacity());
		attributes.put("newPositionId1", getNewPositionId1());
		attributes.put("newPositionName1", getNewPositionName1());
		attributes.put("newPositionId2", getNewPositionId2());
		attributes.put("newPositionName2", getNewPositionName2());
		attributes.put("newOrgId1", getNewOrgId1());
		attributes.put("newOrgName1", getNewOrgName1());
		attributes.put("newOrgId2", getNewOrgId2());
		attributes.put("newOrgName2", getNewOrgName2());
		attributes.put("historyStartDate", getHistoryStartDate());
		attributes.put("historyEndDate", getHistoryEndDate());
		attributes.put("oldOrgId", getOldOrgId());
		attributes.put("oldPositionId", getOldPositionId());
		attributes.put("contractStartDate", getContractStartDate());
		attributes.put("contractEndDate", getContractEndDate());
		attributes.put("contractStatus", getContractStatus());
		attributes.put("approverCategory", getApproverCategory());
		attributes.put("approvalLevel", getApprovalLevel());
		attributes.put("akiReasonCode", getAkiReasonCode());
		attributes.put("faLetter", isFaLetter());
		attributes.put("fbmLetter", isFbmLetter());
		attributes.put("cbmLetter", isCbmLetter());
		attributes.put("iraLicence", isIraLicence());
		attributes.put("finalStatus", isFinalStatus());
		attributes.put("processedFlag", getProcessedFlag());
		attributes.put("oldOrgName", getOldOrgName());
		attributes.put("oldPositionName", getOldPositionName());
		attributes.put("parentOrgName", getParentOrgName());
		attributes.put("grandParentOrgName", getGrandParentOrgName());
		attributes.put("status", getStatus());
		attributes.put("statusByUserId", getStatusByUserId());
		attributes.put("statusByUserName", getStatusByUserName());
		attributes.put("statusDate", getStatusDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long complianceId = (Long)attributes.get("complianceId");

		if (complianceId != null) {
			setComplianceId(complianceId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long createdby = (Long)attributes.get("createdby");

		if (createdby != null) {
			setCreatedby(createdby);
		}

		String modifiedby = (String)attributes.get("modifiedby");

		if (modifiedby != null) {
			setModifiedby(modifiedby);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long contractId = (Long)attributes.get("contractId");

		if (contractId != null) {
			setContractId(contractId);
		}

		Long entityId = (Long)attributes.get("entityId");

		if (entityId != null) {
			setEntityId(entityId);
		}

		Date approvalDate = (Date)attributes.get("approvalDate");

		if (approvalDate != null) {
			setApprovalDate(approvalDate);
		}

		String comments = (String)attributes.get("comments");

		if (comments != null) {
			setComments(comments);
		}

		String approverComments = (String)attributes.get("approverComments");

		if (approverComments != null) {
			setApproverComments(approverComments);
		}

		String firstName = (String)attributes.get("firstName");

		if (firstName != null) {
			setFirstName(firstName);
		}

		String middleName = (String)attributes.get("middleName");

		if (middleName != null) {
			setMiddleName(middleName);
		}

		String lastName = (String)attributes.get("lastName");

		if (lastName != null) {
			setLastName(lastName);
		}

		String contractNumber = (String)attributes.get("contractNumber");

		if (contractNumber != null) {
			setContractNumber(contractNumber);
		}

		String approverContractNumber = (String)attributes.get(
			"approverContractNumber");

		if (approverContractNumber != null) {
			setApproverContractNumber(approverContractNumber);
		}

		String approverVerdict = (String)attributes.get("approverVerdict");

		if (approverVerdict != null) {
			setApproverVerdict(approverVerdict);
		}

		String approverAction = (String)attributes.get("approverAction");

		if (approverAction != null) {
			setApproverAction(approverAction);
		}

		String errorReason = (String)attributes.get("errorReason");

		if (errorReason != null) {
			setErrorReason(errorReason);
		}

		String assignmentStatus = (String)attributes.get("assignmentStatus");

		if (assignmentStatus != null) {
			setAssignmentStatus(assignmentStatus);
		}

		String capacity = (String)attributes.get("capacity");

		if (capacity != null) {
			setCapacity(capacity);
		}

		String newPositionId1 = (String)attributes.get("newPositionId1");

		if (newPositionId1 != null) {
			setNewPositionId1(newPositionId1);
		}

		String newPositionName1 = (String)attributes.get("newPositionName1");

		if (newPositionName1 != null) {
			setNewPositionName1(newPositionName1);
		}

		String newPositionId2 = (String)attributes.get("newPositionId2");

		if (newPositionId2 != null) {
			setNewPositionId2(newPositionId2);
		}

		String newPositionName2 = (String)attributes.get("newPositionName2");

		if (newPositionName2 != null) {
			setNewPositionName2(newPositionName2);
		}

		String newOrgId1 = (String)attributes.get("newOrgId1");

		if (newOrgId1 != null) {
			setNewOrgId1(newOrgId1);
		}

		String newOrgName1 = (String)attributes.get("newOrgName1");

		if (newOrgName1 != null) {
			setNewOrgName1(newOrgName1);
		}

		String newOrgId2 = (String)attributes.get("newOrgId2");

		if (newOrgId2 != null) {
			setNewOrgId2(newOrgId2);
		}

		String newOrgName2 = (String)attributes.get("newOrgName2");

		if (newOrgName2 != null) {
			setNewOrgName2(newOrgName2);
		}

		String historyStartDate = (String)attributes.get("historyStartDate");

		if (historyStartDate != null) {
			setHistoryStartDate(historyStartDate);
		}

		String historyEndDate = (String)attributes.get("historyEndDate");

		if (historyEndDate != null) {
			setHistoryEndDate(historyEndDate);
		}

		String oldOrgId = (String)attributes.get("oldOrgId");

		if (oldOrgId != null) {
			setOldOrgId(oldOrgId);
		}

		String oldPositionId = (String)attributes.get("oldPositionId");

		if (oldPositionId != null) {
			setOldPositionId(oldPositionId);
		}

		String contractStartDate = (String)attributes.get("contractStartDate");

		if (contractStartDate != null) {
			setContractStartDate(contractStartDate);
		}

		String contractEndDate = (String)attributes.get("contractEndDate");

		if (contractEndDate != null) {
			setContractEndDate(contractEndDate);
		}

		String contractStatus = (String)attributes.get("contractStatus");

		if (contractStatus != null) {
			setContractStatus(contractStatus);
		}

		String approverCategory = (String)attributes.get("approverCategory");

		if (approverCategory != null) {
			setApproverCategory(approverCategory);
		}

		String approvalLevel = (String)attributes.get("approvalLevel");

		if (approvalLevel != null) {
			setApprovalLevel(approvalLevel);
		}

		String akiReasonCode = (String)attributes.get("akiReasonCode");

		if (akiReasonCode != null) {
			setAkiReasonCode(akiReasonCode);
		}

		Boolean faLetter = (Boolean)attributes.get("faLetter");

		if (faLetter != null) {
			setFaLetter(faLetter);
		}

		Boolean fbmLetter = (Boolean)attributes.get("fbmLetter");

		if (fbmLetter != null) {
			setFbmLetter(fbmLetter);
		}

		Boolean cbmLetter = (Boolean)attributes.get("cbmLetter");

		if (cbmLetter != null) {
			setCbmLetter(cbmLetter);
		}

		Boolean iraLicence = (Boolean)attributes.get("iraLicence");

		if (iraLicence != null) {
			setIraLicence(iraLicence);
		}

		Boolean finalStatus = (Boolean)attributes.get("finalStatus");

		if (finalStatus != null) {
			setFinalStatus(finalStatus);
		}

		String processedFlag = (String)attributes.get("processedFlag");

		if (processedFlag != null) {
			setProcessedFlag(processedFlag);
		}

		String oldOrgName = (String)attributes.get("oldOrgName");

		if (oldOrgName != null) {
			setOldOrgName(oldOrgName);
		}

		String oldPositionName = (String)attributes.get("oldPositionName");

		if (oldPositionName != null) {
			setOldPositionName(oldPositionName);
		}

		String parentOrgName = (String)attributes.get("parentOrgName");

		if (parentOrgName != null) {
			setParentOrgName(parentOrgName);
		}

		String grandParentOrgName = (String)attributes.get(
			"grandParentOrgName");

		if (grandParentOrgName != null) {
			setGrandParentOrgName(grandParentOrgName);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Long statusByUserId = (Long)attributes.get("statusByUserId");

		if (statusByUserId != null) {
			setStatusByUserId(statusByUserId);
		}

		String statusByUserName = (String)attributes.get("statusByUserName");

		if (statusByUserName != null) {
			setStatusByUserName(statusByUserName);
		}

		Date statusDate = (Date)attributes.get("statusDate");

		if (statusDate != null) {
			setStatusDate(statusDate);
		}
	}

	@Override
	public Object clone() {
		return new ComplianceWrapper((Compliance)_compliance.clone());
	}

	@Override
	public int compareTo(Compliance compliance) {
		return _compliance.compareTo(compliance);
	}

	/**
	 * Returns the aki reason code of this compliance.
	 *
	 * @return the aki reason code of this compliance
	 */
	@Override
	public String getAkiReasonCode() {
		return _compliance.getAkiReasonCode();
	}

	/**
	 * Returns the approval date of this compliance.
	 *
	 * @return the approval date of this compliance
	 */
	@Override
	public Date getApprovalDate() {
		return _compliance.getApprovalDate();
	}

	/**
	 * Returns the approval level of this compliance.
	 *
	 * @return the approval level of this compliance
	 */
	@Override
	public String getApprovalLevel() {
		return _compliance.getApprovalLevel();
	}

	/**
	 * Returns the approver action of this compliance.
	 *
	 * @return the approver action of this compliance
	 */
	@Override
	public String getApproverAction() {
		return _compliance.getApproverAction();
	}

	/**
	 * Returns the approver category of this compliance.
	 *
	 * @return the approver category of this compliance
	 */
	@Override
	public String getApproverCategory() {
		return _compliance.getApproverCategory();
	}

	/**
	 * Returns the approver comments of this compliance.
	 *
	 * @return the approver comments of this compliance
	 */
	@Override
	public String getApproverComments() {
		return _compliance.getApproverComments();
	}

	/**
	 * Returns the approver contract number of this compliance.
	 *
	 * @return the approver contract number of this compliance
	 */
	@Override
	public String getApproverContractNumber() {
		return _compliance.getApproverContractNumber();
	}

	/**
	 * Returns the approver verdict of this compliance.
	 *
	 * @return the approver verdict of this compliance
	 */
	@Override
	public String getApproverVerdict() {
		return _compliance.getApproverVerdict();
	}

	/**
	 * Returns the assignment status of this compliance.
	 *
	 * @return the assignment status of this compliance
	 */
	@Override
	public String getAssignmentStatus() {
		return _compliance.getAssignmentStatus();
	}

	/**
	 * Returns the capacity of this compliance.
	 *
	 * @return the capacity of this compliance
	 */
	@Override
	public String getCapacity() {
		return _compliance.getCapacity();
	}

	/**
	 * Returns the cbm letter of this compliance.
	 *
	 * @return the cbm letter of this compliance
	 */
	@Override
	public boolean getCbmLetter() {
		return _compliance.getCbmLetter();
	}

	/**
	 * Returns the comments of this compliance.
	 *
	 * @return the comments of this compliance
	 */
	@Override
	public String getComments() {
		return _compliance.getComments();
	}

	/**
	 * Returns the company ID of this compliance.
	 *
	 * @return the company ID of this compliance
	 */
	@Override
	public long getCompanyId() {
		return _compliance.getCompanyId();
	}

	/**
	 * Returns the compliance ID of this compliance.
	 *
	 * @return the compliance ID of this compliance
	 */
	@Override
	public long getComplianceId() {
		return _compliance.getComplianceId();
	}

	/**
	 * Returns the contract end date of this compliance.
	 *
	 * @return the contract end date of this compliance
	 */
	@Override
	public String getContractEndDate() {
		return _compliance.getContractEndDate();
	}

	/**
	 * Returns the contract ID of this compliance.
	 *
	 * @return the contract ID of this compliance
	 */
	@Override
	public long getContractId() {
		return _compliance.getContractId();
	}

	/**
	 * Returns the contract number of this compliance.
	 *
	 * @return the contract number of this compliance
	 */
	@Override
	public String getContractNumber() {
		return _compliance.getContractNumber();
	}

	/**
	 * Returns the contract start date of this compliance.
	 *
	 * @return the contract start date of this compliance
	 */
	@Override
	public String getContractStartDate() {
		return _compliance.getContractStartDate();
	}

	/**
	 * Returns the contract status of this compliance.
	 *
	 * @return the contract status of this compliance
	 */
	@Override
	public String getContractStatus() {
		return _compliance.getContractStatus();
	}

	/**
	 * Returns the create date of this compliance.
	 *
	 * @return the create date of this compliance
	 */
	@Override
	public Date getCreateDate() {
		return _compliance.getCreateDate();
	}

	/**
	 * Returns the createdby of this compliance.
	 *
	 * @return the createdby of this compliance
	 */
	@Override
	public long getCreatedby() {
		return _compliance.getCreatedby();
	}

	/**
	 * Returns the entity ID of this compliance.
	 *
	 * @return the entity ID of this compliance
	 */
	@Override
	public long getEntityId() {
		return _compliance.getEntityId();
	}

	/**
	 * Returns the error reason of this compliance.
	 *
	 * @return the error reason of this compliance
	 */
	@Override
	public String getErrorReason() {
		return _compliance.getErrorReason();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _compliance.getExpandoBridge();
	}

	/**
	 * Returns the fa letter of this compliance.
	 *
	 * @return the fa letter of this compliance
	 */
	@Override
	public boolean getFaLetter() {
		return _compliance.getFaLetter();
	}

	/**
	 * Returns the fbm letter of this compliance.
	 *
	 * @return the fbm letter of this compliance
	 */
	@Override
	public boolean getFbmLetter() {
		return _compliance.getFbmLetter();
	}

	/**
	 * Returns the final status of this compliance.
	 *
	 * @return the final status of this compliance
	 */
	@Override
	public boolean getFinalStatus() {
		return _compliance.getFinalStatus();
	}

	/**
	 * Returns the first name of this compliance.
	 *
	 * @return the first name of this compliance
	 */
	@Override
	public String getFirstName() {
		return _compliance.getFirstName();
	}

	/**
	 * Returns the grand parent org name of this compliance.
	 *
	 * @return the grand parent org name of this compliance
	 */
	@Override
	public String getGrandParentOrgName() {
		return _compliance.getGrandParentOrgName();
	}

	/**
	 * Returns the group ID of this compliance.
	 *
	 * @return the group ID of this compliance
	 */
	@Override
	public long getGroupId() {
		return _compliance.getGroupId();
	}

	/**
	 * Returns the history end date of this compliance.
	 *
	 * @return the history end date of this compliance
	 */
	@Override
	public String getHistoryEndDate() {
		return _compliance.getHistoryEndDate();
	}

	/**
	 * Returns the history start date of this compliance.
	 *
	 * @return the history start date of this compliance
	 */
	@Override
	public String getHistoryStartDate() {
		return _compliance.getHistoryStartDate();
	}

	/**
	 * Returns the ira licence of this compliance.
	 *
	 * @return the ira licence of this compliance
	 */
	@Override
	public boolean getIraLicence() {
		return _compliance.getIraLicence();
	}

	/**
	 * Returns the last name of this compliance.
	 *
	 * @return the last name of this compliance
	 */
	@Override
	public String getLastName() {
		return _compliance.getLastName();
	}

	/**
	 * Returns the middle name of this compliance.
	 *
	 * @return the middle name of this compliance
	 */
	@Override
	public String getMiddleName() {
		return _compliance.getMiddleName();
	}

	/**
	 * Returns the modifiedby of this compliance.
	 *
	 * @return the modifiedby of this compliance
	 */
	@Override
	public String getModifiedby() {
		return _compliance.getModifiedby();
	}

	/**
	 * Returns the modified date of this compliance.
	 *
	 * @return the modified date of this compliance
	 */
	@Override
	public Date getModifiedDate() {
		return _compliance.getModifiedDate();
	}

	/**
	 * Returns the new org id1 of this compliance.
	 *
	 * @return the new org id1 of this compliance
	 */
	@Override
	public String getNewOrgId1() {
		return _compliance.getNewOrgId1();
	}

	/**
	 * Returns the new org id2 of this compliance.
	 *
	 * @return the new org id2 of this compliance
	 */
	@Override
	public String getNewOrgId2() {
		return _compliance.getNewOrgId2();
	}

	/**
	 * Returns the new org name1 of this compliance.
	 *
	 * @return the new org name1 of this compliance
	 */
	@Override
	public String getNewOrgName1() {
		return _compliance.getNewOrgName1();
	}

	/**
	 * Returns the new org name2 of this compliance.
	 *
	 * @return the new org name2 of this compliance
	 */
	@Override
	public String getNewOrgName2() {
		return _compliance.getNewOrgName2();
	}

	/**
	 * Returns the new position id1 of this compliance.
	 *
	 * @return the new position id1 of this compliance
	 */
	@Override
	public String getNewPositionId1() {
		return _compliance.getNewPositionId1();
	}

	/**
	 * Returns the new position id2 of this compliance.
	 *
	 * @return the new position id2 of this compliance
	 */
	@Override
	public String getNewPositionId2() {
		return _compliance.getNewPositionId2();
	}

	/**
	 * Returns the new position name1 of this compliance.
	 *
	 * @return the new position name1 of this compliance
	 */
	@Override
	public String getNewPositionName1() {
		return _compliance.getNewPositionName1();
	}

	/**
	 * Returns the new position name2 of this compliance.
	 *
	 * @return the new position name2 of this compliance
	 */
	@Override
	public String getNewPositionName2() {
		return _compliance.getNewPositionName2();
	}

	/**
	 * Returns the old org ID of this compliance.
	 *
	 * @return the old org ID of this compliance
	 */
	@Override
	public String getOldOrgId() {
		return _compliance.getOldOrgId();
	}

	/**
	 * Returns the old org name of this compliance.
	 *
	 * @return the old org name of this compliance
	 */
	@Override
	public String getOldOrgName() {
		return _compliance.getOldOrgName();
	}

	/**
	 * Returns the old position ID of this compliance.
	 *
	 * @return the old position ID of this compliance
	 */
	@Override
	public String getOldPositionId() {
		return _compliance.getOldPositionId();
	}

	/**
	 * Returns the old position name of this compliance.
	 *
	 * @return the old position name of this compliance
	 */
	@Override
	public String getOldPositionName() {
		return _compliance.getOldPositionName();
	}

	/**
	 * Returns the parent org name of this compliance.
	 *
	 * @return the parent org name of this compliance
	 */
	@Override
	public String getParentOrgName() {
		return _compliance.getParentOrgName();
	}

	/**
	 * Returns the primary key of this compliance.
	 *
	 * @return the primary key of this compliance
	 */
	@Override
	public long getPrimaryKey() {
		return _compliance.getPrimaryKey();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _compliance.getPrimaryKeyObj();
	}

	/**
	 * Returns the processed flag of this compliance.
	 *
	 * @return the processed flag of this compliance
	 */
	@Override
	public String getProcessedFlag() {
		return _compliance.getProcessedFlag();
	}

	/**
	 * Returns the status of this compliance.
	 *
	 * @return the status of this compliance
	 */
	@Override
	public int getStatus() {
		return _compliance.getStatus();
	}

	/**
	 * Returns the status by user ID of this compliance.
	 *
	 * @return the status by user ID of this compliance
	 */
	@Override
	public long getStatusByUserId() {
		return _compliance.getStatusByUserId();
	}

	/**
	 * Returns the status by user name of this compliance.
	 *
	 * @return the status by user name of this compliance
	 */
	@Override
	public String getStatusByUserName() {
		return _compliance.getStatusByUserName();
	}

	/**
	 * Returns the status by user uuid of this compliance.
	 *
	 * @return the status by user uuid of this compliance
	 */
	@Override
	public String getStatusByUserUuid() {
		return _compliance.getStatusByUserUuid();
	}

	/**
	 * Returns the status date of this compliance.
	 *
	 * @return the status date of this compliance
	 */
	@Override
	public Date getStatusDate() {
		return _compliance.getStatusDate();
	}

	/**
	 * Returns the uuid of this compliance.
	 *
	 * @return the uuid of this compliance
	 */
	@Override
	public String getUuid() {
		return _compliance.getUuid();
	}

	@Override
	public int hashCode() {
		return _compliance.hashCode();
	}

	/**
	 * Returns <code>true</code> if this compliance is approved.
	 *
	 * @return <code>true</code> if this compliance is approved; <code>false</code> otherwise
	 */
	@Override
	public boolean isApproved() {
		return _compliance.isApproved();
	}

	@Override
	public boolean isCachedModel() {
		return _compliance.isCachedModel();
	}

	/**
	 * Returns <code>true</code> if this compliance is cbm letter.
	 *
	 * @return <code>true</code> if this compliance is cbm letter; <code>false</code> otherwise
	 */
	@Override
	public boolean isCbmLetter() {
		return _compliance.isCbmLetter();
	}

	/**
	 * Returns <code>true</code> if this compliance is denied.
	 *
	 * @return <code>true</code> if this compliance is denied; <code>false</code> otherwise
	 */
	@Override
	public boolean isDenied() {
		return _compliance.isDenied();
	}

	/**
	 * Returns <code>true</code> if this compliance is a draft.
	 *
	 * @return <code>true</code> if this compliance is a draft; <code>false</code> otherwise
	 */
	@Override
	public boolean isDraft() {
		return _compliance.isDraft();
	}

	@Override
	public boolean isEscapedModel() {
		return _compliance.isEscapedModel();
	}

	/**
	 * Returns <code>true</code> if this compliance is expired.
	 *
	 * @return <code>true</code> if this compliance is expired; <code>false</code> otherwise
	 */
	@Override
	public boolean isExpired() {
		return _compliance.isExpired();
	}

	/**
	 * Returns <code>true</code> if this compliance is fa letter.
	 *
	 * @return <code>true</code> if this compliance is fa letter; <code>false</code> otherwise
	 */
	@Override
	public boolean isFaLetter() {
		return _compliance.isFaLetter();
	}

	/**
	 * Returns <code>true</code> if this compliance is fbm letter.
	 *
	 * @return <code>true</code> if this compliance is fbm letter; <code>false</code> otherwise
	 */
	@Override
	public boolean isFbmLetter() {
		return _compliance.isFbmLetter();
	}

	/**
	 * Returns <code>true</code> if this compliance is final status.
	 *
	 * @return <code>true</code> if this compliance is final status; <code>false</code> otherwise
	 */
	@Override
	public boolean isFinalStatus() {
		return _compliance.isFinalStatus();
	}

	/**
	 * Returns <code>true</code> if this compliance is inactive.
	 *
	 * @return <code>true</code> if this compliance is inactive; <code>false</code> otherwise
	 */
	@Override
	public boolean isInactive() {
		return _compliance.isInactive();
	}

	/**
	 * Returns <code>true</code> if this compliance is incomplete.
	 *
	 * @return <code>true</code> if this compliance is incomplete; <code>false</code> otherwise
	 */
	@Override
	public boolean isIncomplete() {
		return _compliance.isIncomplete();
	}

	/**
	 * Returns <code>true</code> if this compliance is ira licence.
	 *
	 * @return <code>true</code> if this compliance is ira licence; <code>false</code> otherwise
	 */
	@Override
	public boolean isIraLicence() {
		return _compliance.isIraLicence();
	}

	@Override
	public boolean isNew() {
		return _compliance.isNew();
	}

	/**
	 * Returns <code>true</code> if this compliance is pending.
	 *
	 * @return <code>true</code> if this compliance is pending; <code>false</code> otherwise
	 */
	@Override
	public boolean isPending() {
		return _compliance.isPending();
	}

	/**
	 * Returns <code>true</code> if this compliance is scheduled.
	 *
	 * @return <code>true</code> if this compliance is scheduled; <code>false</code> otherwise
	 */
	@Override
	public boolean isScheduled() {
		return _compliance.isScheduled();
	}

	@Override
	public void persist() {
		_compliance.persist();
	}

	/**
	 * Sets the aki reason code of this compliance.
	 *
	 * @param akiReasonCode the aki reason code of this compliance
	 */
	@Override
	public void setAkiReasonCode(String akiReasonCode) {
		_compliance.setAkiReasonCode(akiReasonCode);
	}

	/**
	 * Sets the approval date of this compliance.
	 *
	 * @param approvalDate the approval date of this compliance
	 */
	@Override
	public void setApprovalDate(Date approvalDate) {
		_compliance.setApprovalDate(approvalDate);
	}

	/**
	 * Sets the approval level of this compliance.
	 *
	 * @param approvalLevel the approval level of this compliance
	 */
	@Override
	public void setApprovalLevel(String approvalLevel) {
		_compliance.setApprovalLevel(approvalLevel);
	}

	/**
	 * Sets the approver action of this compliance.
	 *
	 * @param approverAction the approver action of this compliance
	 */
	@Override
	public void setApproverAction(String approverAction) {
		_compliance.setApproverAction(approverAction);
	}

	/**
	 * Sets the approver category of this compliance.
	 *
	 * @param approverCategory the approver category of this compliance
	 */
	@Override
	public void setApproverCategory(String approverCategory) {
		_compliance.setApproverCategory(approverCategory);
	}

	/**
	 * Sets the approver comments of this compliance.
	 *
	 * @param approverComments the approver comments of this compliance
	 */
	@Override
	public void setApproverComments(String approverComments) {
		_compliance.setApproverComments(approverComments);
	}

	/**
	 * Sets the approver contract number of this compliance.
	 *
	 * @param approverContractNumber the approver contract number of this compliance
	 */
	@Override
	public void setApproverContractNumber(String approverContractNumber) {
		_compliance.setApproverContractNumber(approverContractNumber);
	}

	/**
	 * Sets the approver verdict of this compliance.
	 *
	 * @param approverVerdict the approver verdict of this compliance
	 */
	@Override
	public void setApproverVerdict(String approverVerdict) {
		_compliance.setApproverVerdict(approverVerdict);
	}

	/**
	 * Sets the assignment status of this compliance.
	 *
	 * @param assignmentStatus the assignment status of this compliance
	 */
	@Override
	public void setAssignmentStatus(String assignmentStatus) {
		_compliance.setAssignmentStatus(assignmentStatus);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_compliance.setCachedModel(cachedModel);
	}

	/**
	 * Sets the capacity of this compliance.
	 *
	 * @param capacity the capacity of this compliance
	 */
	@Override
	public void setCapacity(String capacity) {
		_compliance.setCapacity(capacity);
	}

	/**
	 * Sets whether this compliance is cbm letter.
	 *
	 * @param cbmLetter the cbm letter of this compliance
	 */
	@Override
	public void setCbmLetter(boolean cbmLetter) {
		_compliance.setCbmLetter(cbmLetter);
	}

	/**
	 * Sets the comments of this compliance.
	 *
	 * @param comments the comments of this compliance
	 */
	@Override
	public void setComments(String comments) {
		_compliance.setComments(comments);
	}

	/**
	 * Sets the company ID of this compliance.
	 *
	 * @param companyId the company ID of this compliance
	 */
	@Override
	public void setCompanyId(long companyId) {
		_compliance.setCompanyId(companyId);
	}

	/**
	 * Sets the compliance ID of this compliance.
	 *
	 * @param complianceId the compliance ID of this compliance
	 */
	@Override
	public void setComplianceId(long complianceId) {
		_compliance.setComplianceId(complianceId);
	}

	/**
	 * Sets the contract end date of this compliance.
	 *
	 * @param contractEndDate the contract end date of this compliance
	 */
	@Override
	public void setContractEndDate(String contractEndDate) {
		_compliance.setContractEndDate(contractEndDate);
	}

	/**
	 * Sets the contract ID of this compliance.
	 *
	 * @param contractId the contract ID of this compliance
	 */
	@Override
	public void setContractId(long contractId) {
		_compliance.setContractId(contractId);
	}

	/**
	 * Sets the contract number of this compliance.
	 *
	 * @param contractNumber the contract number of this compliance
	 */
	@Override
	public void setContractNumber(String contractNumber) {
		_compliance.setContractNumber(contractNumber);
	}

	/**
	 * Sets the contract start date of this compliance.
	 *
	 * @param contractStartDate the contract start date of this compliance
	 */
	@Override
	public void setContractStartDate(String contractStartDate) {
		_compliance.setContractStartDate(contractStartDate);
	}

	/**
	 * Sets the contract status of this compliance.
	 *
	 * @param contractStatus the contract status of this compliance
	 */
	@Override
	public void setContractStatus(String contractStatus) {
		_compliance.setContractStatus(contractStatus);
	}

	/**
	 * Sets the create date of this compliance.
	 *
	 * @param createDate the create date of this compliance
	 */
	@Override
	public void setCreateDate(Date createDate) {
		_compliance.setCreateDate(createDate);
	}

	/**
	 * Sets the createdby of this compliance.
	 *
	 * @param createdby the createdby of this compliance
	 */
	@Override
	public void setCreatedby(long createdby) {
		_compliance.setCreatedby(createdby);
	}

	/**
	 * Sets the entity ID of this compliance.
	 *
	 * @param entityId the entity ID of this compliance
	 */
	@Override
	public void setEntityId(long entityId) {
		_compliance.setEntityId(entityId);
	}

	/**
	 * Sets the error reason of this compliance.
	 *
	 * @param errorReason the error reason of this compliance
	 */
	@Override
	public void setErrorReason(String errorReason) {
		_compliance.setErrorReason(errorReason);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {

		_compliance.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_compliance.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_compliance.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	 * Sets whether this compliance is fa letter.
	 *
	 * @param faLetter the fa letter of this compliance
	 */
	@Override
	public void setFaLetter(boolean faLetter) {
		_compliance.setFaLetter(faLetter);
	}

	/**
	 * Sets whether this compliance is fbm letter.
	 *
	 * @param fbmLetter the fbm letter of this compliance
	 */
	@Override
	public void setFbmLetter(boolean fbmLetter) {
		_compliance.setFbmLetter(fbmLetter);
	}

	/**
	 * Sets whether this compliance is final status.
	 *
	 * @param finalStatus the final status of this compliance
	 */
	@Override
	public void setFinalStatus(boolean finalStatus) {
		_compliance.setFinalStatus(finalStatus);
	}

	/**
	 * Sets the first name of this compliance.
	 *
	 * @param firstName the first name of this compliance
	 */
	@Override
	public void setFirstName(String firstName) {
		_compliance.setFirstName(firstName);
	}

	/**
	 * Sets the grand parent org name of this compliance.
	 *
	 * @param grandParentOrgName the grand parent org name of this compliance
	 */
	@Override
	public void setGrandParentOrgName(String grandParentOrgName) {
		_compliance.setGrandParentOrgName(grandParentOrgName);
	}

	/**
	 * Sets the group ID of this compliance.
	 *
	 * @param groupId the group ID of this compliance
	 */
	@Override
	public void setGroupId(long groupId) {
		_compliance.setGroupId(groupId);
	}

	/**
	 * Sets the history end date of this compliance.
	 *
	 * @param historyEndDate the history end date of this compliance
	 */
	@Override
	public void setHistoryEndDate(String historyEndDate) {
		_compliance.setHistoryEndDate(historyEndDate);
	}

	/**
	 * Sets the history start date of this compliance.
	 *
	 * @param historyStartDate the history start date of this compliance
	 */
	@Override
	public void setHistoryStartDate(String historyStartDate) {
		_compliance.setHistoryStartDate(historyStartDate);
	}

	/**
	 * Sets whether this compliance is ira licence.
	 *
	 * @param iraLicence the ira licence of this compliance
	 */
	@Override
	public void setIraLicence(boolean iraLicence) {
		_compliance.setIraLicence(iraLicence);
	}

	/**
	 * Sets the last name of this compliance.
	 *
	 * @param lastName the last name of this compliance
	 */
	@Override
	public void setLastName(String lastName) {
		_compliance.setLastName(lastName);
	}

	/**
	 * Sets the middle name of this compliance.
	 *
	 * @param middleName the middle name of this compliance
	 */
	@Override
	public void setMiddleName(String middleName) {
		_compliance.setMiddleName(middleName);
	}

	/**
	 * Sets the modifiedby of this compliance.
	 *
	 * @param modifiedby the modifiedby of this compliance
	 */
	@Override
	public void setModifiedby(String modifiedby) {
		_compliance.setModifiedby(modifiedby);
	}

	/**
	 * Sets the modified date of this compliance.
	 *
	 * @param modifiedDate the modified date of this compliance
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_compliance.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_compliance.setNew(n);
	}

	/**
	 * Sets the new org id1 of this compliance.
	 *
	 * @param newOrgId1 the new org id1 of this compliance
	 */
	@Override
	public void setNewOrgId1(String newOrgId1) {
		_compliance.setNewOrgId1(newOrgId1);
	}

	/**
	 * Sets the new org id2 of this compliance.
	 *
	 * @param newOrgId2 the new org id2 of this compliance
	 */
	@Override
	public void setNewOrgId2(String newOrgId2) {
		_compliance.setNewOrgId2(newOrgId2);
	}

	/**
	 * Sets the new org name1 of this compliance.
	 *
	 * @param newOrgName1 the new org name1 of this compliance
	 */
	@Override
	public void setNewOrgName1(String newOrgName1) {
		_compliance.setNewOrgName1(newOrgName1);
	}

	/**
	 * Sets the new org name2 of this compliance.
	 *
	 * @param newOrgName2 the new org name2 of this compliance
	 */
	@Override
	public void setNewOrgName2(String newOrgName2) {
		_compliance.setNewOrgName2(newOrgName2);
	}

	/**
	 * Sets the new position id1 of this compliance.
	 *
	 * @param newPositionId1 the new position id1 of this compliance
	 */
	@Override
	public void setNewPositionId1(String newPositionId1) {
		_compliance.setNewPositionId1(newPositionId1);
	}

	/**
	 * Sets the new position id2 of this compliance.
	 *
	 * @param newPositionId2 the new position id2 of this compliance
	 */
	@Override
	public void setNewPositionId2(String newPositionId2) {
		_compliance.setNewPositionId2(newPositionId2);
	}

	/**
	 * Sets the new position name1 of this compliance.
	 *
	 * @param newPositionName1 the new position name1 of this compliance
	 */
	@Override
	public void setNewPositionName1(String newPositionName1) {
		_compliance.setNewPositionName1(newPositionName1);
	}

	/**
	 * Sets the new position name2 of this compliance.
	 *
	 * @param newPositionName2 the new position name2 of this compliance
	 */
	@Override
	public void setNewPositionName2(String newPositionName2) {
		_compliance.setNewPositionName2(newPositionName2);
	}

	/**
	 * Sets the old org ID of this compliance.
	 *
	 * @param oldOrgId the old org ID of this compliance
	 */
	@Override
	public void setOldOrgId(String oldOrgId) {
		_compliance.setOldOrgId(oldOrgId);
	}

	/**
	 * Sets the old org name of this compliance.
	 *
	 * @param oldOrgName the old org name of this compliance
	 */
	@Override
	public void setOldOrgName(String oldOrgName) {
		_compliance.setOldOrgName(oldOrgName);
	}

	/**
	 * Sets the old position ID of this compliance.
	 *
	 * @param oldPositionId the old position ID of this compliance
	 */
	@Override
	public void setOldPositionId(String oldPositionId) {
		_compliance.setOldPositionId(oldPositionId);
	}

	/**
	 * Sets the old position name of this compliance.
	 *
	 * @param oldPositionName the old position name of this compliance
	 */
	@Override
	public void setOldPositionName(String oldPositionName) {
		_compliance.setOldPositionName(oldPositionName);
	}

	/**
	 * Sets the parent org name of this compliance.
	 *
	 * @param parentOrgName the parent org name of this compliance
	 */
	@Override
	public void setParentOrgName(String parentOrgName) {
		_compliance.setParentOrgName(parentOrgName);
	}

	/**
	 * Sets the primary key of this compliance.
	 *
	 * @param primaryKey the primary key of this compliance
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		_compliance.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_compliance.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	 * Sets the processed flag of this compliance.
	 *
	 * @param processedFlag the processed flag of this compliance
	 */
	@Override
	public void setProcessedFlag(String processedFlag) {
		_compliance.setProcessedFlag(processedFlag);
	}

	/**
	 * Sets the status of this compliance.
	 *
	 * @param status the status of this compliance
	 */
	@Override
	public void setStatus(int status) {
		_compliance.setStatus(status);
	}

	/**
	 * Sets the status by user ID of this compliance.
	 *
	 * @param statusByUserId the status by user ID of this compliance
	 */
	@Override
	public void setStatusByUserId(long statusByUserId) {
		_compliance.setStatusByUserId(statusByUserId);
	}

	/**
	 * Sets the status by user name of this compliance.
	 *
	 * @param statusByUserName the status by user name of this compliance
	 */
	@Override
	public void setStatusByUserName(String statusByUserName) {
		_compliance.setStatusByUserName(statusByUserName);
	}

	/**
	 * Sets the status by user uuid of this compliance.
	 *
	 * @param statusByUserUuid the status by user uuid of this compliance
	 */
	@Override
	public void setStatusByUserUuid(String statusByUserUuid) {
		_compliance.setStatusByUserUuid(statusByUserUuid);
	}

	/**
	 * Sets the status date of this compliance.
	 *
	 * @param statusDate the status date of this compliance
	 */
	@Override
	public void setStatusDate(Date statusDate) {
		_compliance.setStatusDate(statusDate);
	}

	/**
	 * Sets the uuid of this compliance.
	 *
	 * @param uuid the uuid of this compliance
	 */
	@Override
	public void setUuid(String uuid) {
		_compliance.setUuid(uuid);
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Compliance>
		toCacheModel() {

		return _compliance.toCacheModel();
	}

	@Override
	public Compliance toEscapedModel() {
		return new ComplianceWrapper(_compliance.toEscapedModel());
	}

	@Override
	public String toString() {
		return _compliance.toString();
	}

	@Override
	public Compliance toUnescapedModel() {
		return new ComplianceWrapper(_compliance.toUnescapedModel());
	}

	@Override
	public String toXmlString() {
		return _compliance.toXmlString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ComplianceWrapper)) {
			return false;
		}

		ComplianceWrapper complianceWrapper = (ComplianceWrapper)obj;

		if (Objects.equals(_compliance, complianceWrapper._compliance)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _compliance.getStagedModelType();
	}

	@Override
	public Compliance getWrappedModel() {
		return _compliance;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _compliance.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _compliance.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_compliance.resetOriginalValues();
	}

	private final Compliance _compliance;

}