/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.compliance.service.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.compliance.service.service.http.UserCommentServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class UserCommentSoap implements Serializable {

	public static UserCommentSoap toSoapModel(UserComment model) {
		UserCommentSoap soapModel = new UserCommentSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setCommentId(model.getCommentId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setCreatedby(model.getCreatedby());
		soapModel.setModifiedby(model.getModifiedby());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setCommentDate(model.getCommentDate());
		soapModel.setApproverName(model.getApproverName());
		soapModel.setApproverContractNumber(model.getApproverContractNumber());
		soapModel.setComment(model.getComment());
		soapModel.setComplianceId(model.getComplianceId());
		soapModel.setStatus(model.getStatus());
		soapModel.setStatusByUserId(model.getStatusByUserId());
		soapModel.setStatusByUserName(model.getStatusByUserName());
		soapModel.setStatusDate(model.getStatusDate());

		return soapModel;
	}

	public static UserCommentSoap[] toSoapModels(UserComment[] models) {
		UserCommentSoap[] soapModels = new UserCommentSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static UserCommentSoap[][] toSoapModels(UserComment[][] models) {
		UserCommentSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new UserCommentSoap[models.length][models[0].length];
		}
		else {
			soapModels = new UserCommentSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static UserCommentSoap[] toSoapModels(List<UserComment> models) {
		List<UserCommentSoap> soapModels = new ArrayList<UserCommentSoap>(
			models.size());

		for (UserComment model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new UserCommentSoap[soapModels.size()]);
	}

	public UserCommentSoap() {
	}

	public long getPrimaryKey() {
		return _commentId;
	}

	public void setPrimaryKey(long pk) {
		setCommentId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getCommentId() {
		return _commentId;
	}

	public void setCommentId(long commentId) {
		_commentId = commentId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getCreatedby() {
		return _createdby;
	}

	public void setCreatedby(long createdby) {
		_createdby = createdby;
	}

	public String getModifiedby() {
		return _modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		_modifiedby = modifiedby;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public Date getCommentDate() {
		return _commentDate;
	}

	public void setCommentDate(Date commentDate) {
		_commentDate = commentDate;
	}

	public String getApproverName() {
		return _approverName;
	}

	public void setApproverName(String approverName) {
		_approverName = approverName;
	}

	public String getApproverContractNumber() {
		return _approverContractNumber;
	}

	public void setApproverContractNumber(String approverContractNumber) {
		_approverContractNumber = approverContractNumber;
	}

	public String getComment() {
		return _comment;
	}

	public void setComment(String comment) {
		_comment = comment;
	}

	public long getComplianceId() {
		return _complianceId;
	}

	public void setComplianceId(long complianceId) {
		_complianceId = complianceId;
	}

	public int getStatus() {
		return _status;
	}

	public void setStatus(int status) {
		_status = status;
	}

	public long getStatusByUserId() {
		return _statusByUserId;
	}

	public void setStatusByUserId(long statusByUserId) {
		_statusByUserId = statusByUserId;
	}

	public String getStatusByUserName() {
		return _statusByUserName;
	}

	public void setStatusByUserName(String statusByUserName) {
		_statusByUserName = statusByUserName;
	}

	public Date getStatusDate() {
		return _statusDate;
	}

	public void setStatusDate(Date statusDate) {
		_statusDate = statusDate;
	}

	private String _uuid;
	private long _commentId;
	private long _groupId;
	private long _companyId;
	private long _createdby;
	private String _modifiedby;
	private Date _createDate;
	private Date _modifiedDate;
	private Date _commentDate;
	private String _approverName;
	private String _approverContractNumber;
	private String _comment;
	private long _complianceId;
	private int _status;
	private long _statusByUserId;
	private String _statusByUserName;
	private Date _statusDate;

}